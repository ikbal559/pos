@extends('layouts.shop')
@section('title')
    @parent
   Member Report
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">

                <div class="panel-heading clearfix">
                    <h5>Member Report</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <form class="form-inline" method="get">
                                                <div class="form-group">
                                                    {{ Form::text('member_id',null,
                                                        ['class'=>'form-control', 'placeholder'=>'Search By Member',  ]
                                                        )}}
                                                </div>
                                                <button type="submit" class="btn btn-default">Search</button>
                                                <div class="form-group">
                                                    <a href="{{ route('member-report') }}" class="fa fa-close push-5-r"></a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            @if(!empty($member))
                                                <table class="table">
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>{{ $member->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Address</td>
                                                        <td>{{ $member->address }}</td>
                                                    </tr>

                                                </table>
                                             @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Latest Store IN</h5>
                    <span class="pull-right btn btn-info">AVG Price: </span>
                </div>
                <div class="panel-body">

                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="panel-body mobilePadding0">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="products-data-table">
                                            <thead>
                                            <th>V No</th>
                                            <th>Qty</th>
                                            <th>Amount</th>
                                            <th>Total</th>
                                            <th>Date</th>
                                            </thead>

                                            <tbody>
                                                {{--@foreach($storeInProduct as $item)--}}
                                                    {{--<tr>--}}
                                                        {{--<td>{{ $item->v_no }}</td>--}}
                                                        {{--<td>{{ $item->qty }}</td>--}}
                                                        {{--<td>{{ $item->price }}</td>--}}
                                                        {{--<td>{{ $item->total }}</td>--}}
                                                        {{--<td>{{ $item->created_at }}</td>--}}
                                                    {{--</tr>--}}
                                                {{--@endforeach--}}
                                            {{--</tbody>--}}
                                        </table>

                                    </div>
                                </div>

                            </div>
                        </div>

                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Latest Sales</h5>
                    <span class="pull-right btn btn-info">Sales: </span>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="panel-body mobilePadding0">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="products-data-table">
                                            <thead>
                                            <th>ID</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                            <th>Point</th>
                                            <th>Date</th>
                                            </thead>

                                            <tbody>
                                            {{--@foreach($lestSaleProduct as $item)--}}
                                                {{--<tr>--}}
                                                    {{--<td>{{ $item->order_id }}</td>--}}
                                                    {{--<td>{{ $item->qty }}</td>--}}
                                                    {{--<td>{{ $item->price }}</td>--}}
                                                    {{--<td>{{ $item->total }}</td>--}}
                                                    {{--<td>{{ $item->point }}</td>--}}
                                                    {{--<td>{{ $item->created_at }}</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
