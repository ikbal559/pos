{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal' ]) !!}

        {{ Form::hidden('id', (!empty($member)) ? $member->id : null  ,['class'=>'form-control']) }}
        {{ Form::hidden('shop_id',  \Auth::user()->shop_id ,['class'=>'form-control']) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('name',  (!empty($member)) ? $member->name : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Name is required'
                ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('phone', 'Phone:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('phone',  (!empty($member)) ? $member->phone : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Phone is required'
                ]) }}
            </div>
        </div>
@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-left clearfix']) }}
    </div>
</div>
{!! Form::close() !!}

