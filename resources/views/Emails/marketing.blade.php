<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        .body {
            background-color: #f6f6f6;
            width: 100%; }
        .container {
            display: block;
            Margin: 30px auto !important;
            /* makes it centered */
            max-width: 880px;
            padding: 10px;
            width: auto !important;
            width: 880px; }
        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 880px;
            padding: 30px;
            background: white;
        }

    </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content"><?php echo $content;?></div>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>