@extends('layouts.network')
@section('title')
    @parent
    User Dashboard
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <!-- END Stats -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="block" style="background-color: #FFF; padding: 15px;">
                        <div class="block-header">
                            <div class="pull-left">
                                <div class="h2 block-title">{{ Auth::user()->name }} </div>
                                <div class="h2 block-title text-warning push-10-t">{{ Auth::user()->username }} </div>
                            </div>

                            <?php $img = Auth::user()->user_images; ?>
                            @if( $img != null)
                                <img  class="img-responsive pull-right" style="width: 160px" src="{{asset("images/user_images/$img")}}">
                            @endif

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="block">

                        <a class="block block-link-hover3  bg-white" href="javascript:void(0)">
                            <table class="block-table text-center">
                                <tbody>
                                    <tr>
                                        <td style="width: 50%;">
                                            <div class="push-30 push-30-t text-uppercase">Balance</div>
                                        </td>
                                        <td class="bg-primary text-white" style="width: 50%;">
                                            <span class="h1">{{ Auth::user()->account->balance }} <span style="font-size: 14px;">tk</span></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </a>

                        <a class="block block-link-hover3  bg-white" href="javascript:void(0)">
                            <table class="block-table text-center">
                                <tbody>
                                    <tr>
                                        <td style="width: 50%;">
                                            <div class="push-30 push-30-t text-uppercase">Point</div>
                                        </td>
                                        <td class="bg-success text-white" style="width: 50%;">
                                            <div class="h1"><span style="font-size: 14px;">PV</span> {{ Auth::user()->account->points }} <span style="font-size: 14px;">tk</span>  </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </a>




                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
