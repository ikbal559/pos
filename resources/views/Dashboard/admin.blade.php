@extends('layouts.app')
@section('title')
    @parent
    Dashboard
@stop
@section('content')
    <div class="block">
        <div class="block-content">

            <!-- Stats -->
            <div class="content bg-white border-b" style="padding-top: 15px; margin-bottom: 25px; ">
                <div class="row items-push text-uppercase">


                    <div class="col-xs-6 col-sm-4">
                        <div class="font-w700 text-gray-darker animated fadeIn">Shops</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> ALL TIME</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">{{ $shops  }}</a>
                    </div>

                    <div class="col-xs-6 col-sm-4">
                        <div class="font-w700 text-gray-darker animated fadeIn">Sales</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Today</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">{{ $sales_today  }}</a>
                    </div>

                    <div class="col-xs-6 col-sm-4">
                        <div class="font-w700 text-gray-darker animated fadeIn">Product Sales</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> This Month</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">{{ $sales_month  }}<span style="font-size: 12px; color: #999999;">{{env('Currency')}}</span></a>
                    </div>


                </div>
            </div>
            <!-- END Stats -->

            <div class="row">
            @if(Auth::user()->id == 1)
                <div class="col-lg-7">
                    <!-- Bars Chart -->
                    <div class="block" style="background-color: #FFF; padding: 15px;">
                        <div class="block-header">
                            <h3 class="block-title">Sales This Week</h3>
                        </div>
                        <div class="block-content block-content-full text-center">
                            <!-- Bars Chart Container -->
                            <div style="height: 400px;"><canvas class="js-chartjs-bars"></canvas></div>
                        </div>
                    </div>
                    <!-- END Bars Chart -->
                </div>
                <div class="col-lg-5">



                </div>
                @else
                <div class="col-lg-12">
                        <!-- Bars Chart -->
                        <div class="block" style="background-color: #FFF; padding: 15px;">
                            <div class="block-header">
                                <h3 class="block-title">Sales This Week</h3>
                            </div>
                            <div class="block-content block-content-full text-center">
                                <!-- Bars Chart Container -->
                                <div style="height: 400px;"><canvas class="js-chartjs-bars"></canvas></div>
                            </div>
                        </div>
                        <!-- END Bars Chart -->
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection



@push('footer.scripts')
<script>
    window.salesLastWeekDay= [<?php  echo $WeekSaleReportDay;  ?>];
    window.salesLastWeekTotal=[<?php echo $WeekSaleReportTotal;  ?>];
</script>
<script type="text/javascript" src="{{asset("js/plugins/chartjs/Chart.min.js")}}"></script>
<script type="text/javascript" src="{{asset("js/pages/base_comp_charts.js")}}"></script>
<script>
    $(function () {
        // Init page helpers (Easy Pie Chart plugin)
        App.initHelpers('easy-pie-chart');
    });
</script>
@endpush
