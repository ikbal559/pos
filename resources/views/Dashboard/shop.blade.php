@extends('layouts.shop')
@section('title')
    @parent
    Shop Dashboard
@stop
@section('content')
    <div class="block">
        <div class="block-content">

            <div class="row">
                <div class="col-lg-7">
                    <!-- Bars Chart -->
                    <div class="block" style="background-color: #FFF; padding: 15px;">
                        <div class="block-header">
                            <h3 class="block-title">Sales This Week</h3>
                        </div>
                        <div class="block-content block-content-full text-center">
                            <!-- Bars Chart Container -->
                            <div style="height: 400px;"><canvas class="js-chartjs-bars"></canvas></div>
                        </div>
                    </div>
                    <!-- END Bars Chart -->
                </div>

            </div>
        </div>
    </div>
@endsection



@push('footer.scripts')
<script>
    window.salesLastWeekDay= [<?php  echo $WeekSaleReportDay;  ?>];
    window.salesLastWeekTotal=[<?php echo $WeekSaleReportTotal;  ?>];
</script>
<script type="text/javascript" src="{{asset("js/plugins/chartjs/Chart.min.js")}}"></script>
<script type="text/javascript" src="{{asset("js/pages/base_comp_charts.js")}}"></script>
<script>
    $(function () {
        // Init page helpers (Easy Pie Chart plugin)
        App.initHelpers('easy-pie-chart');
    });
</script>
@endpush
