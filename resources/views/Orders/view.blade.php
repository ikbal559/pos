@extends('layouts.shop')
@section('title')
    @parent
    View Order
@stop

@push('head.stylesheets')
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th {
        padding: 4px 2px 4px;
        font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 12px;
        font-weight: 600;
        text-transform: uppercase;
    }
    .table>tbody>tr>td{
        padding: 4px 2px 4px;
        font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 10px;
        text-transform: uppercase;
    }

    @media print{
        .table-bordered , .table>tbody>tr>td, .table > thead > tr > th{
            border: 1px solid #333 !important;
        }
        #main-container {
            margin-top: 0px !important;
            width: 100%;
        }
        .content {
            padding: 2px 2px 1px;
        }
        #page-container {
            min-width: 220px;
        }
    }

</style>
@endpush

@section('content')
    <div class="row" >
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="panel panel-default" >
                <div class="panel-body" style="padding: 10px;">

                    <!-- Invoice -->
                    <div class="block" style="margin: 0;" >
                        <div class="block-header" style="padding: 15px 0 3px;">
                            <div  style="font-size: 12px;" class="pull-left block-title">#{{ $order->order_id }}</div>
                            <div  style="font-size: 10px;"  class="block-title pull-right">{{  date("F j, Y g:i a", strtotime($order->created_at)) }}</div>
                        </div>
                        <div class="block-content " >
                            <div class="row">
                                <!-- Company Info -->
                                <div class="col-xs-12" style="margin-bottom: 5px;">
                                    <p style="font-size: 16px; font-weight: 600; margin: 0; text-align: center;">OmBazar.com</p>
                                    <p style="font-size: 14px; margin: 0;  text-align: center;">{{ $order->shop->name }}</p>
                                    @if($order->member_id > 0)
                                        <p style="font-size: 10px;  margin: 0; text-align: left;">Customer: {{ $order->member->name}} - {{ $order->member->username}}</p>
                                        <p style="font-size: 10px;  margin: 0; text-align: left;">Address: {{ $order->member->address}}</p>
                                    @endif
                                </div>
                                <!-- END Company Info -->

                            </div>
                            <!-- END Invoice Info -->

                            <!-- END Invoice Info -->

                            <!-- Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    @if(!$products->isEmpty())

                                    <thead>
                                    <tr>
                                        <th style="font-size: 10px;  text-transform: capitalize; width: 20px;"  class="text-center"  >SI</th>
                                        <th style="font-size: 10px;  text-transform: capitalize;" >Description</th>
                                        <th style="font-size: 10px;  text-transform: capitalize; width: 40px;" class="text-center"  >Qty.</th>
                                        <th style="font-size: 10px;  text-transform: capitalize; width: 60px;" class="text-right"  >Rate</th>
                                        <th style="font-size: 10px;  text-transform: capitalize; width: 100px;" class="text-right"  >Amount</th>
                                        <th style="font-size: 10px;  text-transform: capitalize; width: 60px;" class="text-right"  >Point</th>
                                    </tr>
                                    </thead>
                                    @endif
                                    <tbody>
                                        <?php
                                            $subTotal = 0;
                                            $si = 1;
                                        ?>
                                    @foreach($products as $product)
                                        <?php $subTotal += $product->total;;?>

                                        <tr>
                                            <td class="text-center">{{ $si }} <?php $si++;?></td>
                                            <td style="font-size: 10px; margin: 0; text-align: left; text-transform: capitalize;">
                                                {{ $product->product->name  }} - {{ $product->product->size_model }}
                                            </td>
                                            <td  style="font-size: 10px; margin: 0; text-align: center;">{{ $product->qty }}</td>
                                            <td class="text-right">{{ $product->price }}</td>
                                            <td class="text-right">{{ $product->total }} {{env('Currency')}}</td>
                                            <td class="text-right">{{ $product->point }}</td>
                                        </tr>
                                    @endforeach

                                    <tr class="active">
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Total </td>
                                        <td class="font-w700 text-right">{{ $order->order_total }} {{env('Currency')}}</td>
                                        <td   class="font-w700 text-uppercase text-right">{{ $order->order_point }}</td>
                                    </tr>


                                    <tr>
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Previous Due</td>
                                        <td class="font-w700 text-right">{{ $order->previous_due }} {{env('Currency')}}</td>
                                        <td   class="font-w700 text-uppercase text-right"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Amount Paid</td>
                                        <td class="font-w700 text-right">{{ $order->paid }} {{env('Currency')}}</td>
                                        <td   class="font-w700 text-uppercase text-right"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Current Due</td>
                                        <td class="font-w700 text-right">{{ $order->member_balance }} {{env('Currency')}}</td>
                                        <td   class="font-w700 text-uppercase text-right"></td>
                                    </tr>


                                    <tr><td colspan="6" class="text-left" style="text-transform: capitalize;">
                                    <?php

                                        function convert_number_to_words($number) {

                                            $hyphen      = '-';
                                            $conjunction = ' and ';
                                            $separator   = ', ';
                                            $negative    = 'negative ';
                                            $decimal     = ' point ';
                                            $dictionary  = array(
                                                    0                   => 'zero',
                                                    1                   => 'one',
                                                    2                   => 'two',
                                                    3                   => 'three',
                                                    4                   => 'four',
                                                    5                   => 'five',
                                                    6                   => 'six',
                                                    7                   => 'seven',
                                                    8                   => 'eight',
                                                    9                   => 'nine',
                                                    10                  => 'ten',
                                                    11                  => 'eleven',
                                                    12                  => 'twelve',
                                                    13                  => 'thirteen',
                                                    14                  => 'fourteen',
                                                    15                  => 'fifteen',
                                                    16                  => 'sixteen',
                                                    17                  => 'seventeen',
                                                    18                  => 'eighteen',
                                                    19                  => 'nineteen',
                                                    20                  => 'twenty',
                                                    30                  => 'thirty',
                                                    40                  => 'fourty',
                                                    50                  => 'fifty',
                                                    60                  => 'sixty',
                                                    70                  => 'seventy',
                                                    80                  => 'eighty',
                                                    90                  => 'ninety',
                                                    100                 => 'hundred',
                                                    1000                => 'thousand',
                                                    1000000             => 'million',
                                                    1000000000          => 'billion',
                                                    1000000000000       => 'trillion',
                                                    1000000000000000    => 'quadrillion',
                                                    1000000000000000000 => 'quintillion'
                                            );

                                            if (!is_numeric($number)) {
                                                return false;
                                            }

                                            if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
                                                // overflow
                                                trigger_error(
                                                        'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                                                        E_USER_WARNING
                                                );
                                                return false;
                                            }

                                            if ($number < 0) {
                                                return $negative . convert_number_to_words(abs($number));
                                            }

                                            $string = $fraction = null;

                                            if (strpos($number, '.') !== false) {
                                                list($number, $fraction) = explode('.', $number);
                                            }

                                            switch (true) {
                                                case $number < 21:
                                                    $string = $dictionary[$number];
                                                    break;
                                                case $number < 100:
                                                    $tens   = ((int) ($number / 10)) * 10;
                                                    $units  = $number % 10;
                                                    $string = $dictionary[$tens];
                                                    if ($units) {
                                                        $string .= $hyphen . $dictionary[$units];
                                                    }
                                                    break;
                                                case $number < 1000:
                                                    $hundreds  = $number / 100;
                                                    $remainder = $number % 100;
                                                    $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                                                    if ($remainder) {
                                                        $string .= $conjunction . convert_number_to_words($remainder);
                                                    }
                                                    break;
                                                default:
                                                    $baseUnit = pow(1000, floor(log($number, 1000)));
                                                    $numBaseUnits = (int) ($number / $baseUnit);
                                                    $remainder = $number % $baseUnit;
                                                    $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                                                    if ($remainder) {
                                                        $string .= $remainder < 100 ? $conjunction : $separator;
                                                        $string .= convert_number_to_words($remainder);
                                                    }
                                                    break;
                                            }

                                            if (null !== $fraction && is_numeric($fraction)) {
                                                $string .= $decimal;
                                                $words = array();
                                                foreach (str_split((string) $fraction) as $number) {
                                                    $words[] = $dictionary[$number];
                                                }
                                                $string .= implode(' ', $words);
                                            }

                                            return $string;
                                        }

                                            echo convert_number_to_words(intval($order->order_total));
                                    ?> Only
                                   </td></tr>
                                    <tr>
                                        <td colspan="6" class="text-left" style="text-transform: capitalize;">Prepared by: {{ $order->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="text-left" style="text-transform: capitalize;">Total Discount Tk. {{ $order->order_discount }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END Table -->
                            <p class=" text-center"><small>Thank you very much for doing business with us.<br> Contact: {{ $order->shop->phone }} </small></p>
                            <button class="btn btn-info hidden-print" type="button" onclick="App.initHelper('print-page');"><i class="si si-printer"></i> Print Invoice</button>
                            <a class="btn btn-warring btn-sm hidden-print pull-right" href="{{ route('order-edit', $order->order_id) }}" ><i class="fa fa-edit"></i> Update Invoice</a>
                            <!-- END Footer -->
                        </div>
                    </div>
                    <!-- END Invoice -->

                </div>
            </div>

        </div>
    </div>
@endsection
