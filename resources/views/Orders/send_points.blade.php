@extends('layouts.shop')
@section('title')

    Send Points
@stop
@section('content')
    <div class="row"  style="margin-top: 10px;">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-md-12">
                        <button class="StartPay btn btn-danger btn-lg push-10-t push-10-l" data-action="{{ route('send-points-process') }}">Start Point Send</button>
                        <span class="btn btn-warning push-10-t push-10-l">Total Point: {{    number_format($order_point, 0) }}</span>
                    </div><!-- /.box-footer-->
                </div>

                <div class="panel-body">
                    @if(!empty($orders))
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Date</th>
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Point</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $item)
                                <tr>
                                    <td><a class="label label-info" href="{{ route('order-view', $item->order_id ) }}">View Order</a></td>
                                    <td>{{  date("F j, Y g:i a", strtotime($item->created_at)) }}</td>
                                    <td>{{ $item->order_id }}</td>
                                    <td>{{ $item->member->name }} - {{ $item->member->username }} ( {{ $item->member_balance }} )</td>
                                    <td>{{ $item->order_point }}</td>
                                    <td><span class="StartPayP order_id_{{ $item->order_id }}">Pending</span></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h4 class="text-center"> No Order Found!</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer.scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var ProcessBill = function () {
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: $('.StartPay').attr('data-action'),

                    success: function (json) {
                        $('.order_id_'+json.id).html('<span class="label label-success">Send</span>');
                        if (json.id > 0) {
                            setTimeout(function(){
                                ProcessBill();
                            }, 3000);
                        }else {
                            setTimeout(function(){location.reload();}, 2000);
                        }
                    },
                    error: function () {
                        $('.errorMessage').removeClass('hidden');
                    },
                    dataType: "json"
                });
            };

            $('.StartPay').click( function () {
                $('.StartPayP').html('<span class="fa fa-spinner"></span>');
                ProcessBill();
            });
        });
    </script>
@endpush