@extends('layouts.shop')
@section('title')
    @parent
    View Orders
@stop
@section('content')
    <div class="row"  style="margin-top: 10px;">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border push-10 push-10-t push-15-r pull-right">
                            <form class="form-inline" method="get">
                                 <div class="form-group">
                                   <a href="{{ route('order-list') }}" class="fa fa-close push-5-r"></a>
                                </div>

                                @if(Auth::user()->user_type == 'admin')
                                    <div class="form-group">
                                        {{ Form::select('shop_id', $shops, null,
                                            ['class'=>'form-control ShopID', 'placeholder'=>'Search By Shop',  ]
                                            )}}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <input value="{{ isset( $_GET['order_id'] )  ? $_GET['order_id']: null }}",   class="form-control" name="order_id" placeholder="Order ID" >
                                </div>
                                <div class="form-group">
                                    <input value="{{ isset( $_GET['customer_id'] )  ? $_GET['customer_id']: null }}",   class="form-control" name="customer_id" placeholder="Customer Phone" >
                                </div>
                                <div class="form-group">
                                    <input value="{{ isset( $_GET['start_date'] )  ? $_GET['start_date']: null }}",  data-date-end-date="0d" class="js-datepicker form-control" name="start_date"  data-date-format="yyyy-mm-dd" placeholder="From: yyyy-mm-dd">
                                </div>
                                <div class="form-group">
                                    <input value="{{isset( $_GET['end_date'] )  ? $_GET['end_date']: null}}" data-date-format="yyyy-mm-dd"  class="js-datepicker form-control" name="end_date" placeholder="TO: yyyy-mm-dd">
                                </div>
                                <button type="submit" class="btn btn-default">Search</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <span class="btn btn-success push-10-t push-10-l">Sales: {{ number_format($order_total, 0) }}</span>
                    <span class="btn btn-success push-10-t push-10-l">Paid: {{ number_format($order_paid, 0) }}</span>
                    <span class="btn btn-success push-10-t push-10-l">Due: {{ number_format($order_due, 0) }}</span>
                    <span class="btn btn-warning push-10-t push-10-l">Profit: {{    number_format($order_profit, 0) }}</span>
                    <span class="btn btn-info push-10-t push-10-l">Point: {{   number_format($order_point, 0)  }}</span>
                    <span class="btn btn-info push-10-t push-10-l">Point Send: {{   number_format($order_point_send, 0)  }}</span>
                    <span class="btn btn-info push-10-t push-10-l">Point Send Cost: {{   $order_point_send * .18  }}</span>
                    <span class="btn btn-primary push-10-t push-10-l">Discount: {{   number_format($order_discount, 0)  }}</span>
                </div><!-- /.box-footer-->
                <div class="panel-body">
                    @if(!empty($orders))
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Point</th>
                                <th>Discount</th>
                                <th>Total</th>
                                <th>Profit</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $item)
                                <tr>
                                    <td><a class="label label-info" href="{{ route('order-view', $item->order_id ) }}">View Order</a></td>
                                    <td>{{ $item->order_id }}</td>
                                    <td>@if($item->member_id)
                                        {{ $item->member->name }} - {{ $item->member->username }} ( {{ $item->member_balance }} )
                                    @endif</td>
                                    <td>{{ $item->order_point }}
                                    @if( $item->order_status == 2 )
                                        <span class="label label-success">Send</span>
                                    @elseif($item->order_status == 3)
                                        <a class="label label-danger " href="{{ route('send-points-retry', $item->order_id) }}">Failed</a>
                                    @else
                                        <span class="label label-info">Pending</span>
                                    @endif
                                    </td>
                                    <td>{{ $item->order_discount }}</td>
                                    <td>{{ $item->order_total }}</td>
                                    <td>{{ $item->order_profit }}</td>
                                    <td>{{  date("F j, Y g:i a", strtotime($item->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h4 class="text-center"> No Order Found!</h4>
                    @endif
                    <div class="col-md-12">
                        <div class="col-md-6 pull-right">
                            {{ $orders->links() }}
                        </div>
                    </div><!-- /.box-footer-->

                </div>
            </div>

        </div>
    </div>
@endsection
