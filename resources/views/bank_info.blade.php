@extends('layouts.home')

@section('content')
        <!-- Main Container -->
        <main id="main-container " class="clearfix">

            <!-- Team -->
            <div class="bg-white" style="margin-bottom: 15px;">
                <section class="content content-boxed">
                    <!-- Section Content -->
                    <div class="push-20">
                        <div class="row items-push-2x">
                            <div class="col-md-4 visibility-hidden" data-toggle="appear" data-class="animated zoomIn">
                                <div class="block">
                                    <div class="block-header">
                                        <h3 class="block-title">Social Islami Bank Ltd</h3>
                                    </div>
                                    <div class="block-content">
                                        <p>Paikarimarketbd.com<br>
                                            Account No. 911330002066<br>
                                            Social Islami Bank Ltd<br>
                                            Mouchak Branch,Dhaka </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 visibility-hidden" data-toggle="appear" data-class="animated zoomIn">
                                <div class="block">
                                    <div class="block-header">
                                        <h3 class="block-title">Bkash</h3>
                                    </div>
                                    <div class="block-content">
                                        <p>01714934093(Personal)</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- END Section Content -->
                </section>
            </div>
            <!-- END Team -->
        </main>
        <!-- END Main Container -->


@endsection
