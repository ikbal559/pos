<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">

            <div class="side-header side-content bg-primary-dark-op">
                <div class="h2 text-white"><i class="fa fa-user" aria-hidden="true"></i>    Admin</div>
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
            </div>

            <!-- Side Content -->
            <div class="side-content">

                <ul class="nav-main">
                    <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close"><i class="fa fa-times"></i></button>

                    <li><a href="{{ route('members')  }}"><span class="fa fa-user"></span><span class="sidebar-mini-hide"> Members</span></a></li>
                    <li><a href="{{ route('clients')  }}"><span class="fa fa-users"></span><span class="sidebar-mini-hide"> All Vendor</span></a></li>
                    <li><a href="{{ route('product-list')  }}"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> All Products</span></a></li>

                    <li><a href="{{ route('store-in-list')  }}"><span class="text-warning"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Store History</span></span></a></li>
                    <li><a href="{{ route('order-list')  }}"><span class="text-info"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Sales</span></span></a></li>



                    <li><a href=" {{ route('shops')  }} "><i class="fa fa-home"></i><span class="sidebar-mini-hide">Shops</span></a></li>
                    <li><a href="{{ route('dashboard-index') }}"><i class="fa fa-dashboard"></i><span class="sidebar-mini-hide">Dashboard</span></a></li>


                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END Sidebar -->
