<nav id="sidebar">
    <div id="sidebar-scroll">
        <div class="sidebar-content">
            <div class="side-header side-content bg-primary-dark">
                <div class="h2 text-white">Welcome</div>
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
            </div>
            <div class="side-content">
                <ul class="nav-main">

                    <li><a href="{{ route('dashboard-index') }}"><i class="fa fa-dashboard"></i><span class="sidebar-mini-hide">Dashboard</span></a></li>


                </ul>
            </div>
        </div>
    </div>
</nav>

