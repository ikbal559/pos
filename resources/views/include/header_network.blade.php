@if (! Auth::guest())
<header id="header-navbar" class="content-mini content-mini-full">
    <div class="ActivityMassage col-md-4 hidden-mobile ">
        <a class="h3 text-white push-20-r" href="{{ route('dashboard-index') }}">Dashboard</a>

    </div>


    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
    <li class="hidden-md hidden-lg">
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default pull-left-mobile" data-toggle="layout" data-action="sidebar_toggle" type="button">
            <i class="fa fa-navicon"></i>
        </button>
    </li>
        <div class="btn-group pull-right">

                <button class="btn btn-default dropdown-toggle fa fa-gear pull-right" data-toggle="dropdown"  type="button"></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('dashboard-index') }}">Dashboard</a></li>
                    <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();    document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                </ul>
                <div class="UserInfo">
                    <?php $img = Auth::user()->user_images; ?>
                    @if( $img != null)
                        <img  class=" hidden-mobile img-circle pull-left" style="width: 45px;margin: -8px 20px 0 0;    border: 2px solid #ddd;" src="{{asset("images/user_images/$img")}}">
                    @endif

                    <div class="h3 headerUserName"  style="display: inline-block"> {{ Auth::user()->name }} </div>

                    @if(Auth::user()->user_type == 'network')
                        <div class="h3" style=" display: inline-block;">
                            <span class="text-amethyst text">|</span> <span class="text-warning">{{   Auth::user()->account->balance }}</span> <span style="font-size: 12px;">tk</span>
                            <span class="text-amethyst">|</span> PV {{   Auth::user()->account->points }}  <span style="font-size: 12px;">tk</span>
                        </div>
                    @endif
                </div>
        </div>
    </ul>
</header>
@endif
