<!-- Footer -->
<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix hidden-print">
    <?php if(!empty($accountsInfo)):?>
    <div class="AccountInfo">
        <ul>
            <li>Account Balance <span></span></li>
            <li>Total Point <span></span></li>
            <li>Commission  <span></span></li>
        </ul>
    </div>
    <?php endif;?>


    <div class="pull-right">
        <a class="font-w600" href="" target="_blank">Home Page</a></span>
    </div>

</footer>
<!-- END Footer -->