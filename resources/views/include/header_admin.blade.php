 @if (! Auth::guest())
<!-- Header -->
<header id="header-navbar" class="content-mini content-mini-full">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">

        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default pull-left-mobile" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>

        <div class="btn-group pull-right">
            <!-- Authentication Links -->
                <button class="btn btn-default dropdown-toggle fa fa-gear pull-right" data-toggle="dropdown"  type="button"></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('dashboard-index') }}">Dashboard</a></li>
                    <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();    document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                </ul>
        </div>
    </ul>
    <!-- END Header Navigation Right -->
</header>
<!-- END Header -->
@endif
