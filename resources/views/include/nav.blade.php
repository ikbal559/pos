<!-- Sidebar -->
<nav id="sidebar" class=" hidden-print">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">


            <!-- Side Content -->
            <div class="side-content">

                <ul class="nav-main">
                    <li><a href="{{ route('clients')  }}"><span class="fa fa-users"></span><span class="sidebar-mini-hide"> All Clients</span></a></li>
                    <li><a href="{{ route('product-list')  }}"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> All Products</span></a></li>
                    <li><a href="{{ route('store-in-list')  }}"><span class="text-warning"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Store Report</span></span></a></li>
                    <li><a href="{{ route('order-list')  }}"><span class="text-info"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Sales Report</span></span></a></li>
                    {{--<li><a href="{{ route('cash-report')  }}"><span class="text-success"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Cash Report</span></span></a></li>--}}
                    <li><a href="{{ route('payments-report')  }}"><span class="text-success"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Cash Report</span></span></a></li>
                    <li><a href="{{ route('send-points')  }}"><span class="text-success"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Send Points</span></span></a></li>
<hr>
                    <li><a class="" href="{{ route('dealer-sales')  }}"><span class=""><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> Dealer Sales</span></span></a></li>
                    <li><a class="text-warning" href="{{ route('product-add')  }}"><span class="fa fa-plus-circle"></span><span class="sidebar-mini-hide"> Add Product</span></a></li>
                    <li><a class="text-warning" href="{{ route('store-in')  }}"><span class="text-warning"><span class="fa fa-plus"></span><span class="sidebar-mini-hide"> Store IN</span></span></a></li>
                    <li><a class="text-danger" href="{{ route('sales')  }}"><span class="text-danger"><span class="fa fa-cart-plus"></span><span class="sidebar-mini-hide"> SALES</span></span></a></li>
                    <li class="push-20-t"><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();    document.getElementById('logout-form').submit();"><span class="fa fa-sign-out"></span><span class="sidebar-mini-hide"> Logout</span></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->
