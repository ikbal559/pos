@if (! Auth::guest())
<!-- Header -->
<header id="header-navbar" class="content-mini content-mini-full ">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">

        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>

        <div class="btn-group pull-right">
            <button class="btn btn-default dropdown-toggle fa fa-gear pull-right" data-toggle="dropdown"  type="button"></button>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="{{ route('dashboard-index') }}"><span class="sidebar-mini-hide">Dashboard</span></a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </li>
            </ul>
            <div class="UserInfo">
                <div class="h3"  style="display: inline-block"> {{ Auth::user()->shop->name  }} </div>
            </div>
        </div>
    </ul>

    <!-- Header Navigation Left -->
    <ul class="nav-header pull-left">
        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>
        <li class="hidden-xs hidden-sm">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                <i class="fa fa-ellipsis-v"></i>
            </button>
        </li>

        <li><a href="{{ route('product-list')  }}"><span class="fa fa-shopping-cart"></span><span class="sidebar-mini-hide"> All Products</span></a></li>
        <li><a class="text-warning" href="{{ route('product-add')  }}"><span class="fa fa-plus-circle"></span><span class="sidebar-mini-hide"> Add Product</span></a></li>
        <li><a class="text-warning" href="{{ route('store-in')  }}"><span class="text-warning"><span class="fa fa-plus"></span><span class="sidebar-mini-hide"> Store IN</span></span></a></li>
        <li><a class="text-white label label-warning" href="{{ route('sales')  }}"><span class="text-white"><span class="fa fa-cart-plus"></span><span class="sidebar-mini-hide"> Sales</span></span></a></li>
    </ul>
    <!-- END Header Navigation Left -->

</header>
<!-- END Header -->
@endif
