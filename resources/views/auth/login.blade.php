@extends('layouts.home')
@section('content')


    <div class="row">
        <div class="col-md-10 col-md-offset-1 MobilFormStyle push-20-t" >

            <div class="block block-themed animated bounceIn" style="background-color: #fff;">
                <div class="block-header bg-info">
                    <h3 class="block-title">Shop Login</h3>
                </div>
                <div class="block-content block-content-full block-content-narrow">
                    <form class="form-horizontal push-50-t" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-info">
                                    Login
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
