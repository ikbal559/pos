@extends('layouts.shop')
@section('title')
    @parent
    Sales
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="clearfix push-5-t">
                            <div class="col-sm-6 col-lg-3">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="1" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-smooth">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $pending }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Pending Orders</div>
                                    </div>

                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="2" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-info">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $request_payment }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Payment Request</div>
                                    </div>

                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="3" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-warning">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $delivery }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Delivery</div>
                                    </div>

                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="4" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-success">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $complete }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Complete</div>
                                    </div>

                                </a>
                            </div>
                            <input type="hidden" value="" class="userType" name="userType">
                        </div>
                        <div class="panel-body mobilePadding0">
                            <div class="table-responsive">
                                <table class="table table-striped" id="order-data-table">
                                    <thead>
                                        <th>Action</th>
                                        <th>Status</th>
                                        <th>Total</th>
                                        <th>ID</th>
                                        <th>Quantity</th>
                                        <th>Discount</th>
                                        <th>Date</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer.scripts')
<script>
    var base_url = window.base_url + '/load/';


    var oTable =  $('#order-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:  base_url + 'Orders/Orders' ,
            data: function (d) {
                d.user_type =  $('.userType').val()
            }
        },
        columns: [
            { data: 'action', name: 'action' },
            { data: 'status', name: 'status' },
            { data: 'order_total', name: 'order_total' },
            { data: 'order_id', name: 'order_id' },
            { data: 'order_qty', name: 'order_qty' },
            { data: 'order_discount', name: 'order_discount' },
            { data: 'created_at', name: 'created_at' },
        ]
    });

    $('.userTypeClick').click(function () {
        $('.homeProduct').val('');
        $('.userType').val($(this).attr('data-type'));
        oTable.draw();
    });

</script>
@endpush