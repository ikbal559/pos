@extends('layouts.shop')
@section('title')
    paikarimarketbd.com
@stop
@section('content')
    <div class="row" >
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url'=>route('order-update-admin'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}
                    {{ Form::hidden('order_id', (!empty($order)) ? $order->order_id : null  ,['class'=>'form-control']) }}
                    <!-- Invoice -->
                    <div class="block">

                        <!-- Invoice Info -->

                        <div class="row items-push-2x">
                            <div class="col-lg-6">

                                <div class="text-left hidden-print"><span class="h1">
                                <?php
                                        switch ($order->order_status){
                                            case 1:
                                                echo '<span class="label label-warning">Pending</span>';
                                                break;
                                            case 2:
                                                echo '<span class="label label-info">Payment Request</span>';
                                                break;
                                            case 3:
                                                echo '<span class="label label-primary">Delivery</span>';
                                                break;
                                            case 4:
                                                echo '<span class="label label-success">Complete</span>';
                                                break;
                                        }
                                        ?>
                                </span></div>
                            </div>


                            <!-- Client Info -->
                            <div class="col-lg-6  text-right">
                                <p class="h2 font-w400 push-5">Customer</p>
                                <address>
                                    {{  $order->user->name }}<br>
                                    <i class="si si-call-end"></i> {{  $order->user->username }}
                                    @if(!empty($order->user->shop))   <br> {{  $order->user->shop->name }}  <br> {{  $order->user->shop->address }} @endif
                                </address>
                            </div>
                            <!-- END Client Info -->
                        </div>
                        <!-- END Invoice Info -->



                            <!-- Table -->
                            <div class="table-responsive push-50">
                                <table class="table table-bordered table-hover">
                                    @if(!$products->isEmpty())

                                    <thead>
                                        <tr>
                                        <th class="text-center" style="width: 50px;">ID</th>
                                        <th>Product</th>
                                        <th class="text-center" style="width: 80px;">Quantity</th>
                                        <th class="text-right" style="width: 80px;">Price</th>
                                        <th class="text-right" style="width: 130px;">Total</th>
                                    </tr>
                                    </thead>
                                    @endif
                                    <tbody>
                                        <?php $subTotal = 0;?>
                                                @foreach($products as $product)
                                        <?php $subTotal += $product->total; ?>
                                        <tr>
                                            <td class="text-center">{{ $product->product_id }}</td>
                                            <td>
                                                <div class="font-w600">{{ $product->product->product_name  }}</div>
                                                <div class="text-muted">{{ $product->product->product_variation }}, {{ $product->product->product_weight }}</div>
                                                <div class="font-w600 push-5-t">{{ $product->shop->name  }}</div>
                                                <div class="">{{ $product->shop->phone  }}</div>
                                            </td>
                                            <td class="text-center">{{ $product->qty }}</td>
                                            <td class="text-right">{{ $product->price }}</td>
                                            <td class="text-right">{{ $product->total }} {{env('Currency')}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Subtotal</td>
                                        <td class="text-right font-w600">{{ $order->order_total }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Discount</td>
                                        <td class="text-right">{{ $order->order_discount }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Service Fee</td>
                                        <td class="text-right">{{ $order->order_service_fee }}</td>
                                    </tr>
                                    <tr class="active">
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Total </td>
                                        <td class="font-w700 text-right">{{ $order->order_total + $order->order_service_fee - $order->order_discount }} {{env('Currency')}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END Table -->


                        <hr class="hidden-print">
                        <div class="row">
                            <div class="col-sm-12 clearfix">
                                <div class="col-sm-6 pull-left">
                                    <div class="h6">Order  Status:</div>  <?php
                                    switch ($order->order_status){
                                        case 1:
                                            echo '<span class="label label-warning">Pending</span>';
                                            break;
                                        case 2:
                                            echo '<span class="label label-info">Payment Request</span>';
                                            break;
                                        case 3:
                                            echo '<span class="label label-primary">Delivery</span>';
                                            break;
                                        case 4:
                                            echo '<span class="label label-success">Complete</span>';
                                            break;
                                    }
                                    ?>
                                    <div class="h6 push-5-t clearfix">Order  Note:</div> <p>{{ $order->order_note }}</p>
                                </div>
                                <div class="col-sm-6 pull-right">
                                    <span class="h6">Delivered By:</span> <p>{{ $order->delivery_name }}</p>
                                    <span class="h6">Delivery Memo Number:</span> <p>{{ $order->delivery_invoice }}</p>
                                </div>
                            </div>
                            <div class="col-sm-12 clearfix pull-left">
                                <ul class="list list-activity push">

                                    @foreach($order->sms as $sms)
                                        <li>
                                            <i class="si si-clock text-success"></i>
                                            <div class="font-w600">{{ date("F j, Y g:i a", strtotime($sms->created_at)) }}</div>
                                            <div><a href="javascript:void(0)">{{ $sms->content }}</a></div>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>

                        <!-- Footer -->
                        <hr class="hidden-print">
                        <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
                        <!-- END Footer -->



                        </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
