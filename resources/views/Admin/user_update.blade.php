@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h5 class="pull-left">Manage Profile</h5>
            </div>
            <div class="panel-body">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content" style="display: block;">
                            <div class="col-md-12">



                                {!! Form::open([ 'url' => route('api.update.profile'),  'method'=>'POST', 'class'=>'form-vertical form-horizontal push-30-t']) !!}

                                {{ Form::hidden('type', 'username' ) }}
                                <div class="h6 text-uppercase text-warning text-center push-10">Update User Name</div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        {{ Form::label('', ' ', [ 'class'=>'col-sm-3 control-label' ]) }}
                                    </div>
                                    <div class="col-sm-9">
                                        {{ Form::select('user_id', $users, null,
                                        [
                                            'placeholder'=> 'Select A User',
                                            'class'=>'form-control ProductSelectBoxGlobal',
                                            'data-fv-number' => 'true',
                                            'data-fv-notempty' => 'true',
                                            'data-fv-blank' => 'true',
                                            'data-fv-notempty-message' => 'User is required'
                                        ]) }}

                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('username', 'User Name:', ['class'=>'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-btn"><button class="btn btn-default" type="button">+880</button></span>
                                            {{ Form::number('username',  null ,
                                            [
                                                    'placeholder'=>'Username (example: 1847XXXXXX )',
                                                    'class'=>'form-control',
                                                    'data-fv-notempty' => 'true',
                                                    'data-fv-blank' => 'true',
                                                    'data-fv-notempty-message' => 'Username is required'
                                            ]) }}
                                        </div>
                                    </div>
                                </div>
                                @include('alert_message')
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        {{ Form::submit("Submit", ['class'=>'btn btn-success']) }}
                                    </div>
                                </div>
                                {!! Form::close() !!}


                                <hr>


                                {!! Form::open([ 'url' => route('api.update.profile'),  'method'=>'POST', 'class'=>'form-vertical form-horizontal push-30-t']) !!}

                                    {{ Form::hidden('type', 'password' ) }}
                                <div class="form-group">
                                    <div class="col-md-3">
                                        {{ Form::label('', ' ', [ 'class'=>'col-sm-3 control-label' ]) }}
                                    </div>
                                    <div class="col-sm-9">
                                        {{ Form::select('user_id', $users, null,
                                        [
                                            'placeholder'=> 'Select A User',
                                            'class'=>'form-control ProductSelectBoxGlobal',
                                            'data-fv-number' => 'true',
                                            'data-fv-notempty' => 'true',
                                            'data-fv-blank' => 'true',
                                            'data-fv-notempty-message' => 'User is required'
                                        ]) }}

                                    </div>
                                </div>

                                    <div class="h6 text-uppercase text-danger text-center push-10">Update Password</div>
                                    <div class="form-group">
                                        {{ Form::label('password', 'Password:', ['class'=>'col-sm-3 control-label']) }}
                                        <div class="col-sm-9">
                                            {{ Form::password('password',
                                            [
                                                    'placeholder'=>'Login Password',
                                                    'class'=>'form-control',
                                                    'data-fv-notempty' => 'true',
                                                    'data-fv-blank' => 'true',
                                                    'data-fv-notempty-message' => 'Password is required'
                                            ]) }}
                                        </div>
                                    </div>

                                    @include('alert_message')
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-success']) }}
                                        </div>
                                    </div>
                                {!! Form::close() !!}


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
