@extends('layouts.shop')
@section('title')
    @parent
    Cash Report
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="clearfix push-5-t">
                            <div class="col-sm-6 col-lg-4">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="1" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-success ">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $cashIn }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Cash IN</div>
                                    </div>

                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="2" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-smooth">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $cashOut }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Cash Oot</div>
                                    </div>

                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="3" href="javascript:void(0)">
                                    <div class="block-content block-content-full bg-warning">
                                        <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $cashIn -  $cashOut }}</span></div>
                                        <div class="h5 text-white-op text-uppercase push-5-t">Balance</div>
                                    </div>

                                </a>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
