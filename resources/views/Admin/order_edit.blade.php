@extends('layouts.shop')
@section('title')
    @parent
    Order Edit
@stop
@section('content')
    <div class="row"  style="margin-top: 100px;">
        <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">


                    {!! Form::open(['url'=>route('order-update-admin'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}
                    {{ Form::hidden('order_id', (!empty($order)) ? $order->order_id : null  ,['class'=>'form-control']) }}
                    <!-- Invoice -->
                    <div class="block">

                        <!-- Invoice Info -->

                        <div class="row items-push-2x">
                            <!-- Client Info -->
                            <div class="col-lg-6  text-left">
                                <p class="h2 font-w400 push-5">Customer</p>
                                <address>
                                    {{  $order->user->name }}<br>
                                    <i class="si si-call-end"></i> {{  $order->user->username }}
                                    @if(!empty($order->user->shop))   <br> {{  $order->user->shop->name }}  <br> {{  $order->user->shop->address }} @endif
                                </address>
                            </div>
                            <!-- END Client Info -->
                            <div class="col-lg-6">

                                <div class="text-right"><span class="h1">
                                <?php
                                        switch ($order->order_status){
                                            case 1:
                                                echo '<span class="label label-warning">Pending</span>';
                                                break;
                                            case 2:
                                                echo '<span class="label label-info">Payment Request</span>';
                                                break;
                                            case 3:
                                                echo '<span class="label label-primary">Delivery</span>';
                                                break;
                                            case 4:
                                                echo '<span class="label label-success">Complete</span>';
                                                break;
                                        }
                                        ?>
                                </span></div>
                            </div>



                        </div>
                        <!-- END Invoice Info -->


                            <!-- Table -->
                            <div class="table-responsive push-50">
                                <table class="table table-bordered table-hover">
                                    @if(!$products->isEmpty())

                                    <thead>
                                        <tr>
                                        <th class="text-center" style="width: 50px;">CODE</th>
                                        <th>Product</th>
                                        <th class="text-center" style="width: 80px;">Quantity</th>
                                        <th class="text-right" style="width: 80px;">Price</th>
                                        <th class="text-right" style="width: 130px;">Total</th>
                                    </tr>
                                    </thead>
                                    @endif
                                    <tbody>
                                        <?php $subTotal = 0;?>
                                                @foreach($products as $product)
                                        <?php $subTotal += $product->total; ?>
                                        <tr>
                                            <td class="text-center">{{ $product->product_id }}</td>
                                            <td>
                                                <p class="font-w600 push-10">{{ $product->product->product_name  }}</p>
                                                <div class="text-muted">{{ $product->product->product_variation }}, {{ $product->product->product_weight }}</div>
                                            </td>
                                            <td class="text-center">
                                                <input class="form-control" name="order[{{ $product->order_product_id }}][qty]" type="number" value="{{ $product->qty }}">
                                            </td>
                                            <td class="text-right">{{ $product->price }}</td>
                                            <td class="text-right">{{ $product->total }} {{env('Currency')}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Subtotal</td>
                                        <td class="text-right font-w600">{{ $order->order_total }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Discount</td>
                                        <td class="text-right"><input name="order_discount"  class="form-control" type="number" value="{{ $order->order_discount }}"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Service Fee</td>
                                        <td class="text-right"><input name="order_service_fee" class="form-control" type="number" value="{{ $order->order_service_fee }}"></td>
                                    </tr>
                                    <tr class="active">
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Total </td>
                                        <td class="font-w700 text-right">{{ $order->order_total + $order->order_service_fee - $order->order_discount }} {{env('Currency')}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END Table -->

                            <hr class="hidden-print">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            {{ Form::label('order_status', 'Status:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                            <div class="col-sm-9">
                                                {{ Form::select('order_status', [
                                                 1 => 'Pending',
                                                 2 => 'Payment Request',
                                                 3 => 'Delivery',
                                                 4 => 'Complete'
                                                ],  (!empty($order)) ? $order->order_status : null,
                                                [
                                                    'placeholder'=> 'Order Status',
                                                    'class'=> 'form-control',
                                                    'data-fv-notempty' => 'true',
                                                    'data-fv-blank' => 'true',
                                                    'data-fv-notempty-message' => 'Status is required'

                                                ]) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('product_status', 'Note:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                            <div class="col-sm-9">
                                                {{ Form::textarea('order_note',   (!empty($order)) ? $order->order_note : null,
                                                [
                                                    'placeholder'=> 'Order Note',
                                                    'class'=> 'form-control',
                                                    'rows'=> '3'
                                                ]) }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            {{ Form::label('delivery_name', 'Delivered  By:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                            <div class="col-sm-9">
                                                {{ Form::text('delivery_name',   (!empty($order)) ? $order->delivery_name : null,
                                                [
                                                    'placeholder'=> 'Delivery Name ',
                                                    'class'=> 'form-control'
                                                ]) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('delivery_invoice', 'Delivery Memo Number:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                            <div class="col-sm-9">
                                                {{ Form::text('delivery_invoice',   (!empty($order)) ? $order->delivery_invoice : null,
                                                [
                                                    'placeholder'=> 'Delivery Invoice',
                                                    'class'=> 'form-control'
                                                ]) }}
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {{ Form::label('delivery_name', 'Delivery Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                            <div class="col-sm-9">
                                                {{ Form::text('delivery_name',   (!empty($order)) ? $order->delivery_name : null,
                                                [
                                                    'placeholder'=> 'Delivery Name ',
                                                    'class'=> 'form-control'
                                                ]) }}
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        <hr class="hidden-print">
                            <div class="row">
                                <div class="col-sm-12">
                                    @include('alert_message')
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Update", ['class'=>'btn btn-primary pull-right']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- END Invoice -->


                {!! Form::close() !!}

            </div>
            </div>

        </div>
     <div class="row"  style="margin-top: 100px;">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-6">
                        {!! Form::open(['url'=>route('admin-send-order-smsEmail'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal', 'id'=>'create-category-form']) !!}
                        {{ Form::hidden('order_id',  $order->order_id ) }}
                        {{ Form::hidden('to',  $order->user->id ) }}
                        <div class="form-group col-lg-12">
                            {{ Form::label('message', 'SMS:', [ 'class'=>'col-sm-3 control-label' ]) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('message',  null,
                                [
                                    'class'=> 'form-control',
                                    'rows'=> '3',
                                    'maxlength'=> 160,
                                    'data-fv-notempty' => 'true',
                                    'data-fv-blank' => 'true',
                                    'data-fv-notempty-message' => 'Message is required'
                                ])}}
                            </div>
                        </div>
                        @include('alert_message')
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                {{ Form::submit("Send", ['class'=>'btn btn-primary']) }}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-lg-6">
                        <ul class="list list-activity push">

                            @foreach($order->sms as $sms)
                            <li>
                                <i class="si si-clock text-success"></i>
                                <div class="font-w600">{{ date("F j, Y g:i a", strtotime($sms->created_at)) }}</div>
                                <div><a href="javascript:void(0)">{{ $sms->content }}</a></div>
                            </li>
                             @endforeach

                        </ul>
                    </div>

                </div>
            </div>
        </div>

@endsection
