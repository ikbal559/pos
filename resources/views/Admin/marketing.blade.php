@extends('layouts.shop')
@section('title')
    @parent
    Marketing
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="box-header with-border push-20 push-20-t">
                            <form class="form-inline" method="get">
                                <a style="margin: 0 15px 0 5px; "  href="{{ url('admin/marketing') }}" class="btn btn-default"><span class="fa fa-close"></span></a>
                                <div class="form-group">
                                    {!! Form::select('user_type', [
                                    ''=> 'ALL Shops',
                                    'shop'=> 'Shop',
                                    'customer'=> 'Customer',
                                    'normal'=> 'Normal'
                                    ],  old('user_type', isset( $_GET['user_type'] )  ? $_GET['user_type']: null), ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <?php $business_types['']='ALL Business Types'?>
                                    {!! Form::select('business_types', $business_types,  old('business_types', isset( $_GET['business_types'] )  ? $_GET['business_types']: null), ['class' => 'form-control ']) !!}
                                </div>

                                <div class="form-group">
                                    <?php $category['']='ALL Product TYPE'?>
                                    {!! Form::select('category', $category,  old('category', isset( $_GET['category'] )  ? $_GET['category']: null), ['class' => 'form-control']) !!}
                                </div>



                                <div class="form-group">

                                        {{ Form::select('division', $division, isset( $_GET['division'] )  ? $_GET['division']: null,
                                        [
                                                'placeholder'=>'Select Your Division',
                                                'class'=>'form-control divisionSelect',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Division is required'
                                        ]) }}

                                </div>


                                <div class="form-group">

                                        {{ Form::select('district', $district, isset( $_GET['district'] )  ? $_GET['district']: null,
                                        [
                                                'placeholder'=>'Select Your District',
                                                'class'=>'form-control districtSelect',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'District is required'
                                        ]) }}

                                </div>

                                <div class="form-group">

                                        {{ Form::select('thana', $locations, isset( $_GET['thana'] )  ? $_GET['thana']: null,
                                        [
                                                'placeholder'=>'Select Your Thana',
                                                'class'=>'form-control thanaSelect',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Thana is required'
                                        ]) }}

                                </div>

                                <button style="margin: 0 15px;" type="submit" class="btn btn-warning">Search</button>

                            </form>

                        </div>
                        <div class="box-body">
                            <div class="scrollbox" style="max-height: 450px; overflow-y: scroll;">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                           <th>ID</th>
                                           <th>Name</th>
                                           <th>Phone</th>
                                           <th>Email</th>
                                           <th>Shop</th>
                                           <th>Address</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $totla = 0;?>
                                        @foreach($users as $user)
                                        <?php $totla++;?>
                                            <tr>
                                                <td>{{$user->username}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>{{$user->user_email}}</td>
                                                <td>@if(!empty($user->shop)) {{ $user->shop->name}} @endif</td>
                                                <td>@if(!empty($user->shop)) {{ $user->shop->address}} @endif</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div><!-- /.box-body -->
                        <div class="col-md-12">
                            <div class="push bg-white row ">
                                {{ $users->links() }}
                                <div class="clearfix h3 col-lg-6 push-20">
                                   Total User: {{ $totla}}
                                </div>
                            </div>
                        </div><!-- /.box-footer-->

                        @if($totla > 0):
                        <div class="col-md-12 push bg-white">
                        <div class="col-md-6">
                            {!! Form::open(['url'=>route('admin-send-sms'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}
                            <div class=" row ">
                                @foreach($users as $user)
                                    {{ Form::hidden('users[]',  $user->id ) }}
                                @endforeach
                                <div class="form-group col-lg-12">

                                    {{ Form::label('message', 'SMS:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                    <div class="col-sm-9">
                                        {{ Form::textarea('message',     null,
                                        [
                                            'class'=> 'form-control',
                                            'rows'=> '3',
                                            'maxlength'=> 160,

                                             'data-fv-notempty' => 'true',
                                                    'data-fv-blank' => 'true',
                                                    'data-fv-notempty-message' => 'Message is required'


                                        ]) }}
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    @include('alert_message')
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-right']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div><!-- /.box-footer-->

                            <div class="col-md-6">
                            {!! Form::open(['url'=>route('admin-send-email'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}
                            <div class=" row ">
                                @foreach($users as $user)
                                    {{ Form::hidden('users[]',  $user->id ) }}
                                @endforeach
                                <div class="form-group col-lg-12">
                                    {{ Form::label('message', 'Email:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                    <div class="col-sm-9">
                                        {{ Form::textarea('message',     null,
                                        [
                                            'class'=> 'form-control',
                                            'rows'=> '6',

                                             'data-fv-notempty' => 'true',
                                                    'data-fv-blank' => 'true',
                                                    'data-fv-notempty-message' => 'Message is required'


                                        ]) }}
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    @include('alert_message')
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-right']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div><!-- /.box-footer-->


                        </div><!-- /.box-footer-->
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer.scripts')
<script >
    $(document).ready(function () {
        $('.divisionSelect').change(function () {
            var division = $(this).val();
            if(division>0){
                $.ajax({

                    type: "POST",
                    cache: false,
                    url: '/api/location/get',
                    data: {id:division, type:'division_id'},
                    success: function(data){
                        $('.districtSelect').html(data);
                        $('.thanaSelect').html('');
                    },
                    dataType: "html"

                });
            }
        });

        $('.districtSelect').change(function () {
            var division = $(this).val();
            if(division>0){
                $.ajax({

                    type: "POST",
                    cache: false,
                    url: '/api/location/get',
                    data: {id:division, type:'district_id'},
                    success: function(data){
                        $('.thanaSelect').html(data);
                    },
                    dataType: "html"

                });
            }
        });
    });
</script>
@endpush