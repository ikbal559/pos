@extends('layouts.home')

@section('content')
        <!-- Main Container -->
        <main id="main-container " class="clearfix">

            <!-- Team -->
            <div class="bg-white push-20">
                <section class="content content-boxed">
                    <!-- Section Content -->
                    <div class="push-20">
                        <div class="row items-push-2x">
                            <div class="col-md-4 visibility-hidden" data-toggle="appear" data-class="animated zoomIn">
                                <div class="block">
                                    <div class="block-header">
                                        <h3 class="block-title">Customer Care</h3>
                                    </div>
                                    <div class="block-content">
                                        <p>Gulfesha Plaza (9th Floor)<br>
                                            Room No # B-69<br>
                                            Outer Circular Road<br>
                                            Moghbazar, Dhaka-1217.<br>
                                            Mobile : 01848304555 </p>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <!-- END Section Content -->
                </section>
            </div>
            <!-- END Team -->
        </main>
        <!-- END Main Container -->


@endsection
