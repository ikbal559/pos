@extends('layouts.app')
@section('title')
    @parent
    Manager
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Manager</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    {!! Form::open(['url'=>route('api.admin.manage.manager'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}
                                    {{ Form::hidden('user_id', $user_id  ,['class'=>'form-control']) }}
                                    @foreach($groups as $group)


                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <label class="css-input css-checkbox css-checkbox-primary">
                                                    <input name="permission[{{$group->permission_group_id}}]" @if(in_array($group->permission_group_id, $permissions)) checked @endif type="checkbox" value="1"><span></span> {{$group->group}}
                                                </label>

                                            </div>
                                        </div>

                                    @endforeach


                                    @include('alert_message')

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-primary']) }}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
