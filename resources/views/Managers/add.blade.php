@extends('layouts.app')
@section('title')
    @parent
    New Manager
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">New Manager</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    {!! Form::open(['url'=>route('api.admin.add.manager'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}


                                    <div class="form-group">
                                        {{ Form::label('name', 'Name:', ['class'=>'col-sm-3 control-label']) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('name',  null,
                                            [
                                                'class'=>'form-control',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Name is required'
                                            ]) }}
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        {{ Form::label('username', 'Username:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('username',    null,
                                            [
                                                'autocomplete'=>'off',
                                                'placeholder'=>'Email ',
                                                'class'=>'form-control',
                                                'data-fv-number' => 'true',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Username is required'
                                            ]) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('password', 'Password:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::password('password',
                                            [
                                                'class'=>'form-control',
                                                'data-fv-number' => 'true',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Password is required'
                                            ]) }}
                                        </div>
                                    </div>

                                    @include('alert_message')

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-primary']) }}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
