@extends('layouts.home')
@section('content')
    <div class="bg-white push-10">
        <section class="content content-boxed">
            <div class="push-20">
                <h3 class="homeCategoryTitle">
                    Your Order
                </h3>

                <div class="row items-push-2x">
                    <div class="col-lg-9">
                        <div class="table-responsive push-50 add-product-url" data-add-product-url="{{ route('order-new') }}">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 50px;"></th>
                                        <th>Product</th>
                                        <th class="text-center" style="width: 80px;">Quantity</th>
                                        <th class="text-right" style="width: 80px;">Price</th>
                                        <th class="text-right" style="width: 130px;">Total</th>
                                    </tr>
                                </thead>
                                <tbody class="posProductItem" data-read = "{{ route('api.read.cart') }}" >
                                <tr><td colspan="5" class="text-center">No Item</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <button class="CartItemTotal btn btn-default btn-lg" style="width: 100%; max-width: 220px; margin: 0 auto 5px; display: block;">0</button>
                        <button class="CartTotal btn btn-default btn-lg" style="width: 100%; max-width: 220px; margin: 0 auto 15px; display: block;">0</button>
                        <a href="{{ route('submit-order') }}" class="btn btn-warning btn-lg" style="width: 100%; padding: 25px 15px;  max-width: 220px; margin: 0 auto 10px; display: block;">Submit Order</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('footer.scripts')
<script src="{{ asset("js/product/sales.js") }}"></script>
@endpush
