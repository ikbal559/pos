@extends('layouts.home')
@section('content')
    <div class="bg-white push-10">
        <section class="content content-boxed">
            <div class="push-20">
                <h3 class="homeCategoryTitle">Search By <span class="text-warning">{{$search_query}}</span></h3>
                <div class="row items-push-2x">
                        @foreach($products as $product)
                        @include('product_list_view', ['product' => $product])
                        @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
