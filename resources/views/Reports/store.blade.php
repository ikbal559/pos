@extends('layouts.shop')
@section('title')
    @parent
    Store Report
@stop
@section('content')
<div class="block">
    <div class="block-content">
        <div class="mobilePadding0 col-lg-12 pull-left">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">

                    <div class="panel-heading clearfix">
                        <div class="h4 pull-left ">Store Report</div>
                    </div>
                    <div class="panel-body mobilePadding0">
                        <div class="table-responsive">
                            <table class="table table-striped" id="report-store-data-table">
                                <thead>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Import</th>
                                    <th>Export</th>
                                    <th>Damage</th>
                                    <th>Sales</th>
                                    <th>Balance</th>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/datatable/tableFactory.js")}}"></script>
@endpush