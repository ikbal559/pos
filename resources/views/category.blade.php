@extends('layouts.home')
@section('content')
    <div class="bg-white push-10">
        <section class="content content-boxed">
            <div class="push-10-t">
                <h3 class="homeCategoryTitle">
                    {{$category->category_name}}
                </h3>
                <div class="row items-push-2x">
                        @foreach($products as $product)
                            @include('product_list_view', ['product' => $product])
                        @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
