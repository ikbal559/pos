@extends('layouts.shop')
@section('title')
    @parent
    Store In List
@stop
@section('content')
    <div class="row"  style="margin-top: 10px;">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-lg-2">
                        <span class="btn btn-success push-10-t push-10-l ">Total:  {{ number_format($total, 0) }}</span>
                    </div>
                    <div class="col-lg-10">
                        <div class="box-header with-border push-10 push-10-t push-15-r pull-right">
                            <form class="form-inline" method="get">
                                 <div class="form-group">
                                   <a href="{{ route('store-in-list') }}" class="fa fa-close push-5-r"></a>
                                </div>
                                <div class="form-group">
                                    {{ Form::select('client_id', $clients, null,
                                    [
                                        'class'=>'form-control',
                                        'placeholder'=>'Select a Client',
                                        'data-fv-notempty' => 'true',
                                        'data-fv-blank' => 'true',
                                        'data-fv-notempty-message' => 'Client is required'
                                    ]) }}
                                </div>
                                <div class="form-group">
                                        {{ Form::select(
                                            'product_id', $products,  (!empty($entry)) ? $entry->product_id : null,
                                        [
                                            'placeholder'=> 'Select a Product',
                                            'class'=>'form-control ProductSelectBoxGlobal',
                                            'data-fv-number' => 'true',
                                            'data-fv-notempty' => 'true',
                                            'data-fv-blank' => 'true',
                                            'data-fv-notempty-message' => 'Product is required'
                                        ]) }}
                                </div>
                                <div class="form-group">
                                    <input value="{{ isset( $_GET['start_date'] )  ? $_GET['start_date']: null }}",  data-date-end-date="0d" class="js-datepicker form-control" name="start_date"  data-date-format="yyyy-mm-dd" placeholder="From: yyyy-mm-dd">
                                </div>
                                <div class="form-group">
                                    <input value="{{isset( $_GET['end_date'] )  ? $_GET['end_date']: null}}" data-date-format="yyyy-mm-dd"  class="js-datepicker form-control" name="end_date" placeholder="TO: yyyy-mm-dd">
                                </div>
                                <button type="submit" class="btn btn-default">Search</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if(!empty($StoreProducts))
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Client</th>
                                <th>Product</th>
                                <th>V No</th>
                                <th>Qty</th>
                                <th>Amount</th>
                                <th>Total</th>
                                <th>AVG</th>
                                <th>In Store</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($StoreProducts as $item)
                                <tr>
                                    <td><a class="btn btn-info btn-xs" href="{{ route('store-in-edit', $item->store_product_id ) }}">Edit</a></td>
                                    <td>{{ ($item->client) ?  $item->client->name : '' }}</td>
                                    <td>{{ $item->product->name  }} - {{ $item->product->size_model }}</td>

                                    <td>{{ $item->v_no }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->total }}</td>
                                    <td>{{ $item->avg_store_in }}</td>
                                    <td>{{ $item->total_store_in }}</td>
                                    <td>{{  date("F j, Y g:i a", strtotime($item->created_at)) }}</td>

                                </tr>
                            @endforeach



                        </tbody>
                    </table>
                    @else
                        <h4 class="text-center"> No Data Found!</h4>
                    @endif
                    <div class="col-md-12">
                        <div class="col-md-6 pull-right">
                            {{ $StoreProducts->links() }}
                        </div>
                    </div><!-- /.box-footer-->
                </div>
            </div>

        </div>
    </div>
@endsection
