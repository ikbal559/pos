@extends('layouts.shop')
@section('title')
    @parent
    Create Product
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Create New Product</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    @include('Product.partials.product',
                                    [
                                            'url' => route('api.product.store')
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
