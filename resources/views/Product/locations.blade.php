@extends('layouts.app')
@section('title')
    @parent
    Locations
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="h4 pull-left ">Locations</div>
                            <a href="{{ route('location-add')  }}" class="pull-right btn btn-info"><span class="fa fa-plus"></span> NEW</a>
                        </div>
                        <div class="panel-body mobilePadding0">
                            <div class="table-responsive">
                                <table class="table table-striped" id="location-data-table">
                                    <thead>
                                        <th>ID</th>
                                        <th>Division</th>
                                        <th>District</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/datatable/tableFactory.js")}}"></script>
@endpush