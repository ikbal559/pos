@extends('layouts.shop')
@section('title')
    @parent
    Create Category
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Store IN Edit</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::open(['url'=>route('api.store.in.update'), 'method'=>'POST', 'class'=>'form-vertical-storeIn form-horizontal', 'id'=>'product-form' ]) !!}
                        {{ Form::hidden('store_product_id',  $store_in->store_product_id ) }}
                        <div class="row">
                            <div class="col-md-12">
                                 <p>Product : {{ $store_in->product->name }}</p>
                                 <p>Client : {{ $store_in->client->name }}</p>
                                 <p>Date  : {{ $store_in->created_at }}</p>
                            </div>

                        <div class="row">
                            <div class="col-lg-12">

                                <div class="col-lg-4">
                                     <div class="form-group">
                                         {{ Form::label('price', 'Buy Price:', ['class'=>'col-sm-3 control-label']) }}
                                         <div class="col-sm-9">
                                             {{ Form::number('price', $store_in->price,
                                             [
                                                 'class'=>'form-control',
                                                 'data-fv-notempty' => 'true',
                                                 'data-fv-blank' => 'true',
                                                 'data-fv-notempty-message' => 'Price is required',
                                                 'min' =>'0',
                                                 'step' =>'0.01',
                                             ]) }}
                                         </div>
                                     </div>
                                 </div>

                                <div class="col-lg-2">
                                    <div class="form-group ">
                                        {{ Form::label('qty', 'Qty:', ['class'=>'col-sm-3 control-label']) }}
                                        <div class="col-sm-9">
                                            {{ Form::number('qty', $store_in->qty,
                                            [
                                                'class'=>'form-control',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Quantity is required',
                                                'min' =>'0',
                                                'step'=>'0.5'
                                            ]) }}
                                        </div>
                                    </div>
                                 </div>

                            </div>
                        </div>
                            <div class="col-lg-12">
                                <div class="col-lg-4 productINfo">
<?php
                                    $product = $store_in->product;
                                    $volume =  $product->total_store_in * $product->avg_store_in;
                                    $html  = '<div class="list-group">';
                                        $html  .= '<a class="list-group-item active" href="javascript:void(0)" >';
                                            $html  .= $product->name;
                                            $html  .= '</a>';

                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html  .= "Last Qty: ". $product->total_store_in;
                                            $html  .= '</a>';
                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html .= "AVG Price: ". $product->avg_store_in;
                                            $html  .= '</a>';
                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html .= "Total Volume: $volume";
                                            $html  .= '</a>';
                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html .= "MRP: {$product->mrp}";
                                            $html  .= '</a>';
                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html .= "Dealer Price: {$product->price}";
                                            $html  .= '</a>';
                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html .= "Sales Price: {$product->sale}";
                                            $html  .= '</a>';

                                        $html  .= '<a class="list-group-item" href="javascript:void(0)">';
                                            $html .= "Point: ". $product->point;
                                            $html  .= '</a>';

                                        $html .= '</div>';
                                    echo  $html;
?>

                                </div>
                                <div class="col-lg-4 productINfoSuccess"></div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-lg btn-info pull-right clearfix']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/product/sales_in.js")}}"></script>
@endpush