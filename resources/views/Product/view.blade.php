@extends('layouts.shop')
@section('title')
    @parent
    View Product
@stop
@section('content')
    @if( (!empty($product))  )
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">

                <div class="panel-heading clearfix">
                    <h5 class="pull-left">{{ $product->name }} </h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                                <table class="table">
                                                    <tr>
                                                        <td>Product Model/Size</td>
                                                        <td>{{ $product->size_model }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Market Price</td>
                                                        <td>{{ $product->mrp }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sale Rate</td>
                                                        <td>{{ $product->mrp }}</td>
                                                    </tr>


                                                </table>

                                        </div>
                                        <div class="col-md-6">
                                                <table class="table">

                                                    <tr>
                                                        <td>Sale Rate</td>
                                                        <td>{{ $product->sale  }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dealer Price</td>
                                                        <td> {{ $product->price }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>In Sotore</td>
                                                        <td>{{ $product->total_store_in  }}</td>
                                                    </tr>

                                                </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Latest Store IN</h5>
                    <span class="pull-right btn btn-info">AVG Price: {{$product->avg_store_in }}</span>
                </div>
                <div class="panel-body">

                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="panel-body mobilePadding0">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="products-data-table">
                                            <thead>
                                            <th>V No</th>
                                            <th>Qty</th>
                                            <th>Amount</th>
                                            <th>Total</th>
                                            <th>Date</th>
                                            </thead>

                                            <tbody>
                                                @foreach($storeInProduct as $item)
                                                    <tr>
                                                        <td>{{ $item->v_no }}</td>
                                                        <td>{{ $item->qty }}</td>
                                                        <td>{{ $item->price }}</td>
                                                        <td>{{ $item->total }}</td>
                                                        <td>{{ $item->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>
                        </div>

                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Latest Sales</h5>
                    <span class="pull-right btn btn-info">Sales: {{$product->sales->sum('total') }}</span>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="panel-body mobilePadding0">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="products-data-table">
                                            <thead>
                                            <th>ID</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                            <th>Point</th>
                                            <th>Date</th>
                                            </thead>

                                            <tbody>
                                            @foreach($lestSaleProduct as $item)
                                                <tr>
                                                    <td>{{ $item->order_id }}</td>
                                                    <td>{{ $item->qty }}</td>
                                                    <td>{{ $item->price }}</td>
                                                    <td>{{ $item->total }}</td>
                                                    <td>{{ $item->point }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
