{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal', 'id'=>'create-category-form']) !!}

    {{ Form::hidden('category_id', (!empty($category)) ? $category->category_id : null  ,['class'=>'form-control']) }}

    <div class="form-group">
        {{ Form::label('name', 'Name:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::text('category_name', (!empty($category)) ? $category->category_name : null ,
            ['class'=>'form-control',  'data-fv-notempty' => 'true', 'data-fv-blank' => 'true', 'data-fv-notempty-message' => 'Name is required']
            )}}
        </div>
    </div>
@if(empty($category) )
    <div class="form-group">
        {{ Form::label('parent_id', 'Parent:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::select('parent_id', $category_parents, (!empty($category)) ? $category->parent_id : 0,
            ['class'=>'form-control', 'placeholder'=>'Main Category',  ]
            )}}
        </div>
    </div>
@endif
    <div class="form-group">
        {{ Form::label('ordering', 'Ordering:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::date('ordering', (!empty($category)) ? $category->ordering : null ,
            ['class'=>'form-control', 'placeholder'=>'Ordering',  ]
            )}}
        </div>
    </div>
@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Save Category", ['class'=>'btn btn-primary']) }}
    </div>
</div>
{!! Form::close() !!}