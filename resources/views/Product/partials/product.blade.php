{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal', 'id'=>'product-form' ]) !!}
{{ Form::hidden('product_id', (!empty($product)) ? $product->product_id : null  ) }}
{{ Form::hidden('shop_id',  Auth::user()->shop_id ) }}
<div class="row">
    <div class="col-md-9">

        <div class="form-group ">
            {{ Form::label('name', 'Product Name:', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {{ Form::text('name', (!empty($product)) ? $product->name : null,
                [
                    'class'=>'form-control',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Name is required'
                ]) }}
            </div>
        </div>

        <div class="form-group ">
            {{ Form::label('size_model', 'Product Model/Size:', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {{ Form::text('size_model', (!empty($product)) ? $product->size_model : null,
                [
                    'class'=>'form-control'
                ]) }}
            </div>
        </div>
<hr>
        <div class="form-group ">
            {{ Form::label('mrp', 'Market Price:', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {{ Form::number('mrp', (!empty($product)) ? $product->mrp : null,
                [
                    'class'=>'form-control',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Market Price is required',
                    'min' =>'0',
                    'step' =>'0.01',
                ]) }}
            </div>
        </div>
        <div class="form-group ">
            {{ Form::label('sale', 'Sale Rate:', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {{ Form::number('sale', (!empty($product)) ? $product->sale : null,
                [
                    'class'=>'form-control',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Sale Rate is required',
                    'min' =>'0',
                    'step' =>'0.01',
                ]) }}
            </div>
        </div>
        <div class="form-group ">
            {{ Form::label('price', 'Dealer Price:', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {{ Form::number('price', (!empty($product)) ? $product->price : null,
                [
                    'class'=>'form-control',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Dealer Price is required',
                    'min' =>'0',
                    'step' =>'0.01',
                ]) }}
            </div>
        </div>
<hr>
        <div class="form-group ">
            {{ Form::label('code', 'Product Code:', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {{ Form::text('code', (!empty($product)) ? $product->code : null,
                [
                    'class'=>'form-control',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Code is required'
                ]) }}
            </div>
        </div>

    </div>
</div>
@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-1 col-sm-9">
        {{ Form::submit("Save Product", ['class'=>'btn btn-primary pull-left clearfix']) }}
    </div>
</div>
{!! Form::close() !!}
