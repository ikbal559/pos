{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}

    {{ Form::hidden('business_types_id', (!empty($data)) ? $data->business_types_id : null  ,['class'=>'form-control']) }}

    <div class="form-group">
        {{ Form::label('name', 'Name:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::text('name', (!empty($data)) ? $data->name : null ,
            ['class'=>'form-control',  'data-fv-notempty' => 'true', 'data-fv-blank' => 'true', 'data-fv-notempty-message' => 'Name is required']
            )}}
        </div>
    </div>

@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Save", ['class'=>'btn btn-primary']) }}
    </div>
</div>
{!! Form::close() !!}