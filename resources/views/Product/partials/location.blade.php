{!! Form::open(['url'=>route('api.manage.location'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal']) !!}

    {{ Form::hidden('location_id', (!empty($data)) ? $data->location_id : null  ,['class'=>'form-control']) }}

    <div class="form-group">
        {{ Form::label('division_id', 'Division:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::select('division_id', $division, (!empty($data)) ? $data->division_id : null ,
            ['class'=>'form-control', 'placeholder'=>'Select Division',  ]
            )}}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('district_id', 'District:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::select('district_id', $district, (!empty($data)) ? $data->district_id : null ,
            ['class'=>'form-control ', 'placeholder'=>'Select District',  ]
            )}}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Name:', ['class'=>'col-sm-3 control-label']) }}
        <div class="col-sm-9">
            {{ Form::text('name', (!empty($data)) ? $data->name : null ,
            ['class'=>'form-control',  'data-fv-notempty' => 'true', 'data-fv-blank' => 'true', 'data-fv-notempty-message' => 'Name is required']
            )}}
        </div>
    </div>
@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Save", ['class'=>'btn btn-primary']) }}
    </div>
</div>
{!! Form::close() !!}