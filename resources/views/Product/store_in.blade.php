@extends('layouts.shop')
@section('title')
    @parent
    Create Category
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Store IN</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::open(['url'=>route('api.store.in.save'), 'method'=>'POST', 'class'=>'form-vertical-storeIn form-horizontal', 'id'=>'product-form' ]) !!}
                        {{ Form::hidden('shop_id',  Auth::user()->shop_id ) }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-12 onChangeUrl" data-onChangeUrl="{{ route('get-product-info') }}" >
                                        {{ Form::select(
                                            'product_id', $products,  (!empty($entry)) ? $entry->product_id : null,
                                        [
                                            'placeholder'=> 'Select a Product',
                                            'class'=>'form-control ProductSelectBoxGlobal StoreInOnChange',
                                            'data-fv-number' => 'true',
                                            'data-fv-notempty' => 'true',
                                            'data-fv-blank' => 'true',

                                            'data-fv-notempty-message' => 'Product is required'
                                        ]) }}
                                    </div>
                                </div>
                            </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        {{ Form::label('price', 'Client:', ['class'=>'col-sm-3 control-label']) }}
                                        <div class="col-sm-9">
                                            {{ Form::select('client_id', $clients, null,
                                            [
                                                'class'=>'form-control',
                                                'placeholder'=>'Select a Client',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Client is required'
                                            ]) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                     <div class="form-group">
                                         {{ Form::label('price', 'Buy Price:', ['class'=>'col-sm-3 control-label']) }}
                                         <div class="col-sm-9">
                                             {{ Form::number('price', null,
                                             [
                                                 'class'=>'form-control',
                                                 'data-fv-notempty' => 'true',
                                                 'data-fv-blank' => 'true',
                                                 'data-fv-notempty-message' => 'Price is required',
                                                 'min' =>'0',
                                                 'step' =>'0.01',
                                             ]) }}
                                         </div>
                                     </div>
                                 </div>

                                <div class="col-lg-2">
                                    <div class="form-group ">
                                        {{ Form::label('qty', 'Qty:', ['class'=>'col-sm-3 control-label']) }}
                                        <div class="col-sm-9">
                                            {{ Form::number('qty', null,
                                            [
                                                'class'=>'form-control',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Quantity is required',
                                                'min' =>'0',
                                                'step'=>'0.5'
                                            ]) }}
                                        </div>
                                    </div>
                                 </div>
                                <div class="col-lg-2">
                                    <div class="form-group ">
                                        {{ Form::label('v_no', 'V.No.:', ['class'=>'col-sm-3 control-label']) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('v_no', null,
                                            [
                                                'class'=>'form-control',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Invoice No is required',

                                            ]) }}
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                            <div class="col-lg-12">
                                <div class="col-lg-4 productINfo"></div>
                                <div class="col-lg-4 productINfoSuccess"></div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-lg btn-info pull-right clearfix']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/product/sales_in.js")}}"></script>
@endpush