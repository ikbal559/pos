@extends('layouts.app')
@section('title')
    @parent
    Business Type
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="h4 pull-left ">Business Type</div>
                            <a href="{{ route('product-business-type-add')  }}" class="pull-right btn btn-info"><span class="fa fa-plus"></span> NEW</a>
                        </div>
                        <div class="panel-body mobilePadding0">
                            <div class="table-responsive">
                                <table class="table table-striped" id="business-data-table">
                                    <thead>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/datatable/tableFactory.js")}}"></script>
@endpush