@extends('layouts.shop')
@section('title')
    @parent
    Products
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">

                        <div class="panel-heading clearfix">
                            <div class="h4 pull-right ">All Products</div>
                            <a href="{{ route('product-add')  }}" class="pull-left btn btn-info"><span class="fa fa-plus"></span> NEW</a>
                            <span  class="btn btn-success push-10-l">Volume: {{   number_format($totalVolume, 0)  }}</span>
                        </div>
                            @if(Auth::user()->user_type == 'admin')
                        <div class="row">
                            <div class="col-lg-6 push-10-l push-10-t">

                                <form class="form-inline" method="get">


                                    <div class="form-group">
                                        {{ Form::select('shop_id', $shops, null,
                                            ['class'=>'form-control ShopID', 'placeholder'=>'Search By Shop',  ]
                                            )}}
                                    </div>
                                    <button type="submit" class="btn btn-default">Search</button>
                                    <div class="form-group">
                                        <a href="{{ route('product-list') }}" class="fa fa-close push-5-r"></a>
                                    </div>
                                </form>
                            </div>

                        </div>
                            @else
                                <input type="hidden" class="ShopID" value="0">
                             @endif

                        <div class="panel-body mobilePadding0">
                            <div class="table-responsive">
                                <table class="table table-striped" id="products-data-table">
                                    <thead>
                                        <th>Action</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>In Store</th>
                                        <th>AVG Price</th>
                                        <th>Total Volume</th>
                                        <th>MRP</th>
                                        <th>Sale</th>
                                        <th>Dealer</th>
                                        <th>Point</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer.scripts')
<script>
    var base_url = window.base_url + '/load/';
    var oTable =  $('#products-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:  base_url + 'Products/Products' ,
            data: function (d) {
                d.shop_id =  $('.ShopID').val()
            }
        },
        columns: [
            { data: 'action', name: 'action' },
            { data: 'code', name: 'code' },
            { data: 'name', name: 'name' },
            { data: 'total_store_in', name: 'total_store_in' },
            { data: 'avg_store_in', name: 'avg_store_in' },
            { data: 'total_volume', name: 'total_volume' },
            { data: 'mrp', name: 'mrp' },
            { data: 'sale', name: 'sale' },
            { data: 'price', name: 'price' },
            { data: 'point', name: 'point' },
        ]
    });

//    $('.userTypeClick').click(function () {
//        $('.homeProduct').val('');
//        $('.userType').val($(this).attr('data-type'));
//        oTable.draw();
//    });
//
//    $('.homeProductClick').click(function () {
//        $('.userType').val('');
//        $('.homeProduct').val($(this).attr('data-type'));
//        oTable.draw();
//    });
</script>
@endpush