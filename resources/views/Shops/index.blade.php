@extends('layouts.app')
@section('title')
    @parent
    Shops
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="clearfix push-5-t">
                            <div class="col-sm-6 col-lg-3">
                                <a class="btn btn-lg btn-info" href="{{ route('shops-add')  }}"><span class="fa fa-plus"></span> Add Shop</a>
                            </div>

                            <input type="hidden" value="" class="userType" name="userType">
                        </div>
                        <div class="panel-body mobilePadding0">
                            <div class="table-responsive">
                                <table class="table table-striped" id="shops-data-table">
                                    <thead>
                                        <th>Action</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Shop</th>
                                        <th>Address</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer.scripts')
<script>
    var base_url = window.base_url + '/load/';
    var oTable =  $('#shops-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:  base_url + 'Shops/Shops' ,
            data: function (d) {
                d.user_type =  $('.userType').val();
            }
        },
        columns: [
            { data: 'action', name: 'action' },
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'phone', name: 'phone' },
            { data: 'user_shop', name: 'user_shop' },
            { data: 'address', name: 'address' },

        ]
    });
    $('.userTypeClick').click(function () {
        $('.userType').val($(this).attr('data-type'));
        oTable.draw();
    });
</script>
@endpush