@extends('layouts.app')
@section('title')
    @parent
    Create Shop
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Create New Shop</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    @include('Shops.partials.shop_add',
                                    [
                                            'url' => route('api.shop.add')
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
