{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal' ]) !!}

        {{ Form::hidden('shop_id', (!empty($shop)) ? $shop->shop_id : null  ,['class'=>'form-control']) }}

        <div class="form-group">
            {{ Form::label('name', 'Shop Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('name',  (!empty($shop)) ? $shop->name : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Name is required'
                ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('phone', 'Phone:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('phone',  (!empty($shop)) ? $shop->phone : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Phone is required'
                ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('address', 'Address:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::textarea('address',  (!empty($shop)) ? $shop->address : null,
                [
                    'class'=>'form-control',
                    'rows'=> 3,
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Address is required'
                ]) }}
            </div>
        </div>
<hr>

        <div class="form-group">
            {{ Form::label('owner', 'Owner Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('owner',    null,
                [
                    'autocomplete'=>'off',
                    'placeholder'=>'Owner Name ',
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Address is required'
                ]) }}
            </div>
        </div>


        <div class="form-group">
            {{ Form::label('username', 'Username:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('username',    null,
                [
                    'autocomplete'=>'off',
                    'placeholder'=>'Username ',
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Username is required'
                ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Password:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::password('password',
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Password is required'
                ]) }}
            </div>
        </div>

@include('alert_message')

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-left clearfix']) }}
    </div>
</div>

{!! Form::close() !!}

