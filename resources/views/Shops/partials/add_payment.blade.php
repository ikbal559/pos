{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal' ]) !!}

    {{ Form::hidden('transaction_shop_payment_shop_id', (!empty($shop)) ? $shop->shop_id : null  ,['class'=>'form-control']) }}

        <div class="form-group">
            <div class=" col-md-offset-3 col-md-9">
                <h4>{{ $shop->name }}</h4>
                <h5>{{ $shop->phone }}</h5>
                <h6>{{ $shop->address }}</h6>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('transaction_shop_payment_amount', 'Amount:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::number('transaction_shop_payment_amount',  null,
                [
                    'class'=>'form-control',
                    'min'=>'9',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Amount is required'
                ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('transaction_shop_payment_shop_description', 'Payment Description:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('transaction_shop_payment_shop_description',  null,
                [
                    'class'=>'form-control'
                ]) }}
            </div>
        </div>


@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-left clearfix']) }}
    </div>
</div>
{!! Form::close() !!}

