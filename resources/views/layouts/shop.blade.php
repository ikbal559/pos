<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-focus">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="author" content="Md. Lutfur Rahman">
    <meta name="description" content="Paikarimarketbd.com is the largest Online Wholesale Shop and becoming a leading online shop brand of Bangladesh. Here customer can find all type of essential, high-valued, quality, exclusive products and 100% original">
    <meta name="keywords" content="paikarimarketbd, Wholesale Market in Bangladesh, Paikari, market">

    <title>
        @section('title')
            BD Counting com
        @show
    </title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oneui.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/datatableStyle.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/media.css') }}" rel="stylesheet">

    <link href="{{ asset('css/backend/media.css') }}" rel="stylesheet">
    <link href="{{ asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker3.css') }}" rel="stylesheet">


    @stack('head.stylesheets')
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
        window.base_url= '{{ route('home')  }}';
    </script>
</head>
<body>
    <div id="app">
        <div id="page-container" class="sidebar-l sidebar-mini sidebar-o side-scroll header-navbar-fixed   header-navbar-transparent">
           @if( Auth::user()->user_type =='shop')
                @include('include.nav')
                @include('include.header')
            @else
                @include('include.nav_admin')
                @include('include.header_admin')
            @endif
                <main id="main-container">
                    <!-- Page Content -->
                    <div class="content">
                        @yield('content')
                    </div>
                    <!-- END Page Content -->
                </main>
                <!-- END Main Container -->
            @include('include.footer')
        </div>
    </div>

    <script src="{{ asset('js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.scrollLock.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.countTo.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.placeholder.min.js') }}"></script>
    <script src="{{ asset('js/core/js.cookie.min.js') }}"></script>

    <script src="{{ asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('js/one.js') }}"></script>

    <script src="{{asset("/js/datatable/jquery.dataTables.min.js")}}"></script>

    <script type="text/javascript" src="{{ asset("vendor/formValidation/js/formValidation.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("vendor/formValidation/js/framework/bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{asset("js/global_validation.js")}}"></script>

    <script type="text/javascript" src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{asset("js/backend/custom.js")}}"></script>
    <!-- Page JS Code -->
    <script>
        $(function () {
            // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
            App.initHelpers(['datepicker']);
        });
    </script>

    @stack('footer.scripts')
</body>
</html>
