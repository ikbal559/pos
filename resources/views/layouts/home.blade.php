<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="author" content="Md. Lutfur Rahman">
    <meta name="description" content="Paikarimarketbd.com is the largest Online Wholesale Shop and becoming a leading online shop brand of Bangladesh. Here customer can find all type of essential, high-valued, quality, exclusive products and 100% original">
    <meta name="keywords" content="paikarimarketbd, Wholesale Market in Bangladesh, Paikari, market">

    <title>
        @section('title')
           Shop
        @show
    </title>
    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oneui.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend/media.css') }}" rel="stylesheet">


    @stack('head.stylesheets')
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="page-container" class=" side-scroll header-navbar-fixed header-navbar-transparent">





    @include('include.home.header')
    @yield('content')
    @include('include.home.footer')
    </div>


    <script src="{{ asset('js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/core/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.scrollLock.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.countTo.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.placeholder.min.js') }}"></script>
    <script src="{{ asset('js/core/js.cookie.min.js') }}"></script>
    <script src="{{ asset('js/one.js') }}"></script>


    <script type="text/javascript" src="{{ asset("vendor/formValidation/js/formValidation.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("vendor/formValidation/js/framework/bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{asset("js/global_validation.js")}}"></script>


    @stack('footer.scripts')

    <!-- Page JS Code -->
    <script>
        $(function () {
            // Init page helpers (Appear + CountTo plugins)
            App.initHelpers(['appear', 'appear-countTo']);
        });
    </script>
</body>
</html>
