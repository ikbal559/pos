@extends('layouts.shop')
@section('title')
    @parent
  Dealer Sales
@stop
@section('content')
    <div class="row push-100-t">
        <div class="col-md-8 col-sm-8 col-xs-12 ">
            <div class="posBox clearfix" >
                <div class="spinner hidden" id="ajax-loader">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                </div>
                        <div class="text-right h2 text-success text-uppercase">dealer Sales</div>
                <div class="form-group add-product-url-code" data-add-product-url-code="{{ route('api.add.to.cartCode.dealer')  }}" >

                        {{ Form::text(
                            'code',   null,
                        [
                            'placeholder'=> 'Product Code',
                            'value'=> '',
                            'autofocus'=> 'autofocus',
                            'autocomplete'=> 'off',
                            'class'=>'form-control ProductSelectBoxCode'
                        ]) }}

                </div>

                <div class="form-group add-product-url" data-add-product-url="{{ route('api.add.to.cart.dealer')  }}" >
                    {{ Form::select(
                        'product_id', $products,  null,
                    [
                        'placeholder'=> 'Select a Product',
                        'class'=>'form-control ProductSelectBox',
                        'data-fv-number' => 'true',
                        'data-fv-notempty' => 'true',
                        'data-fv-blank' => 'true',
                        'data-fv-notempty-message' => 'Product is required'
                    ]) }}
                </div>
            </div>

            <div class="posBox clearfix push-5-t">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-left" style="width: 20px; text-transform: capitalize!important;"></th>
                        <th style="text-transform: capitalize!important;">Product</th>
                        <th class="text-left" style="width: 40px;  text-transform: capitalize!important;">InStock</th>
                        <th class="text-left" style="width: 40px; text-transform: capitalize!important;">Price</th>
                        <th class="text-center" style="width: 20px;  text-transform: capitalize!important;">Qty.</th>
                        <th class="text-left" style="width: 40px;  text-transform: capitalize!important;">Point</th>
                        <th class="text-right" style="width: 90px;  text-transform: capitalize!important;">Total</th>
                    </tr>
                    </thead>
                    <tbody class="posProductItem" data-read = "{{ route('api.read.cart.dealer') }}" >
                    <tr><td colspan="5" class="text-center">No Item</td></tr>
                    </tbody>
                </table>
                <a class="" href="{{ route('api.empty.cart.dealer') }}"><span class="fa fa-remove"></span> Empty Cart</a>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="posBox clearfix push-5-t">
                <span class="getURLorder" data-action="{{ route('get-order-data.dealer') }}"></span>
                {!! Form::open(['url'=>route('api.complete.cart.dealer'), 'method'=>'POST', 'class'=>'form-vertical-sales form-horizontal' ]) !!}
                    <table class="block-table text-center">
                    <tbody>
                    {{--<tr>--}}
                    {{--<td style="width: 50%;"><div class="push-10 push-10-t">Set Discount</div></td>--}}
                    {{--<td  style="width: 50%;"><input class="form-control CartDiscount" data-last="0" value="0" type="number"></td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td style="width: 50%;"><div class="push-10-t text-right">Total Quantity</div></td>
                        <td style="width: 50%;"><div class="h4 font-w700 CartItemTotal">0</div></td>
                    </tr>
                    <tr>
                        <td style="width: 50%;"><div class=" push-20 text-right">Total Point</div></td>
                        <td style="width: 50%;"><div class="h4 font-w700 CartTotalPoint push-20">0</div></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border: 2px dotted #ddd;" ><div class="h1  font-w700 text-smooth text-center CartTotal ">0<span style="font-size: 12px;">tk</span></div></td>
                    </tr>
                    <tr class="customerArea">
                        <td colspan="2">
                            <div class="form-group push-10-t" >
                                <div class="col-sm-12">
                                    {{ Form::text('customer',    null,
                                    [
                                        'autocomplete'=>'off',
                                        'placeholder'=>'Enter Customer Number',
                                        'data-cus_url'=> route('get-customer.dealer'),
                                        'class'=>'form-control onChangeCustomerSales salesCustomer'

                                    ]) }}
                                </div>
                            </div>
                            <div class="customerInfoSales">
                                <div class="form-group"  style="margin-bottom: 10px;" >
                                    {{ Form::label('', 'Name:', [ 'class'=>'col-sm-4 control-label' ]) }}
                                    <div class="col-sm-8 text-left cusName" style="padding-top: 7px;"></div>
                                </div>
                                <div class="form-group" style="margin-bottom: 5px;" >
                                    {{ Form::label('', 'Previous Due:', [ 'class'=>'col-sm-4 control-label ' ]) }}
                                    <div class="col-sm-8 text-left cusDue text-warning font-w600 h4" style="padding-top: 3px;"></div>
                                </div>
                                <div class="form-group" >
                                    {{ Form::label('', 'Total Payable:', [ 'class'=>'col-sm-4 control-label push-10-t' ]) }}
                                    <div class="col-sm-8 text-left cusPayable text-danger font-w600 h2" style="padding-top: 7px;"></div>
                                </div>
                            </div>
                            <div class="pleaseWait"><i class="fa fa-spinner text-info"></i></div>
                            <div class="noCustomerFound">
                                <p style="margin-bottom: 5px;">No Customer Found!</p>
                                <div class="form-group"  style="margin-bottom: 10px;" >
                                    {{ Form::label('', 'Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                    <div class="col-sm-9 text-left">
                                        {{ Form::text('customer_name',    null,
                                           [
                                               'placeholder'=>'Customer Name',
                                               'class'=>'form-control '
                                           ]) }}
                                    </div>
                                </div>
                                <div class="form-group"  style="margin-bottom: 10px;" >
                                    {{ Form::label('', 'Address:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                    <div class="col-sm-9 text-left" style="padding-top: 7px;">
                                        {{ Form::textarea('customer_address',    null,
                                           [
                                               'placeholder'=>'Customer Address',
                                               'class'=>'form-control ',
                                               'rows'=> 3
                                           ]) }}
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="form-group push-10-t" >
                                <div class="col-sm-12">
                                    {{ Form::number('paid',    null,
                                    [
                                        'min'=> 0,
                                        'autocomplete'=>'off',
                                        'placeholder'=>'Enter Paid Amount',
                                        'class'=>'form-control paidAmount',
                                        'data-fv-number' => 'true',
                                        'data-fv-notempty' => 'true',
                                        'data-fv-blank' => 'true',
                                        'data-fv-notempty-message' => 'Paid Amount is required'
                                    ]) }}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><div class="push-10 push-10-t ">
                                <button    class="btn btn-lg btn-success"  type="submit">Complete Sale</button></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


<div class="modal fade" id="OutOffStock" tabindex="-1" role="dialog" aria-labelledby="OutOffStock" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-smooth text-white">
                Out Off Stock
            </div>
            <div class="modal-body">
                <div class="outOffStockMessage">Product is Out off Stock!</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@push('footer.scripts')
<script src="{{ asset("js/product/sales.js") }}"></script>
@endpush
