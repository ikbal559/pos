<div class="col-sm-offset-3 hidden" id="error">
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <span class="message"></span>
    </div>
</div>
<div class="col-sm-offset-3 hidden" id="success">
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <span class="message"></span>
    </div>
</div>
<div class="col-sm-offset-3 hidden" id="please-wait">
    <div class="alert alert-info alert-dismissible fade in" role="alert">
        <span class="message">Processing. Please Wait....</span>
        
    </div>
</div>