@extends('layouts.shop')
@section('title')
    @parent
   Payments Report
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left"> Payments Report</h5>
                </div>
                <div class="panel-body">
                    <div class="clearfix push-5-t">
                        <div class="col-sm-6 col-lg-4">
                            <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="1" href="javascript:void(0)">
                                <div class="block-content block-content-full bg-success ">
                                    <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $cashIn }}</span></div>
                                    <div class="h5 text-white-op text-uppercase push-5-t">Cash IN</div>
                                </div>

                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="2" href="javascript:void(0)">
                                <div class="block-content block-content-full bg-smooth">
                                    <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $cashOut }}</span></div>
                                    <div class="h5 text-white-op text-uppercase push-5-t">Cash Out</div>
                                </div>

                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <a class="block block-rounded block-link-hover3 text-center  userTypeClick" data-type="3" href="javascript:void(0)">
                                <div class="block-content block-content-full bg-warning">
                                    <div class="h1 font-w700 text-white"><span class="h2 text-white-op">{{ $cashIn -  $cashOut }}</span></div>
                                    <div class="h5 text-white-op text-uppercase push-5-t">Balance</div>
                                </div>

                            </a>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Action</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments_report as $item)
                                        <tr>
                                            <td>{{  date("F j, Y g:i a", strtotime($item->created_at)) }}</td>
                                            <td>
                                                @if($item->ref_id > 0 && $item->type == 'Order')
                                                    <a class="label label-info" href="{{ route('order-view', $item->ref_id ) }}">View Order</a>
                                                @endif
                                            </td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->type }}</td>
                                            <td>{{ $item->amount }}</td>
                                            <td>{{ $item->balance }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
