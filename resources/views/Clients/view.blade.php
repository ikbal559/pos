@extends('layouts.shop')
@section('title')
    @parent
    View Client
@stop
@section('content')
    @if( (!empty($client))  )
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">

                <div class="panel-heading clearfix">
                    <h5 class="pull-left">{{ $client->name }} </h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            Phone : {{ $client->phone }}
                                        </div>
                                        <div class="col-md-6">
                                            Address: {{ $client->address  }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Latest Store IN</h5>
                </div>
                <div class="panel-body">

                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="panel-body mobilePadding0">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="products-data-table">
                                            <thead>
                                            <th>V No</th>
                                            <th>Qty</th>
                                            <th>Amount</th>
                                            <th>Total</th>
                                            <th>Date</th>
                                            </thead>

                                            <tbody>
                                                @foreach($storeInProduct as $item)
                                                    <tr>
                                                        <td>{{ $item->v_no }}</td>
                                                        <td>{{ $item->qty }}</td>
                                                        <td>{{ $item->price }}</td>
                                                        <td>{{ $item->total }}</td>
                                                        <td>{{ $item->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>
                        </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Latest Payments</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="panel-body mobilePadding0">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="products-data-table">
                                            <thead>
                                                <th>V No</th>
                                                <th>Amount</th>
                                                <th>Note</th>
                                                <th>Date</th>
                                            </thead>
                                            <tbody>
                                                @foreach($clientPayments  as $item)
                                                    <tr>
                                                        <td>{{ $item->v_no }}</td>
                                                        <td>{{ $item->amount }}</td>
                                                        <td>{{ $item->note }}</td>
                                                        <td>{{ $item->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
