@extends('layouts.shop')
@section('title')
    @parent
   Client Payment
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h5 class="pull-left">Client Payment</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content" style="display: block;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    {!! Form::open(['url'=>route('api.client.add_payment'), 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal' ]) !!}

                                    {{ Form::hidden('client_id', (!empty($client)) ? $client->client_id : null  ,['class'=>'form-control'] ) }}
                                    {{ Form::hidden('shop_id',  \Auth::user()->shop_id, ['class'=>'form-control'] ) }}

                                    <div class="form-group">
                                        {{ Form::label('name', 'Client Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('name',  (!empty($client)) ? $client->name : null,
                                            [
                                                'class'=>'form-control',
                                                'data-fv-number' => 'true',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'readonly' => 'readonly',
                                                'data-fv-notempty-message' => 'Name is required'
                                            ]) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('phone', 'Phone:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('phone',  (!empty($client)) ? $client->phone : null,
                                            [
                                                'class'=>'form-control',
                                                'data-fv-number' => 'true',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'readonly' => 'readonly',
                                                'data-fv-notempty-message' => 'Phone is required'
                                            ]) }}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group bg-warning    font-w600">
                                        {{ Form::label('amount', 'Amount:', [ 'class'=>'col-sm-3 control-label text-white' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::number('amount',  (!empty($payment)) ? $payment->amount : null,
                                            [
                                                'class'=>'form-control  text-smooth font-w600 ',
                                                'data-fv-number' => 'true',
                                                'data-fv-notempty' => 'true',
                                                'data-fv-blank' => 'true',
                                                'data-fv-notempty-message' => 'Amount is required'
                                            ]) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('v_no', 'Invoice No.:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('v_no',  (!empty($payment)) ? $payment->v_no : null,
                                            [
                                                'class'=>'form-control'
                                            ]) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('note', 'Payment Description:', [ 'class'=>'col-sm-3 control-label' ]) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('note',  (!empty($payment)) ? $payment->note : null,
                                            [
                                                'class'=>'form-control'
                                            ]) }}
                                        </div>
                                    </div>
                                    @include('alert_message')
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-left clearfix']) }}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
