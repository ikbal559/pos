@extends('layouts.shop')
@section('title')
    @parent
    Clients
@stop
@section('content')
<div class="block">
    <div class="block-content">
        <div class="mobilePadding0 col-lg-12 pull-left">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="clearfix push-5-t">
                        <div class="col-sm-6 col-lg-3">
                            <a class="btn btn-lg btn-info" href="{{ route('clients-add')  }}"><span class="fa fa-plus"></span> Add Clients</a>
                        </div>
                    </div>
                    <div class="panel-body mobilePadding0">
                        <div class="table-responsive">
                            <table class="table table-striped" id="UserClients-data-table">
                                <thead>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Balance</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/datatable/tableFactory.js")}}"></script>
@endpush
