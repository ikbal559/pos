{!! Form::open(['url'=>$url, 'method'=>'POST', 'class'=>'form-vertical form-vertical-global form-horizontal' ]) !!}

        {{ Form::hidden('client_id', (!empty($user)) ? $user->client_id : null  ,['class'=>'form-control'] ) }}
        {{ Form::hidden('shop_id',  \Auth::user()->shop_id, ['class'=>'form-control'] ) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('name',  (!empty($user)) ? $user->name : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Name is required'
                ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('phone', 'Phone:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('phone',  (!empty($user)) ? $user->phone : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Phone is required'
                ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('address', 'Address:', [ 'class'=>'col-sm-3 control-label' ]) }}
            <div class="col-sm-9">
                {{ Form::text('address',  (!empty($user)) ? $user->address : null,
                [
                    'class'=>'form-control',
                    'data-fv-number' => 'true',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Address is required'
                ]) }}
            </div>
        </div>

    @include('alert_message')
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            {{ Form::submit("Submit", ['class'=>'btn btn-primary pull-left clearfix']) }}
        </div>
    </div>
{!! Form::close() !!}

