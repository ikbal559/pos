@extends('layouts.shop')
@section('title')
    @parent
    Manage Product
@stop
@section('content')
    <div class="block">
        <div class="block-content">
            <div class="mobilePadding0 col-lg-12 pull-left">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="h4 pull-left ">Manage Store</div>
                        </div>
                        <div class="panel-body mobilePadding0">
                            <div class="table-responsive">
                                <table class="table table-striped" id="manage-data-table">
                                    <thead>
                                        <th>ID</th>
                                        <th>Type</th>
                                        <th>Invoice ID</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/datatable/tableFactory.js")}}"></script>
@endpush