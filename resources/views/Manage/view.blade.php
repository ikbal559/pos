@extends('layouts.pos')
@section('title')
    @parent
    Store
@stop
@section('content')
    <div class="row"  style="margin-top: 100px;">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <!-- Invoice -->
                    <div class="block" >
                        <div class="block-header" style="padding: 15px 0;">
                            <ul class="block-options">
                                <li>
                                    <!-- Print Page functionality is initialized in App() -> uiHelperPrint() -->
                                    <button type="button" onclick="App.initHelper('print-page');"><i class="si si-printer"></i> Print Invoice</button>
                                </li>
                            </ul>
                            <div class="h4 block-title pull-right">{{  date("F j, Y g:i a", strtotime($manage->created_at)) }}</div>
                            <div class="h3 block-title">#{{ $manage->manage_products_id }}</div>
                        </div>
                        <div class="block-content ">
                            <!-- Invoice Info -->
                            <div class="h1 text-center push-30-t push-30 hidden-print">INVOICE</div>
                            <hr class="hidden-print">
                            <div class="row items-push-2x">
                                <!-- Company Info -->
                                <div class="col-xs-6 col-sm-4 col-lg-3">
                                    <p class="h2 font-w400 push-5">{{ $manage->shop->name  }}</p>
                                    <address>
                                        {{ $manage->shop->address  }}<br>
                                        <i class="si si-call-end"></i> {{ $manage->shop->phone  }}
                                    </address>
                                </div>
                                <!-- END Company Info -->

                                <!-- Client Info -->
                                <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-lg-3 col-lg-offset-6 text-right">
                                    <p class="h5 font-w400 push-5">Invoice: {{ $manage->manage_products_invoice  }} </p>
                                    <p class="push-5">Note: {{ $manage->manage_products_note  }} </p>
                                </div>
                                <!-- END Client Info -->
                            </div>
                            <!-- END Invoice Info -->

                            <!-- Table -->
                            <div class="table-responsive push-50">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="width: 50px;">ID</th>
                                        <th>Product</th>
                                        <th class="text-center" style="width: 80px;">Quantity</th>
                                        <th class="text-right" style="width: 80px;">Price</th>
                                        <th class="text-right" style="width: 130px;">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $subTotal = 0;?>
                                    @foreach($products as $product)
                                        <?php $total =  $product->price * $product->quantity; $subTotal += $total;?>
                                        <tr>
                                            <td class="text-center">{{ $product->product_id }}</td>
                                            <td>
                                                <p class="font-w600 push-10">{{ $product->product->product_name  }}</p>
                                                <div class="text-muted">{{ $product->product->product_variation }}, {{ $product->product->product_weight }}</div>
                                            </td>
                                            <td class="text-center"><span class="badge badge-primary">{{ $product->quantity }}</span></td>
                                            <td class="text-right">{{ $product->price }}</td>
                                            <td class="text-right">{{ $total }}</td>
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Subtotal</td>
                                        <td class="text-right font-w600">{{ $subTotal  }} Tk</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Vat Rate</td>
                                        <td class="text-right">0%</td>
                                    </tr>
                                    <tr class="active">
                                        <td colspan="4" class="font-w700 text-uppercase text-right">Total </td>
                                        <td class="font-w700 text-right">{{ $manage->manage_products_total }}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <!-- END Table -->

                            <!-- Footer -->
                            <hr class="hidden-print">
                            <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
                            <!-- END Footer -->
                        </div>
                    </div>
                    <!-- END Invoice -->

                </div>
            </div>

        </div>
    </div>
@endsection
