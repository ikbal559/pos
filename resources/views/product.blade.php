@extends('layouts.home')
@section('content')
    <div class="bg-white push-10">
        <section class="content content-boxed">
            <div class="push-20">
                <h3 class="homeCategoryTitle">
                    {{$product->product_name}}
                </h3>
                <div class="row items-push-2x">
                    <div class="col-md-3">
                        <div><img src="{{ asset('images/product_images/'.$product->product_images) }}"> </div>
                    </div>
                    <div class="col-lg-5">
                         <table class="table table-striped">
                            @if (!Auth::guest() && Auth::user()->user_type != 'normal')
                             <tr>
                                 <td>Whole Sale Price:</td>
                                 <td>{{$product->product_price}}</td>
                             </tr>
                             @elseif (!Auth::guest() && Auth::user()->user_type == 'normal')
                                 <tr>
                                     <td colspan="2">
                                         <div class="text-center btn btn-info" style="width: 100%">Your account is on Pending list.<br> Please wait Until we verify your information.<br> Help: 01848304555</div>
                                     </td>
                                 </tr>
                             @else
                                 <tr>
                                     <td colspan="2">
                                         <div class="text-center btn btn-success" style="width: 100%">To order a product you must register. <br> Help: 01848304555</div>
                                     </td>
                                 </tr>
                             @endif
                            <tr>
                                <td>MRP:</td>
                                <td>{{$product->product_mrp}}</td>
                            </tr>
                             <tr>
                                <td>Code:</td>
                                <td>{{$product->product_id}}</td>
                            </tr>
                             <tr>
                                <td>Variation:</td>
                                <td>{{$product->product_variation}}</td>
                            </tr>
                            <tr>
                                <td>Weight / Size:</td>
                                <td>{{$product->product_weight}}</td>
                            </tr>
                             <tr>
                                 <td>Min Order Qty:</td>
                                 <td>{{$product->minimum_order}}</td>
                             </tr>
                             <tr>
                                <td>Description:</td>
                                <td>{{$product->description}}</td>
                            </tr>
                            <tr>
                                <td>Terms &amp; Conditions:</td>
                                <td>{{$product->terms_and_conditions}}</td>
                            </tr>
                        </table>

                    </div>
                    <div class="col-lg-4">
                        @if (Auth::guest())
                            <a href="/login" class="btn btn-sm btn-primary">Login</a> or <a href="{{ route('users-registration') }}" class="btn btn-sm btn-success">Register</a> to see Wholesale Price
                        @else
                            @if (!Auth::guest() && Auth::user()->user_type != 'normal')
                                {!! Form::open(['url'=>route('order-new'), 'method'=>'POST', 'class'=>'form-vertical_cart form-vertical-global form-horizontal']) !!}
                                {{ Form::hidden('id',  $product->product_id  ) }}
                                <div class="hidden" id="error">
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                        <span class="message">Invalid Order</span>
                                    </div>
                                </div>
                                <div class="hidden" id="success">
                                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                                        <span class="message"><span class="fa fa-check"></span> Product Successfully Added</span>
                                    </div>
                                </div>
                                <div class="hidden" id="please-wait">
                                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                                        <span class="message"><span class="fa fa-spinner"></span> Please Wait....</span>
                                    </div>
                                </div>


                                <div class="input-group">
                                    <input class="form-control" type="number" required min="1"   name="qty" placeholder="Enter Quantity">
                                    <span class="input-group-addon " style="padding: 0;"><input class="btn btn-warning" type="submit" value="ORDER NOW"></span>
                                </div>
                                {!! Form::close() !!}

                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>

    @if($relatedProducts->count())
    <div class="bg-white push-10">
        <section class="content content-boxed">
            <!-- Section Content -->
            <div class="push-10-t">
                <h3 class="homeCategoryTitle">
                    Related Product
                </h3>
                <div class="row items-push-2x">
                    @foreach($relatedProducts as $product)
                        @include('product_list_view', ['product' => $product])
                    @endforeach
                </div>
            </div>
            <!-- END Section Content -->
        </section>
    </div>
    @endif
@endsection