<div class="col-md-3 visibility-hidden productGit" data-toggle="appear" data-class="animated zoomIn">
    <a href="{{ route('product-single', $product->product_id) }}">
        <div><img src="{{ asset('images/product_images/'.$product->product_images) }}"> </div>
        <h4>{{$product->product_name}} ({{$product->product_variation}})</h4>
        <h6 class="text-default" style="color: #666; font-weight: 400; margin-bottom: 5px; clear: both; display: block; ">
           <span style="    width: 48%;    display: inline-block;  text-align: left;">Code: {{$product->product_id}}</span>
           <span style="    width: 48%;    display: inline-block;  text-align: right;">{{$product->product_weight}}</span>
        </h6>
    </a>
    <div class="productPriceGroup">
        <span class="pull-left">{{$product->product_mrp}} <span style="display: block; font-size: 10px; color:#666;">Market Price</span></span>
        @if (!Auth::guest() && Auth::user()->user_type != 'normal')
            <span class="pull-right text-right text-danger font-w600">{{$product->product_price}} <span style="display: block; font-size: 10px; color:green;">Wholesale Price</span></span>
        @else
            <span class="pull-right text-right text-danger font-w400" style="font-size: 10px;">Login/Register <br> To See Wholesale Price</span>
        @endif
    </div>
</div>