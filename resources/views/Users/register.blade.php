@extends('layouts.home')

@section('content')

        <div class="row">
            <div class="col-md-10 col-md-offset-1 MobilFormStyle" >

                <div class="block block-themed animated slideInUp" style="background-color: #fff;">
                    <div class="block-header bg-success">
                        <h3 class="block-title">New User Register</h3>
                    </div>
                    <div class="block-content block-content-full block-content-narrow">
                        @include('Users.partials.registration',
                            [
                                    'url' => route('api.network.registration-frontend'),
                                    'redirect_url' => route('login'),
                            ])
                    </div>
                </div>

            </div>
        </div>


@endsection
@push('footer.scripts')
<script type="text/javascript" src="{{asset("js/registration/registration.js")}}"></script>
@endpush