{!! Form::open([ 'url' => $url, 'data-redirect-url' => $redirect_url, 'method'=>'POST', 'class'=>'form-vertical-registration-frontend form-horizontal']) !!}


<div class="form-group" style="margin-top: 40px;">
    {{ Form::label('users[name]', 'Name:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('users[name]',  null ,
        [
                'placeholder'=>'Your  Name',
                'class'=>'form-control',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'Name is required'
        ]) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('users[email]', 'Email:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::email('users[email]',  null ,
        [
                'placeholder'=>'Your Email',
                'class'=>'form-control'
        ]) }}
    </div>
</div>


<hr>


<div class="form-group">
    {{ Form::label('users[username]', 'User Name:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        <div class="input-group">
            <span class="input-group-btn"><button class="btn btn-default" type="button">+88</button></span>
            {{ Form::text('users[username]',  null ,
            [
                    'placeholder'=>'Username (example: 01800XXXXXX )',
                    'class'=>'form-control',
                    'data-fv-notempty' => 'true',
                    'data-fv-blank' => 'true',
                    'data-fv-notempty-message' => 'Username is required'
            ]) }}
        </div>
    </div>
</div>




<div class="form-group">
    {{ Form::label('users[password]', 'Password:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::password('users[password]',
        [
                'placeholder'=>'Login Password',
                'class'=>'form-control',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'Password is required'
        ]) }}
    </div>
</div>
<div class="form-group">
    <label for="password-confirm" class="col-md-3 control-label">Confirm Password:</label>

    <div class="col-md-9">
        <input id="password-confirm" type="password" placeholder="Retype Password" class="form-control" name="password_confirmation" required>
    </div>
</div>

<div class="form-group">
    {{ Form::label('users[phone]', 'Other Contact Number:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('users[phone]',  null ,
        [
                'placeholder'=>'Contact Number',
                'class'=>'form-control',
        ]) }}
    </div>
</div>
<hr>


<div class="form-group">
    {{ Form::label('shop[name]', 'Organization Name:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('shop[name]',  null ,
        [
                'placeholder'=>'Your Organization Name',
                'class'=>'form-control',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'Organization Name is required'
        ]) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('business_type[]', 'Business Type:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        @if(!empty($business_types))
            @foreach($business_types as $key => $business_type)
                <label class="css-input css-checkbox css-checkbox-success" style="margin-right: 15px;">
                    <input name="business_type[]" value="{{ $key }}" type="checkbox" ><span></span> {{ $business_type }}
                </label>
            @endforeach
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('business_category[]', 'Product Type:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        @if(!empty($category))
            @foreach($category as $key => $cat)
                <label class="css-input css-checkbox css-checkbox-success" style="margin-right: 15px;">
                    <input name="business_category[]" value="{{ $key }}" type="checkbox"  ><span></span> {{ $cat }}
                </label>
            @endforeach
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('shop[address]', 'Address:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::textarea('shop[address]',null,
        [
                'placeholder'=>'Address',
                'rows'=>3,
                'class'=>'form-control',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'Address is required'
        ]) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('shop[division]', 'Division:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::select('shop[division]', $division,null,
        [
                'placeholder'=>'Select Your Division',
                'class'=>'form-control divisionSelect',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'Division is required'
        ]) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('shop[district]', 'District:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::select('shop[district]', [], null,
        [
                'placeholder'=>'Select Your District',
                'class'=>'form-control districtSelect',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'District is required'
        ]) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('shop[thana]', 'Thana:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::select('shop[thana]', [], null,
        [
                'placeholder'=>'Select Your Thana',
                'class'=>'form-control thanaSelect',
                'data-fv-notempty' => 'true',
                'data-fv-blank' => 'true',
                'data-fv-notempty-message' => 'Thana is required'
        ]) }}
    </div>
</div>


<hr>


<div class="form-group">
    {{ Form::label('shop[reference_name]', 'Introducer Name:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('shop[reference_name]',  null ,
        [
                'placeholder'=>'Introducer  Name',
                'class'=>'form-control',

        ]) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('shop[reference_phone]', 'Introducer Phone:', ['class'=>'col-sm-3 control-label']) }}
    <div class="col-sm-9">
        {{ Form::text('shop[reference_phone]',  null ,
        [
                'placeholder'=>'Introducer Phone Number',
                'class'=>'form-control',

        ]) }}
    </div>
</div>

@include('alert_message')
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
        {{ Form::submit("Submit", ['class'=>'btn btn-success']) }}
    </div>
</div>
{!! Form::close() !!}

@push('footer.scripts')
<script >
    $(document).ready(function () {
        $('.divisionSelect').change(function () {
            var division = $(this).val();
            if(division>0){
                $.ajax({

                    type: "POST",
                    cache: false,
                    url: '/api/location/get',
                    data: {id:division, type:'division_id'},
                    success: function(data){
                        $('.districtSelect').html(data);
                        $('.thanaSelect').html('');
                    },
                    dataType: "html"

                });
            }
        });

        $('.districtSelect').change(function () {
            var division = $(this).val();
            if(division>0){
                $.ajax({

                    type: "POST",
                    cache: false,
                    url: '/api/location/get',
                    data: {id:division, type:'district_id'},
                    success: function(data){
                        $('.thanaSelect').html(data);
                    },
                    dataType: "html"

                });
            }
        });
    });
</script>
@endpush