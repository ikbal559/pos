<?php

namespace Products;

use Illuminate\Database\Eloquent\Model;
use Orders\OrdersProducts;
use Shops\Shops;
use Users\Client;
use Users\User;
use Yajra\Datatables\Facades\Datatables;

use Auth;


    class StoreProducts extends Model
{

    protected $table = 'store_products';
    protected $primaryKey = 'store_product_id';


    protected $fillable = [
        'product_id',
        'client_id',
        'shop_id',
        'qty',
        'v_no',
        'total',
        'avg_store_in',
        'total_store_in',
        'price'
    ];
 
    public function scopeSearchByShop($query)
    {
        if( Auth::user()->user_type =='shop'){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
        }
        return $query;
    }

    public function scopeSearchBy($query, $request)
    {
        if ($request->get('end_date')) {
            $query->whereDate('created_at', '<=', $request->get('end_date'));
        }

        if ($request->get('start_date')) {
            $query->whereDate('created_at', '>=', $request->get('start_date'));
        }

        if ($request->get('client_id')) {
            $query->where('client_id', $request->get('client_id'));
        }

        if ($request->get('product_id')) {
            $query->where('product_id', $request->get('product_id'));
        }

        return $query;
    }
    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id', 'product_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class,  'client_id', 'client_id');
    }

}
