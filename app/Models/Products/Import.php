<?php

namespace Products;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Products\Products;
use Auth;

class Import extends Model
{
    protected $table = 'imports';


    protected $primaryKey = 'import_id';


    protected $fillable = [
        'manage_products_id',  'product_id', 'quantity', 'price', 'total'
    ];

    public function product()
    {
        return $this->hasOne(Products::class, 'product_id', 'product_id');
    }
 }
