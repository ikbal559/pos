<?php

namespace Products;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Products\Products;
use Auth;

class Damage extends Model
{
    protected $table = 'damages';


    protected $primaryKey = 'damage_id';

 
    protected $fillable = [
        'manage_products_id',  'product_id', 'quantity', 'price', 'total'
    ];

    public function product()
    {
        return $this->hasOne(Products::class, 'product_id', 'product_id');
    }

}
