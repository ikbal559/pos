<?php

namespace Products;


use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Shops\Shops;
use Auth;

class ManageProducts extends Model
{
    protected $table = 'manage_products';
    protected $primaryKey = 'manage_products_id';

    protected $fillable = [

        'shop_id',
        'user_id',
        'partner_id',
        'manage_products_type',
        'manage_products_quantity',
        'manage_products_total',
        'manage_products_invoice',
        'manage_products_note',
        'manage_products_date'
        
    ];

    public function shop()
    {
        return $this->hasOne(Shops::class, 'shop_id', 'shop_id');
    }

    public function scopeSearchByShop($query)
    {
        $query->where('shop_id', Auth::user()->shop_id);
        return $query;
    }

    public static function getDatatableData($request){

        $query = self::SearchByShop()->get();
        return Datatables::of($query)
            ->editColumn('manage_products_type', function ($data) {

                if( $data->manage_products_type  == 'import' ){
                    return '<span class="label label-success">Import</span>';
                }
                if( $data->manage_products_type == 'export' ){
                    return '<span class="label label-warning">Export</span>';
                }
                if( $data->manage_products_type == 'damage' ){
                    return '<span class="label label-danger">Damage</span>';
                }

            })
            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })

            ->addColumn('action', function ($data) {
                $button  = ' <a class="btn push-10-r btn-danger btn-xs" href="'.route('manage-edit', $data->manage_products_id ).'">Edit </a> ';
                $button .= ' <a class="btn btn-info btn-xs" href="'.route('manage-products-view', $data->manage_products_id ).'"> View Detail</a> ';
                return $button;
            })
            ->rawColumns(['action', 'manage_products_type'])
            ->make(true);

    }

}
