<?php

namespace Products;

use Illuminate\Database\Eloquent\Model;
use Orders\OrdersProducts;
use Shops\Shops;
use Users\User;
use Yajra\Datatables\Facades\Datatables;

use Auth;


class Products extends Model
{
    protected $table = 'products';


    protected $primaryKey = 'product_id';


    protected $fillable = [
        'shop_id',
        'code',
        'name',
        'mrp',
        'sale',
        'price',
        'size_model',

        'avg_store_in',
        'total_store_in',
        'point',        

    ];

    public function scopeSearchByShop($query)
    {
       if( Auth::user()->user_type =='shop'){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
       }
        return $query;
    }

    public function scopeActive($query)
    {
      return $query->where('product_status', 1);
    }

    public function scopeOrdering($query)
    {
        return $query->orderBy('ordering', 'DESC');
    }

    public function scopeHome($query)
    {
      return $query->where('home_status', 1);
    }
 

    public static function getProductList()
    {
        $products = self::SearchByShop()->get();
        $list = [];
        foreach ( $products as $product){
            
            $list[$product->product_id] = "{$product->code} | {$product->name} - {$product->size_model} | {$product->avg_store_in}";
        }
        return $list;
    }

    public function imports()
    {
        return $this->hasMany(Import::class, 'product_id', 'product_id');
    }

    public function exports()
    {
        return $this->hasMany(Export::class, 'product_id', 'product_id');
    }

    public function damages()
    {
        return $this->hasMany(Damage::class, 'product_id', 'product_id');
    }

    public function sales()
    {
        return $this->hasMany(OrdersProducts::class, 'product_id', 'product_id');
    }

    public function store_in()
    {
        return $this->hasMany(StoreProducts::class, 'product_id', 'product_id');
    }

    public function shop()
    {
        return $this->belongsTo(Shops::class, 'shop_id', 'shop_id');
    }



    public static function getDatatableData($request){

        if(!empty($request->shop_id) && Auth::user()->user_type =='admin'){
            $query = self::where('shop_id' , $request->shop_id)->get();
        }else{
            $query = self::SearchByShop()->get();
        }

        return Datatables::of($query)

            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-info btn-xs" href="'.route('product-edit', $data->product_id ).'">Edit</a> ';
                $button .= '<a class="btn btn-success btn-xs" href="'.route('product-view', $data->product_id ).'">Detail</a>';
                return $button;
            })
 
            ->addColumn('total_volume', function ($data) {
                $volume = $data->total_store_in * $data->avg_store_in ;
               return number_format((float)$volume, 2, '.', '');
            })
            ->make(true);

    }


}
