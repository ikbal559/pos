<?php

namespace Products;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;

class BusinessTypes extends Model
{
    protected $table = 'business_types';


    protected $primaryKey = 'business_types_id';


    protected $fillable = [
        'name', 'status',
    ];
 

    public static function getDatatableData($request){


        $query = self::all();
        return Datatables::of($query)

            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-info btn-xs" href="'.route('product-business-type-edit', $data->business_types_id ).'">Edit</a>';
                return $button;
            })
            ->make(true);


    }

}
