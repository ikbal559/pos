<?php

namespace Products;



use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class Category extends Model
{
    protected $table = 'category';


    protected $primaryKey = 'category_id';


    protected $fillable = [
        'parent_id',  'category_id', 'category_name', 'category_status', 'ordering',
    ];

    public function scopeSearchByShop($query)
    {
       return true;
    }
    
    public function scopeOrdering($query)
    {
       return $query->orderBy('ordering', 'DESC');
    }

    public function childs(){
        return $this->hasMany(Category::class, 'parent_id');
    }

    public static function getDatatableData($request){


        $query = self::all();
        return Datatables::of($query)

            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-info btn-xs" href="'.route('product-category-edit', $data->category_id ).'">Edit</a>';
                return $button;
            })
            ->make(true);


    }


}
