<?php

namespace Permission;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Permissions extends Model
{
    use Notifiable;

    protected $table = 'permissions';
    protected $primaryKey = 'permission_id';


    protected $fillable = [
        'permission_group_id',
        'user_id',
        'status',
     ];
    public function group()
    {
        return $this->belongsTo(PermissionGroup::class, 'permission_group_id', 'permission_group_id');
    }
}
