<?php

namespace Permission;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PermissionGroup extends Model
{
    use Notifiable;

    protected $table = 'permission_group';
    protected $primaryKey = 'permission_group_id';


}
