<?php

namespace Transaction;

use Illuminate\Database\Eloquent\Model;
use Users\User;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class AccountPayments extends Model
{
    protected $table = 'transaction_account_payments';

    protected $primaryKey = 'transaction_account_payment_id';

    protected $fillable = [
        'user_id',  'description',  'amount', 'balance'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public static function getDatatableData($request){


        $query = self::all();

        return Datatables::of($query)
            ->editColumn('user_id', function ($data) {
                return $data->user->username;
            })
            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })
            ->make(true);


    }

}
