<?php

namespace Transaction;

use Illuminate\Database\Eloquent\Model;
use Shops\Shops;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class ShopPayment extends Model
{
    protected $table = 'transaction_shop_payments';


    protected $primaryKey = 'transaction_shop_payment_id';


    protected $fillable = [
        'shop_id',  'description', 'amount', 'balance'
    ];

    public function shop()
    {
        return $this->hasOne(Shops::class, 'shop_id', 'shop_id');
    }


    public function scopeSearchByShop($query)
    {
        $query->where('shop_id', Auth::user()->shop_id);
        return $query;
    }

    public static function getDatatableData($request){


        if ($request->get('admin')) {
            $query = self::SearchByShop()->get();
        }else{
            $query = self::all();
        }
        return Datatables::of($query)

            ->editColumn('shop_id', function ($data) {
                return $data->shop->name;
            })

            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })

            ->make(true);


    }



}
