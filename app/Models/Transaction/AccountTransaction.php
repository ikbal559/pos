<?php

namespace Transaction;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class AccountTransaction extends Model
{
    protected $table = 'transaction_accounts';

    protected $primaryKey = 'transaction_account_id';

    protected $fillable = [
        'user_id',  'description', 'type',  'amount', 'balance'
    ];

    public static function getDatatableData($request){


        $query = self::where('user_id', Auth::user()->id)->get();

        return Datatables::of($query)

            ->editColumn('type', function ($data) {

                if( $data->type  == 'credit' ){
                    return '<span class="label label-success">Credit</span>';
                }
                if( $data->type == 'debit' ){
                    return '<span class="label label-warning">Debit</span>';
                }

            })
            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })
            ->rawColumns(['type'])
            ->make(true);


    }

}
