<?php

namespace Marketing;


use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class Sms extends Model
{
    protected $table = 'sms';


    protected $primaryKey = 'id';


    protected $fillable = [
         'user_id',  'order_id', 'content'
    ];


    public static function getDatatableData($request){

        $query = self::where('type', $request->type)->get();
        return Datatables::of($query)
            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })


            ->make(true);


    }

}
