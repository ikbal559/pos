<?php

namespace Marketing;


use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class Marketing extends Model
{
    protected $table = 'marketing';


    protected $primaryKey = 'id';


    protected $fillable = [
         'user_id',  'type', 'content', 'total'
    ];


    public static function getDatatableData($request){

        $query = self::where('type', $request->type)->get();
        return Datatables::of($query)
            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })
            ->editColumn('to', function ($data) {

                return implode(',' , unserialize($data->user_id ));
            })

            ->make(true);


    }

}
