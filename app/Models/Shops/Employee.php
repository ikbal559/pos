<?php

namespace Shops;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;
use Auth;

class Employee extends Model
{
    protected $table = 'entrys';


    protected $primaryKey = 'entry_id';


    protected $fillable = [
        'shop_id',  'product_id', 'quantity', 'product_price'
    ];

    public function product()
    {
        return $this->hasOne(Products::class, 'product_id', 'product_id');
    }


    public function scopeSearchByShop($query)
    {
        $query->where('shop_id', Auth::user()->shop_id);
        return $query;
    }

    public static function getDatatableData($request){


        $query = self::SearchByShop()->get();
        return Datatables::of($query)

            ->editColumn('product_name', function ($data) {
                return $data->product->product_name;
            })

            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })
            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-info btn-xs" href="'.route('product-entry-edit', $data->entry_id ).'">Edit</a>';
                return $button;
            })
            ->make(true);


    }



}
