<?php

namespace Shops;

use Illuminate\Database\Eloquent\Model;
use Products\BusinessTypes;
use Products\Category;
use Users\Locations;
use Users\User;
use Yajra\Datatables\Facades\Datatables;


class Shops extends Model
{
    protected $table = 'shops';


    protected $primaryKey = 'shop_id';


    protected $fillable = [
        'name', 'phone',  'address', 
    ];





    public function user()
    {
        return $this->belongsTo(User::class, 'shop_id', 'shop_id' );
    }

    public function district()
    {
        return $this->belongsTo(Locations::class, 'district_id', 'location_id' );
    }

    public function location()
    {
        return $this->belongsTo(Locations::class, 'location_id', 'location_id' );
    }

    static function getShopList(){
        $shops = self::all();
        $List = [];
        foreach ($shops as $shop){
            if(!empty($shop->user)){
                $List[$shop->shop_id] = "{$shop->user->username} - {$shop->name} - {$shop->user->name}";
            }
        }
        return $List;
    }
 

    public function businessTypes()
    {
        return $this->belongsToMany(BusinessTypes::class, 'user_business_types',
            'shop_id',  'business_types_id');
    }


    public static function getDatatableData($request){

    $query = User::where('user_type', 'shop')->get();
    return Datatables::of($query)

        ->editColumn('user_shop', function ($data) {
            if($data->shop){
                return $data->shop->name;
            }
            return 'Null';
        })

        ->editColumn('address', function ($data) {
            if($data->shop){
                return $data->shop->address;
            }
            return 'Null';
        })
        ->addColumn('action', function ($data) {
            if($data->shop){
                $button  =   '<a class="btn btn-info btn-xs" href="'.route('shops-edit', $data->shop->shop_id ).'">Edit</a> ';
                return $button;
            }
            return '';
        })
        ->make(true);

    }



}
