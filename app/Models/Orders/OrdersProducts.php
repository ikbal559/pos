<?php

namespace Orders;
use Products\Products;
use Illuminate\Database\Eloquent\Model;
use Shops\Shops;
use Yajra\Datatables\Facades\Datatables;


class OrdersProducts extends Model
{
    protected $table = 'orders_products';


    protected $primaryKey = 'order_product_id';


    protected $fillable = [
        'order_id',
        'shop_id',
        'product_id',
        'qty',
        'price',
        'point',
        'total',

        'avg_store_in',
        'mrp',
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id', 'product_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shops::class, 'shop_id', 'shop_id');
    }


}
