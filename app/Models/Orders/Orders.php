<?php

namespace Orders;

use Marketing\Sms;
use Orders\OrdersProducts;
use Illuminate\Database\Eloquent\Model;
use Shops\Shops;
use Users\Member;
use Users\User;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class Orders extends Model
{
    protected $table = 'orders';


    protected $primaryKey = 'order_id';


    protected $fillable = [
        'shop_id',
        'user_id',
        'member_id',

        'order_total',
        'order_point',
        'order_qty',
        'order_date',
        'order_discount',
        'order_profit',

        'paid',
        'due',
        'previous_due',
        'member_balance',

        'order_status',
    ];

    public function shop()
    {
        return $this->hasOne(Shops::class, 'shop_id', 'shop_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function member()
    {
        return $this->hasOne(Member::class, 'member_id', 'member_id');
    }

    public function sms()
    {
        return $this->hasMany(Sms::class, 'order_id', 'order_id');
    }

    public function products()
    {
        return $this->hasMany(OrdersProducts::class, 'order_id', 'order_id');
    }


    public function scopeSearchBy($query, $request)
    {
         if ($request->get('order_id')) {
            $query->where('order_id',  $request->get('order_id'));
             return $query;
        }

        if ($request->get('end_date')) {
            $query->whereDate('created_at', '<=', $request->get('end_date'));
        }
        if ($request->get('start_date')) {
            $query->whereDate('created_at', '>=', $request->get('start_date'));
        }

        if ($request->get('customer_id')) {
            $member = Member::where('username', $request->get('customer_id') )->first();
            if(!empty($member)){
                $query->where('member_id', $member->member_id);
            }
        }



        return $query;
    }

    public function scopeSearchByShop($query)
    {
        if( Auth::user()->user_type == 'shop' ){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
        }
        return $query;
    }

    public static function getDatatableData($request){

        if(empty($request->user_type)){
            $query = self::all();
        }else {
            $query = self::where('order_status', $request->user_type)->get();
        }

        return Datatables::of($query)
            ->editColumn('created_at', function ($data) {
                return date("F j, Y g:i a", strtotime($data->created_at));
            })
            ->editColumn('status', function ($data) {
                switch ($data->order_status){
                    case 1:
                        return 'Pending';
                        break;
                    case 2:
                        return 'Payment Request';
                        break;
                    case 3:
                        return 'Delivery';
                        break;
                    case 4:
                        return 'Complete';
                        break;
                }
            })
            ->addColumn('action', function ($data) {
                $button = '';
                if ($data->order_status != 4){
                        $button .= ' <a class="btn push-10-r btn-danger btn-xs" href="' . route('order-edit-admin', $data->order_id) . '">Edit </a> ';
                }
                $button .= ' <a class="btn btn-info btn-xs" href="' . route('order-view-admin', $data->order_id) . '"> View Detail</a> ';
                return $button;
            })
            ->make(true);


    }

}
