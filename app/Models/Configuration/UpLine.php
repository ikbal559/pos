<?php

namespace Configuration;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class UpLine extends Model
{
    protected $table = 'up_line_bill';

    protected $primaryKey = 'up_line_bill_id';

    protected $fillable = [
       'value'
    ];
}
