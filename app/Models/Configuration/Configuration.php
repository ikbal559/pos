<?php

namespace Configuration;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class Configuration extends Model
{
    protected $table = 'configurations';

    protected $primaryKey = 'configuration_id';

    protected $fillable = [
        'name',  'key',  'value'
    ];
}
