<?php

namespace Configuration;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class Network extends Model
{
    protected $table = 'network_status';


    protected $primaryKey = 'network_status_id';


    protected $fillable = [
        'position',  'network_status_child',  'network_status_count', 'commission'
    ];



}
