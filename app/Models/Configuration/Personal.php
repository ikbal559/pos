<?php

namespace Configuration;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class Personal extends Model
{
    protected $table = 'personal_status';


    protected $primaryKey = 'personal_status_id';


    protected $fillable = [
        'position',  'purchase', 'commission'
    ];


}
