<?php

namespace Configuration;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class PacTracking extends Model
{
    protected $table = 'pac_tracking';


    protected $primaryKey = 'pac_tracking_id';


    protected $fillable = [
        'personal_status_id',  'bill', 'total_user', 'pac_date'
    ];


}
