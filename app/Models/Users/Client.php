<?php

namespace Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Products\StoreProducts;
use Yajra\Datatables\Datatables;
use Auth;
class Client extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'clients';

    protected $primaryKey = 'client_id';

    protected $fillable = [
        'shop_id', 'phone', 'name', 'address', 'total_due'
    ];
    public function scopeSearchByShop($query)
    {
        if( Auth::user()->user_type =='shop'){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
        }
        return $query;
    }
    public function store_in()
    {
        return $this->hasMany(StoreProducts::class, 'client_id', 'client_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'client_id', 'client_id');
    }
    public static function getDatatableData($request){

        $query = self::SearchByShop()->get();
        return Datatables::of($query)
            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-info btn-xs" href="'.route('clients-edit', $data->client_id ).'">Edit</a>';
                $button .= ' <a class="btn btn-success btn-xs" href="'.route('clients-view', $data->client_id ).'">Detail</a>';
                $button .= ' <a class="btn btn-danger btn-xs" href="'.route('clients-payment', $data->client_id ).'">Add Payment</a>';
                return $button;
            })
            ->addColumn('balance', function ($data) {
                $storeIn = $data->store_in->sum('total');
                $payment = $data->payments->sum('amount');
                return $storeIn - $payment;
            })
            ->make(true);


    }


}
