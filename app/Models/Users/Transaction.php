<?php

namespace Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class Transaction extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'transactions';
    public function scopeSearchByShop($query)
    {
        if( Auth::user()->user_type == 'shop' ){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
        }
        return $query;
    }

    protected $primaryKey = 'id';

    protected $fillable = [
        'ref_id', 'shop_id', 'amount', 'type', 'balance', 'description'
    ];
}
