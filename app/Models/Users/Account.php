<?php

namespace Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Account extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'accounts';


    protected $primaryKey = 'account_id';

    protected $fillable = [
        'user_id', 'sponsor_id', 'personal_status_id', 'network_status_id', 'balance', 'points'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id' );
    }

}
