<?php

namespace Users;

use Illuminate\Database\Eloquent\Model;

class UserBusinessTypes extends Model
{
 

    protected $table = 'user_business_types';


    protected $primaryKey = 'user_business_type_id';

    protected $fillable = [
        'user_id', 'business_types_id', 'shop_id'
    ];


}
