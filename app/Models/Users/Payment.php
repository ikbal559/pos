<?php

namespace Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Yajra\Datatables\Datatables;
use Auth;

class Payment extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'payments';

    protected $primaryKey = 'payment_id';

    protected $fillable = [
        'shop_id', 'client_id', 'amount', 'v_no', 'note'
    ];
    public function scopeSearchByShop($query)
    {
        if( Auth::user()->user_type =='shop'){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
        }
        return $query;
    }
 

}
