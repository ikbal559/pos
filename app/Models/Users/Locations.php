<?php

namespace Users;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;

class Locations extends Model
{
    protected $table = 'locations';


    protected $primaryKey = 'location_id';


    protected $fillable = [
        'division_id',  'district_id', 'name', 'status'
    ];

    public static function getDatatableData($request){

        $query = self::all();
        return Datatables::of($query)

            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-info btn-xs" href="'.route('location-edit', $data->location_id ).'">Edit</a>';
                return $button;
            })
            ->make(true);

    }

}
