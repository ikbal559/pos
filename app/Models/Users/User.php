<?php

namespace Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Permission\Permissions;
use Products\BusinessTypes;
use Products\Category;
use Yajra\Datatables\Facades\Datatables;
use Shops\Shops;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name','user_email', 'shop_id',  'username', 'phone', 'password', 'user_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function shop()
    {
        return $this->hasOne(Shops::class, 'shop_id', 'shop_id');
    }
    public function account()
    {
        return $this->hasOne(Account::class, 'user_id', 'id');
    }

    public function profile()
    {
        return $this->hasOne(ProfileDetail::class, 'user_id', 'id');
    }

    public function permissions()
    {
        return $this->hasMany(Permissions::class, 'user_id', 'id');
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'user_business_categories', 'user_id', 'category_id');
    }


    static function getAllListSelect($except = []){
        $users = self::all();
        $userList = [];
        foreach ($users as $user){
            $userList[$user->id] = "{$user->id} - {$user->username} - {$user->name} ";
        }
        return $userList;
    }

    static function getNetWorkListSelect($except = []){
        if(empty($except)){
            $users = self::where('user_type', 'network')->get();
        }else{
            $users = self::where('user_type', 'network')->whereNotIn('id' , $except)->get();
        }
        $userList = [];
        foreach ($users as $user){
            $userList[$user->id] = "{$user->id} - {$user->username} - {$user->name} ";
        }
        return $userList;
    }
    public function scopeSearchByShop($query)
    {
        if( Auth::user()->user_type =='shop'){
            $query->where('shop_id', Auth::user()->shop_id);
            return $query;
        }
        return $query;
    }


    public function scopeSearchBy($query, $request)
    {
        if ($request->get('user_type')) {
            $query->where('user_type', $request->get('user_type'));
        }

        if ($request->get('business_types')) {
            $ids = UserBusinessTypes::where('business_types_id',  $request->get('business_types'))->pluck('user_id');
            $query->whereIn('id',  $ids);
        }

        if ($request->get('category')) {
            $ids = UserBusinessCategories::where('category_id',  $request->get('category'))->pluck('user_id');
            $query->whereIn('id',  $ids);
        }

        if ($request->get('division')) {
            $query->where('division_id',  $request->get('division') );
        }
        if ($request->get('district')) {
            $query->where('district_id',  $request->get('district') );
        }
        if ($request->get('thana')) {
            $query->where('location_id',  $request->get('thana') );
        }

        return $query;
    }

}
