<?php

namespace Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProfileDetail extends Model
{
    use Notifiable;

    protected $table = 'profile_detail';


    protected $primaryKey = 'profile_detail_id';

    protected $fillable = [
        'user_id',
        'father',
        'mother',
        'email',
        'religion',
        'date_of_birth',
        'gender',

        'education',
        'marketing',
        'present_address',
        'permanent_address',

        'myself',
        'special',

        'nominee_name',
        'nominee_fathers_name',
        'nominee_mother_name',
        'nominee_date_of_birth',
        'nominee_gender',
        'nominee_relationship',
        'nominee_address',

        'bank_name',
        'bank_branch',
        'bank_account_name',
        'bank_account_number',

    ];

}
