<?php

namespace Users;

use Illuminate\Database\Eloquent\Model;
use Products\Category;

class UserBusinessCategories extends Model
{
    
    protected $table = 'user_business_categories';


    protected $primaryKey = 'user_business_categories_id';

    protected $fillable = [
        'user_id', 'category_id', 'shop_id'
    ];


}
