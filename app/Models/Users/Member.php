<?php

namespace Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Orders\Orders;
use Yajra\Datatables\Datatables;

class Member extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'members';


    protected $primaryKey = 'member_id';

    protected $fillable = [
        'username',
        'name',
        'om_user_id',
        'address',
        'shop_id',
    ];
    public static function getDatatableData($request){
        $query = self::all();
        return Datatables::of($query)
            ->make(true);
    }
 
    public static function getMemberList()
    {
        $data = self::all();
        $list = [];
        foreach ( $data as $item){
            $list[$item->username] = "{$item->username} {$item->name}";
        }
        return $list;
    }

}
