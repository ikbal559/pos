<?php

namespace Company;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

use Auth;

class CompanyAccount extends Model
{
    protected $table = 'company_accounts';

    protected $primaryKey = 'company_account_id';

    protected $fillable = [
         'value'
    ];
}
