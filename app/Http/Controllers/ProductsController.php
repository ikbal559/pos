<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Orders\Orders;
use Orders\OrdersProducts;
use Products\BusinessTypes;
use Products\Import;
use Products\Export;
use Products\Damage;

use Products\ManageProducts;
use Products\Products;
use Products\Category;

use Products\StoreProducts;
use Shops\Shops;
use Users\Client;
use Users\Locations;
use Users\User;

use Auth;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if( Auth::user()->user_type == 'admin' && !empty($request->shop_id) ){
            $porducts = Products::where( 'shop_id', $request->shop_id )->get();
        }else{
            $porducts = Products::SearchByShop()->get();
        }

        $totalVolume = 0;
        foreach ($porducts as $porduct){
            $total = $porduct->avg_store_in * $porduct->total_store_in;
            $totalVolume += $total;
        }
        return view('Product.index',[
            'totalVolume' => $totalVolume,
            'shops' => Shops::pluck( 'name', 'shop_id')
        ]);
    }

    public function store()
    {
        return view('Product.store');
    }

    public function store_in()
    {
        return view('Product.store_in',
            [
                'products' => Products::getProductList(),
                'clients' => Client::SearchByShop()->pluck('name', 'client_id')
            ]
        );
    }

    public function store_in_edit($id)
    {
        return view('Product.store_in_edit',
                [
                    'store_in' => StoreProducts::find($id)
                ]
            );
    }

    public function store_in_list(Request $request)
    {
        $store_in = StoreProducts::SearchByShop()->SearchBy($request);
        return view('Product.store_in_list',
                [
                    'StoreProducts' => StoreProducts::SearchByShop()->SearchBy($request)->paginate(30),
                    'products' => Products::getProductList(),
                    'clients' => Client::SearchByShop()->pluck('name', 'client_id'),
                    'total' => $store_in->sum('total')
                ]
            );
    }

    public function update($id)
    {

        return view('Product.update', [
            'product' => $product = Products::SearchByShop()->where('product_id', $id)->first()
        ]);
    }

        public function view($id)
    {

        return view('Product.view', [
            'product' => Products::SearchByShop()->where('product_id', $id)->first(),
            'lestSaleProduct' => OrdersProducts::where('product_id', $id)->orderBy('created_at', 'desc')->take(50)->get(),
            'storeInProduct' => StoreProducts::where('product_id', $id)->orderBy('created_at', 'desc')->take(50)->get()
        ]);
    }

    public function get_product_info(Request $request)
    {
        $product = Products::find($request->id);
        if($product){
            $lastQty = $product->total_store_in;
            $volume = $lastQty * $product->avg_store_in;

            $html  = '<div class="list-group">';
            $html  .= '<a class="list-group-item active" href="javascript:void(0)">';
            $html  .= $product->name;
            $html  .= '</a>';

            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html  .= "Last Qty: $lastQty";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "AVG Price: {$product->avg_store_in}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Total Volume: $volume";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "MRP: {$product->mrp}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Dealer Price: {$product->price}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Sales Price: {$product->sale}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Point: {$product->point}";
            $html  .= '</a>';
            $html .= '</div>';
            return $html;

        }else{
            return 'No Product Found';
        }

    }



    public function sales(Request $request)
    {
        if(Auth::user()->user_type != 'shop' ){
            Auth::logout();  return redirect('login');
        }
        $order_id =0;
        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
        }

        return view('Sales.sales', [
             'users' => User::getNetWorkListSelect()
             ,'products' => Products::getProductList()
            ,'order_id' => $order_id
        ]);

    }




    public function manage_products(){
        return view('Manage.index');
    }

    public function manage()
    {
        if(Auth::user()->user_type != 'shop' ){
            Auth::logout();  return redirect('login');
        }
        return view('manage', [
             'products' => Products::getProductList()
            ,'categories' => Category::pluck('category_name', 'category_id')
        ]);

    }


    public function manage_view($id){

        $manage = ManageProducts::SearchByShop()->find($id);
        if($manage){

            if( $manage->manage_products_type  == 'import' ){
                $products = Import::with('product')->where('manage_products_id', $id)->get();
            }
            if( $manage->manage_products_type == 'export' ){
                $products = Export::with('product')->where('manage_products_id', $id)->get();
            }
            if( $manage->manage_products_type == 'damage' ){
                $products = Damage::with('product')->where('manage_products_id', $id)->get();
            }
            
        }
        return view('Manage.view', ['manage' => $manage, 'products' => $products]);

    }
}
