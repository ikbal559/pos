<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Orders\Orders;
use Orders\OrdersProducts;
use Products\Category;
use Products\Products;
use Shops\Shops;
use Users\Member;
use Users\User;

class SalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function sales(Request $request)
    {
        if(Auth::user()->user_type != 'shop' ){
            Auth::logout();  return redirect('login');
        }
        $order_id =0;
        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
        }
        return view('Sales.sales', [
//             'users' => Member::getMemberList()
            'products' => Products::getProductList()
            ,'order_id' => $order_id
        ]);
    }

    public function dealer_sales(Request $request)
    {
        if(Auth::user()->user_type != 'shop' ){
            Auth::logout();  return redirect('login');
        }
        $order_id =0;
        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
        }
        return view('Sales.dealer', [
             //'users' => Member::getMemberList(),
            'products' => Products::getProductList()
            ,'order_id' => $order_id
        ]);
    }


    public $items = [];
    public $total = 0;
    public $qty = 0;
    public $point = 0;
    public $discount = 0;
    public $order_id = 0;

    private function setCart ($request){

        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $this->items = $oldCart['items'];
            $this->total = $oldCart['total'];
            $this->qty = $oldCart['qty'];
            $this->order_id = ( isset($oldCart['order_id']) && $oldCart['order_id'] > 0 ) ? $oldCart['order_id'] : 0;
        }
    }
    public function new_order(Request $request)
    {
        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $this->items = $oldCart['items'];
            $this->total = $oldCart['total'];
            $this->qty = $oldCart['qty'];
            $this->order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
        }

        if($request->qty > 0){

            $product = Products::findOrFail($request->id);
            $total = $request->qty * $product->product_price;
            $this->items[$request->id] = [
                  'qty' =>   $request->qty
                ,'price' => $product->product_price
                ,'total' =>   number_format((float)$total, 2, '.', '')
                ,'product_name' =>  $product->product_name
            ];

        }else{
            if (array_key_exists($request->id, $this->items )){
                unset( $this->items[$request->id] );
            }
        }

        $this->updateTotal();
        $cart = [
            'items' => $this->items,
            'total' => $this->total,
            'qty' => $this->qty,
            'order_id' => $this->order_id
        ];
        $request->session()->put('cart', $cart);
        return response()->json([
            'status' => 'success',
            'cart'   => $cart
        ]);

    }



    private function updateTotal(){
        $this->total = 0;
        $this->qty = 0;

        foreach ($this->items as $item){

            $this->total += $item['total'];
            $this->qty += $item['qty'];
        }
    }


    public function read(Request $request){
        $this->setCart($request);
        return response()->json([
            'status' => 'success',
            'cart'   => [  'items' => $this->items,  'total' => $this->total,    'qty' => $this->qty]
        ]);
    }

    public function cart(Request $request){

        if($request->session()->has('cart')){
            return view('cart', [
                'cart' => $request->session()->get('cart')
            ]);
        }else{
            return redirect()->back();
        }
    }
    


    public function view_order($id){
        $order = Orders::SearchByShop()->findOrFail($id);
        $products = OrdersProducts::where('order_id', $id)->get();
        return view('Orders.view', ['order' => $order, 'products' => $products]);
    }



    public function view_orders(Request $request){

        if( Auth::user()->user_type == 'admin' && !empty($request->shop_id) ){

                $orders =  Orders::SearchBy($request)->where( 'shop_id', $request->shop_id );
                return view('Orders.view_orders', [
                    'orders' => Orders::SearchBy($request)->where( 'shop_id', $request->shop_id )->orderBy('order_id','desc')->paginate(50),
                    'order_total' => $orders->sum('order_total'),
                    'order_profit' => $orders->sum('order_profit'),
                    'order_point' => $orders->sum('order_point'),
                    'order_point_send' => Orders::SearchBy($request)->where( 'shop_id', $request->shop_id )->where('order_status', 2)->sum('order_point'),
                    'order_discount' => $orders->sum('order_discount'),
                    'order_paid' => $orders->sum('paid'),
                    'order_due' => $orders->sum('due'),
                    'shops' => Shops::pluck( 'name', 'shop_id')
                ]);
        }else{

            $orders =  Orders::SearchBy($request)->SearchByShop();
            return view('Orders.view_orders', [
                'orders' => Orders::SearchBy($request)->SearchByShop()->orderBy('order_id','desc')->paginate(50),
                'order_total' => $orders->sum('order_total'),
                'order_profit' => $orders->sum('order_profit'),
                'order_point' => $orders->sum('order_point'),
                'order_point_send' => Orders::SearchBy($request)->SearchByShop()->where('order_status', 2)->sum('order_point'),
                'order_discount' => $orders->sum('order_discount'),
                'order_paid' => $orders->sum('paid'),
                'order_due' => $orders->sum('due'),
                'shops' => Shops::pluck( 'name', 'shop_id')
            ]);
        }
    }

    public function send_points(){
        return view('Orders.send_points', [
            'orders' => Orders::where('order_status', 1)->where('member_id', '>',0)->SearchByShop()->orderBy('order_id','desc')->get(),
            'order_point' => Orders::where('order_status', 1)->where('member_id', '>', 0)->SearchByShop()->sum('order_point')
        ]);
    }
    public function send_points_process(Request $request)
    {
        try{

           $order =  Orders::where('order_status', 1)->where('member_id', '>',0)->SearchByShop()->orderBy('order_id','desc')->first();
            if(!empty($order)){

                if($order->order_point > 0 && !empty($order->member->username)  && !empty($order->shop->phone) ){

                        $url = "http://www.ombazar.com/services/ptoc?key=Tsi4y9lRvfuaAfwMfujZrFMAsbAFYOEcHw0Od5UWcpDYB8NrxPfx5HRN1IHAEFn&from={$order->shop->phone}&to={$order->member->username}&amount={$order->order_point}";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_URL,$url);
                        $result=curl_exec($ch);
                        curl_close($ch);
                    $result = json_decode($result, true);
                    if($result == 200){
                        $order->update([
                            'order_status' => 2
                        ]);
                    }else{
                        $order->update([
                            'order_status' => 3
                        ]);
                    }
                }else{
                    $order->update([
                        'order_status' => 3
                    ]);

                }

                return response()->json([
                    'status' => 'success',
                    'id' => $order->order_id
                ]);
            }else {
                return response()->json([
                    'status' => 'success',
                    'id' => 0
                ]);
            }
            


        }catch (Exception $ex) {

            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage()
            ]);

        }
    }


    public function send_points_retry($id)
    {


           $order =  Orders::where('order_status', 3)->where('member_id', '>',0)->SearchByShop()->where('order_id',$id)->first();
            if(!empty($order)){

                if($order->order_point > 0 && !empty($order->member->username)  && !empty($order->shop->phone) ){

                        $url = "http://www.ombazar.com/services/ptoc?key=Tsi4y9lRvfuaAfwMfujZrFMAsbAFYOEcHw0Od5UWcpDYB8NrxPfx5HRN1IHAEFn&from={$order->shop->phone}&to={$order->member->username}&amount={$order->order_point}";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_URL,$url);
                        $result=curl_exec($ch);
                        curl_close($ch);
                    $result = json_decode($result, true);
                    if($result == 200){
                        $order->update([
                            'order_status' => 2
                        ]);
                    }else{
                        $order->update([
                            'order_status' => 3
                        ]);
                    }
                }else{
                    $order->update([
                        'order_status' => 3
                    ]);

                }

                return redirect()->route('order-list');

            }else {
                return redirect()->route('order-list');
            }




    }
    public function save_order(Request $request){

            $this->setCart($request);
            if($this->total > 0){

                $login_user = Auth::user();
                if ($this->order_id> 0 ){

                    $order = Orders::find($this->order_id);

                    if($order->order_status != 1){
                        return response()->json([
                            'status'  => 'false',
                            'message' => "Order is not editable!"
                        ]);
                    }


                    $order->update([
                        'order_total' => $this->total,
                        'order_qty'   => $this->qty,
                        'order_date' => date('Y-m-d'),
                        'order_discount', $this->discount
                    ]);
                    OrdersProducts::where('order_id', $this->order_id)->delete();;
                }else{
                    $order = Orders::create([
                        'user_id'  => $login_user->id,
                        'order_total' => $this->total,
                        'order_qty' => $this->qty,
                        'order_date' => date('Y-m-d'),
                        'order_discount', $this->discount
                    ]);

                }
                if(!empty($order)){
                    $orderProducts = [];
                    foreach ($this->items as $product_id => $item){
                        $product = Products::find($product_id);
                        array_push($orderProducts, [

                            'shop_id' => $product->shop_id,
                            'order_id' => $order->order_id,
                            'product_id' => $product_id,

                            'qty'   => $item['qty'],
                            'price' => $item['price'],
                            'total' => $item['total']
                        ]);
                    }
                    if(!empty($orderProducts)){
                        OrdersProducts::insert($orderProducts);
                    }
                    $request->session()->forget('cart');
                    return redirect(route('order-view', $order->order_id ));
                }
            }


    }

}
