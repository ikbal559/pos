<?php

    /**
     * Prometheus
     *
     * Single point entry to load all datatables
     *
     * LICENSE:
     *
     * @package     Prometheus
     * @subpackage  Datatables
     * @author      Nick Woodhead <naw103@gmail.com>
     * @copyright   Copyright (c) 2016
     * @license
     * @version     1.0
     * @link        https://www.Prometheus.com
     */

namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use ReflectionClass;
    
    class DataTableController extends Controller
    {
        /**
        * Loads the data table data for any model
        *
        * @author Nick Woodhead <naw103@gmail.com>
        * @todo   
        * @param  str namespace, str model name
        * @return json datatable data
        */
        public function load(Request $request, $namespace, $model)
        {
            $class = new ReflectionClass($namespace . '\\' . $model);
            $modelClass = $class->getMethod('getDatatableData')->class;
            return $modelClass::getDatatableData($request);
        }
    }   
    