<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Orders\Orders;
use Orders\OrdersProducts;
use Products\Category;
use Products\Products;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        parent::__construct();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( !Auth::guest()){
            return redirect('/dashboard');
        }
        return view('auth.login');
    }
 

    public $items = [];
    public $total = 0;
    public $qty = 0;
    public $point = 0;
    public $discount = 0;
    public $order_id = 0;

    private function setCart ($request){

        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $this->items = $oldCart['items'];
            $this->total = $oldCart['total'];
            $this->qty = $oldCart['qty'];
            $this->order_id = ( isset($oldCart['order_id']) && $oldCart['order_id'] > 0 ) ? $oldCart['order_id'] : 0;
        }
    }
    public function new_order(Request $request)
    {
        if($request->session()->has('cart')){
            $oldCart = $request->session()->get('cart');
            $this->items = $oldCart['items'];
            $this->total = $oldCart['total'];
            $this->qty = $oldCart['qty'];
            $this->order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
        }

        if($request->qty > 0){

            $product = Products::findOrFail($request->id);
            $total = $request->qty * $product->product_price;
            $this->items[$request->id] = [
                  'qty' =>   $request->qty
                ,'price' => $product->product_price
                ,'total' =>   number_format((float)$total, 2, '.', '')
                ,'product_name' =>  $product->product_name
            ];

        }else{
            if (array_key_exists($request->id, $this->items )){
                unset( $this->items[$request->id] );
            }
        }

        $this->updateTotal();
        $cart = [
            'items' => $this->items,
            'total' => $this->total,
            'qty' => $this->qty,
            'order_id' => $this->order_id
        ];
        $request->session()->put('cart', $cart);
        return response()->json([
            'status' => 'success',
            'cart'   => $cart
        ]);

    }



    private function updateTotal(){
        $this->total = 0;
        $this->qty = 0;

        foreach ($this->items as $item){

            $this->total += $item['total'];
            $this->qty += $item['qty'];
        }
    }


 
    public function cart(Request $request){

        if($request->session()->has('cart')){
            return view('cart', [
                'cart' => $request->session()->get('cart')
            ]);
        }else{
            return redirect()->back();
        }
    }



    public function save_order(Request $request){

            $this->setCart($request);
            if($this->total > 0){

                $login_user = Auth::user();
                if ($this->order_id> 0 ){

                    $order = Orders::find($this->order_id);

                    if($order->order_status != 1){
                        return response()->json([
                            'status'  => 'false',
                            'message' => "Order is not editable!"
                        ]);
                    }


                    $order->update([
                        'order_total' => $this->total,
                        'order_qty'   => $this->qty,
                        'order_date' => date('Y-m-d'),
                        'order_discount', $this->discount
                    ]);
                    OrdersProducts::where('order_id', $this->order_id)->delete();;
                }else{
                    $order = Orders::create([
                        'user_id'  => $login_user->id,
                        'order_total' => $this->total,
                        'order_qty' => $this->qty,
                        'order_date' => date('Y-m-d'),
                        'order_discount', $this->discount
                    ]);

                }
                if(!empty($order)){
                    $orderProducts = [];
                    foreach ($this->items as $product_id => $item){
                        $product = Products::find($product_id);
                        array_push($orderProducts, [

                            'shop_id' => $product->shop_id,
                            'order_id' => $order->order_id,
                            'product_id' => $product_id,

                            'qty'   => $item['qty'],
                            'price' => $item['price'],
                            'total' => $item['total']
                        ]);
                    }
                    if(!empty($orderProducts)){
                        OrdersProducts::insert($orderProducts);
                    }
                    $request->session()->forget('cart');
                    return redirect(route('order-view', $order->order_id ));
                }
            }


    }

}
