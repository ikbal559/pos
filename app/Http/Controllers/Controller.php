<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use Illuminate\Support\Facades\View;
use Products\Category;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $main_categories;
    public function __construct()
    {
 
    }

    protected function hasAdminPermission($route_name = null){
        $user = Auth::user();
        $access_admin = [];
        if($user->user_type == 'admin' && $user->id > 1){
            $permissions = Permissions::with('group')->where('user_id', $user->id )->where('status', 1 )->get();
            if($permissions->isNotEmpty()){
                foreach ($permissions as $permission){
                    array_push($access_admin, $permission->group->route_name);
                }
                if($route_name && !in_array($route_name, $access_admin)){
                    Auth::logout();  return redirect('login');
                }
            }
            return $access_admin;
        }else{
            if($user->id !=1 ){
                Auth::logout();  return redirect('login');
            }
        }
    }


    protected function sendSMSgetway($to, $message='Welcome to www.ombazar.com'){
        //set POST variables
        $url = 'https://bmpws.robi.com.bd/ApacheGearWS/SendTextMessage?';
        //$url = 'https://bmpws.robi.com.bd/ApacheGearWS/SendTextMultiMessage?';
        $fields = array(
            'Username' => 'Opportunity',
            'Password' => 'om#789Dhaka',
            'To' => '88'.$to,
            'Message' => $message,
            'From' => '8801847169976'
        );
        //url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        //open connection
        $ch = curl_init();
        //set the url, number of POST vars, POST data


        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        //execute post
        curl_exec($ch);
        //close connection
        curl_close($ch);
        //$r  = file_get_contents( "https://bmpws.robi.com.bd/ApacheGearWS/SendTextMessage?Username=Opportunity&Password=om#789Dhaka&To=8801712600741&Message=HI&From=8801847169976");

    }



}
