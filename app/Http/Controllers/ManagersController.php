<?php

namespace App\Http\Controllers;

use Configuration\Network;
use Configuration\Personal;
use Illuminate\Http\Request;


use Auth;
use Permission\PermissionGroup;
use Permission\Permissions;
use Users\Account;
use Users\User;

class ManagersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        if(Auth::user()->id !=1 ){
            Auth::logout();  return redirect('login');
        }
        return view('Managers.index');
    }


    public function add()
    {
        if(Auth::user()->id !=1 ){
            Auth::logout();  return redirect('login');
        }

        return view('Managers.add');
    }

    public function manage($id)
    {
        if(Auth::user()->id !=1 ){
            Auth::logout();  return redirect('login');
        }

        return view('Managers.manage', [
            'groups' => PermissionGroup::all(),
            'permissions' => Permissions::where('user_id', $id)->where('status', 1)->pluck('permission_group_id')->toArray(),
            'user_id' => $id
        ]);
    }

}
