<?php

namespace App\Http\Controllers;

use Permission\Permissions;
use Products\BusinessTypes;
use Products\Category;
use Shops\Shops;
use Users\Locations;
use Users\User;
use Auth;

class ShopsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        return view('Shops.index', [
            'access_admin' => $this->hasAdminPermission('shops'),
            'normal' => $users->where('user_type', 'normal')->count(),
            'customer' => $users->where('user_type', 'customer')->count(),
            'shop' => $users->where('user_type', 'shop')->count(),
            'admin' => $users->where('user_type', 'admin')->count(),

        ]);
    }

    public function store()
    {
        return view('Shops.add');
    }

    public function update($id)
    {
        $shop = Shops::find($id);
        return view('Shops.edit', [
            'shop' => $shop,
            'access_admin' => $this->hasAdminPermission('shops')
        ]);
    }
 
}
