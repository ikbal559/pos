<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Company\CompanyAccount;
use Configuration\Configuration;
use Configuration\Network;
use Configuration\PacTracking;
use Configuration\Personal;
use Configuration\UpLine;
use Illuminate\Support\Facades\Mail;
use Marketing\Marketing;
use Marketing\Sms;
use Orders\Orders;
use Orders\OrdersProducts;
use Permission\Permissions;
use Transaction\AccountPayments;
use Transaction\AccountTransaction;
use Users\Account;
use Users\User;
use Carbon\Carbon;
use DB;

use Illuminate\Http\Request;
use Validator;

class AdminController extends Controller
{

    public function add_manager(Request $request)
    {

        $validator = Validator::make( $request->input(),
            [
                'name' => 'required',
                'username' => 'required|unique:users|max:20',

            ],
            [
                'name.required'    => 'Name is Required',
                'username.required'    => 'User Name is Required',
            ]
        );

        if ($validator->passes()) {

            User::create([
                'password'   => bcrypt($request->input('password')),
                'name' => $request->input('name'),
                'user_type' => 'admin',
                'username' => $request->input('username'),
                'shop_id' => 0,
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Manager Created Successfully.'
            ]);


        }else{
            return response()->json([
                'status' => 'false',
                'message' => 'Username Already Used!'
            ]);
        }

    }
    public function manage_manager(Request $request)
    {

        $validator = Validator::make( $request->input(),
            [
                'user_id' => 'required'

            ],
            [
                'user_id.required'    => 'User is Required',

            ]
        );

        if ($validator->passes()) {

            $persissions = isset($request->permission ) ? $request->input('permission') : null;
            Permissions::where('user_id', $request->user_id)->update(['status'=> 0]);
            if(!empty($persissions)){
                foreach ($persissions as $persission_id => $val){
                    $permission_check = Permissions::where('permission_group_id', $persission_id)->where('user_id', $request->user_id)->first();

                    if(!empty($permission_check) ){
                            $permission_check->status =1;
                            $permission_check->update();
                    }else{
                        Permissions::create([
                            'permission_group_id' => $persission_id,
                            'user_id'=> $request->user_id,
                            'status'=> 1
                        ]);
                    }
                }
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Manager Update Successfully.'
            ]);


        }else{
            return response()->json([
                'status' => 'false',
                'message' => 'Username Already Used!'
            ]);
        }

    }

    public function order_update(Request $request){

        $total_order = 0;
        foreach ($request->order as $order_product_id => $qty){
            $order_p = OrdersProducts::find($order_product_id);
            if(!empty($order_p)){
                $total =  $qty['qty'] * $order_p->price;
                $total_order += $total;
                $order_p->update([
                    'qty'   => $qty['qty'],
                    'total' => $total
                ]);
            }
        }

        $order = Orders::find($request->order_id);
        $order->update([
            'order_discount'        => $request->order_discount,
            'order_service_fee'     => $request->order_service_fee,
            'order_status'        => $request->order_status,
            'order_note'            => $request->order_note,
            'delivery_invoice'      => $request->delivery_invoice,
            'delivery_name'         => $request->delivery_name,
            'order_total'         => $total_order
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Order Update Successfully.'
        ]);
    }

    public function admin_send_sms(Request $request){

        $validator = Validator::make( $request->input(),
            [
                'message' => 'required'

            ],
            [
                'message.required'    => 'message is Required',

            ]
        );

        if ($validator->passes()) {


            if(!empty($request->users)){
                Marketing::create([
                    'user_id' => serialize($request->users),
                    'type' => 'sms',
                    'content' => $request->message,
                    'total' => count($request->users),
                ]);
                $this->sendSMS($request->users, $request->message);
            }
            return response()->json([
                'status' => 'success',
                'message' => 'SMS SEND Successfully.'
            ]);


        }else{
            return response()->json([
                'status' => 'false',
                'message' => 'Invalid'
            ]);
        }
    }

    public function admin_send_order_smsEmail(Request $request){

        $validator = Validator::make( $request->input(),
            [
                'message' => 'required'

            ],
            [
                'message.required'    => 'message is Required',

            ]
        );

        if ($validator->passes()) {


            if(!empty($request->to)){
                Sms::create([
                    'order_id' => $request->order_id,
                    'user_id' => $request->to,
                    'content' => $request->message
                ]);
                $this->sendSMS([$request->to], $request->message);
            }
            return response()->json([
                'status' => 'success',
                'message' => 'SMS SEND Successfully.'
            ]);


        }else{
            return response()->json([
                'status' => 'false',
                'message' => 'Invalid'
            ]);
        }
    }

    public function admin_send_email(Request $request){

        $validator = Validator::make( $request->input(),
            [
                'message' => 'required'

            ],
            [
                'message.required'    => 'message is Required',

            ]
        );

        if ($validator->passes()) {


            if(!empty($request->users)){
                Marketing::create([
                    'user_id' => serialize($request->users),
                    'type' => 'email',
                    'content' => $request->message,
                    'total' => count($request->users),
                ]);
                $this->sendEmail($request->users, $request->message);
            }
            return response()->json([
                'status' => 'success',
                'message' => 'Email SEND Successfully.'
            ]);

        }else{
            return response()->json([
                'status' => 'false',
                'message' => 'Invalid'
            ]);
        }
    }
 
    private function sendSMS($users, $content){
        foreach ($users as $id){
            $to = User::find($id);
            if(!empty($to->username) && $to->username > 5000){
                 $this->sendSMSgetway($to->username, $content);
            }
        }
    }

    private function sendEmail($users, $content){

        $subject = 'Admin | PaikariMarketBD';
        foreach ($users as $id){
            $to = User::find($id);
            if(!empty($to->user_email)){
                Mail::send('Emails.marketing', ["content" => $content], function ($message) use ($to, $subject, $content) {
                    $message
                        ->to($to->user_email, $to->name)
                        ->subject($subject)
                        ->from(env('MAIL_USERNAME'), 'PaikariMarketBD - Admin');
                });
            }
        }
    }


}
