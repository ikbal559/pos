<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Products\Damage;
use Products\Entry;
use Products\Products;
use Products\Category;
use Products\BusinessTypes;
use Products\StoreProducts;
use Users\Locations;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ProductController extends Controller
{

    public function index(Request $request)
    {

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->input(),
            [

                'name' => 'required',
                'mrp' => 'required|numeric|between:0,99999.99',
                'sale' => 'required|numeric|between:0,99999.99',
                'price' => 'required|numeric|between:0,99999.99',
            ],
            [

                'name.required'  => 'Product Name is Required',
                'sale.required' => 'Special Price is Required',
                'mrp.required' => 'MRP Price is Required',
                'price.required' => 'Product Price is Required',
            ]
        );
        if ($validator->passes()) {

            $checkCode = Products::where( 'code', $request->code )->where('shop_id', $request->shop_id)->first();
            if(empty($checkCode)){
                Products::create($request->input());
                return response()->json([
                    'status' => 'success',
                    'message' => 'Product Created Successfully.'
                ]);
            }else{
                return response()->json([
                    'status' => 'false',
                    'message' => $request->code . " is already used with ". $checkCode->product_id .' ' .$checkCode->name
                ]);
            }



        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }

    public function store_in_save(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'product_id' => 'required',
                'qty' => 'required|numeric|between:0,99999',
                'price' => 'required|numeric|between:0,99999.99',
                'v_no' => 'required',
            ],
            [
                'product_id.required'  => 'Product is Required',
                'price.required'  => 'Price is Required',
                'qty.required'  => 'Qty is Required',
                'v_no.required'  => 'Invoice No is Required',

            ]
        );
        if ($validator->passes()) {


            $storeP = $request->input();
            $storeP['total'] = $request->qty * $request->price;

            $product = Products::find($request->input('product_id'));
            if($request->price > $product->sale){
                return  "Buy Price Can't be grather then MRP";
            }

            $lastQty =  $product->total_store_in;
            $volume = $lastQty * $product->avg_store_in;
            $volume += $storeP['total'];
            $lastQty += $request->qty;

            $productAvg = $volume / $lastQty;
            $profit = $product->sale - $productAvg;

            $product->update([
                'avg_store_in' => $productAvg,
                'total_store_in' => $lastQty,
                'point' => $profit / .30
            ]);


            $storeP['avg_store_in'] = $productAvg;
            $storeP['total_store_in'] =$lastQty;
            StoreProducts::create($storeP);

            $product = Products::find($request->input('product_id'));
            $volume = $product->total_store_in * $product->avg_store_in;

            $html  = '<div class="list-group">';
            $html  .= '<a class="list-group-item active" href="javascript:void(0)" style="background-color: #65d25c; border-color: #65d25c;">';
            $html  .= $product->name;
            $html  .= '</a>';

            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html  .= "Last Qty: ". $product->total_store_in;
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "AVG Price: ". $product->avg_store_in;
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Total Volume: $volume";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "MRP: {$product->mrp}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Dealer Price: {$product->price}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Sales Price: {$product->sale}";
            $html  .= '</a>';

            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Point: ". $product->point;
            $html  .= '</a>';

            $html .= '</div>';
            return $html;




        }else {
             echo 'error';
        }
    }


    public function store_in_update(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'store_product_id' => 'required',
                'qty' => 'required|numeric|between:0,99999',
                'price' => 'required|numeric|between:0,99999.99',

            ],
            [
                'store_product_id.required'  => 'store_product_id is Required',
                'price.required'  => 'Price is Required',
                'qty.required'  => 'Qty is Required',


            ]
        );
        if ($validator->passes()) {

            $storeP = $request->input();
            $storeP['total'] = $request->qty * $request->price;

            $storeIn = StoreProducts::find($request->store_product_id);

            $product = Products::find( $storeIn->product_id );

            $lastQty = $product->total_store_in;
            if($storeIn->qty != $request->qty){
                if($storeIn->qty > $request->qty){
                    $change = $storeIn->qty - $request->qty;
                    $lastQty = $product->total_store_in - $change;

                }else{
                    $change = $request->qty - $storeIn->qty;
                    $lastQty = $product->total_store_in + $change;
                }
            }

            $volume  = $lastQty * $product->avg_store_in;
            if($storeIn->price != $request->price){
                if($storeIn->price > $request->price){
                    $change = $storeIn->price - $request->price;
                    $change *= $request->qty;
                    $volume -= $change;
                }else{
                    $change = $request->price - $storeIn->price;
                    $change *= $request->qty;
                    $volume += $change;
                }
            }





            $productAvg = $volume / $lastQty;
            $profit = $product->sale - $productAvg;

            $product->update([
                'avg_store_in' => $productAvg,
                'total_store_in' => $lastQty,
                'point' => $profit / .30
            ]);

            $storeP['avg_store_in']   = $productAvg;
            $storeP['total_store_in'] = $lastQty;
            $storeP['qty'] = $request->qty;
            $storeP['price'] = $request->price;
            $storeIn->update($storeP);





            $product = Products::find($storeIn->product_id);
            $volume =  $product->total_store_in * $product->avg_store_in;
            $html  = '<div class="list-group">';
            $html  .= '<a class="list-group-item active" href="javascript:void(0)" style="background-color: #65d25c; border-color: #65d25c;">';
            $html  .= $product->name;
            $html  .= '</a>';

            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html  .= "Last Qty: ". $product->total_store_in;
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "AVG Price: ". $product->avg_store_in;
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Total Volume: $volume";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "MRP: {$product->mrp}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Dealer Price: {$product->price}";
            $html  .= '</a>';
            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Sales Price: {$product->sale}";
            $html  .= '</a>';

            $html  .= '<a class="list-group-item" href="javascript:void(0)">';
            $html .= "Point: ". $product->point;
            $html  .= '</a>';

            $html .= '</div>';
            return $html;




        }else {
             echo 'error';
        }
    }


    public function edit(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'code' => 'required',
                'name' => 'required',
                'mrp' => 'required|numeric|between:0,99999.99',
                'sale' => 'required|numeric|between:0,99999.99',
                'price' => 'required|numeric|between:0,99999.99',
            ],
            [
                'code.required'  => 'Product Code is Required',
                'name.required'  => 'Product Name is Required',
                'sale.required' => 'Special Price is Required',
                'mrp.required' => 'MRP Price is Required',
                'price.required' => 'Product Price is Required',
            ]
        );
        if ($validator->passes()) {

            $data = Products::find($request->product_id);
            $updateData = $request->input();
            $profit = $request->sale - $data->avg_store_in;
            $updateData['point']= $profit / .30;
            $data->update($updateData);

            return response()->json([
                'status' => 'success',
                'message' => 'Data Saved Successfully',
            ]);
        } else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }


    public function entry(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'quantity' => 'required|numeric'
            ],
            [

                'quantity.required' => 'Product Quantity is Required',
            ]
        );

        if ($validator->passes()) {

            Entry::create($request->input());
            return response()->json([
                'status' => 'success',
                'message' => 'Product Added Successfully.'
            ]);

        }else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }

    public function entry_update(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'quantity' => 'required|numeric'
            ],
            [

                'quantity.required' => 'Product Quantity is Required',
            ]
        );

        if ($validator->passes()) {

            $data = Entry::find($request->entry_id);
            $data->update($request->input());
            return response()->json([
                'status' => 'success',
                'message' => 'Product Added Successfully.'
            ]);

        }else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }

    public function damage(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'quantity' => 'required|numeric',

            ],
            [
                'quantity.required'  => 'Product Quantity is Required',

            ]
        );

        if ($validator->passes()) {

            Damage::create($request->input());
            return response()->json([
                'status' => 'success',
                'message' => 'Damage Update Successfully.'
            ]);

        }else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }


    public function damage_update(Request $request)
    {
        $validator = Validator::make($request->input(),
            ['quantity' => 'required|numeric' ],
            [ 'quantity.required'  => 'Product Quantity is Required']
        );

        if ($validator->passes()) {

            $data = Damage::find( $request->damage_id );
            $data->update( $request->input() );

            return response()->json([
                'status' => 'success',
                'message' => 'Damage Update Successfully.'
            ]);

        }else{

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }





    public function storeCategory(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'category_name' => 'required',
            ],
            [
                'category_name.required' => 'Category Name is Required',
            ]
        );

        if ($validator->passes()) {

            Category::create($request->input());

            return response()->json([
                'status' => 'success',
                'message' => 'Category Created Successfully.'
            ]);

        } else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }



    public function updateCategory(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'category_name' => 'required',
            ],
            [
                'category_name.required' => 'Category is Required',
            ]
        );

        if ($validator->passes()) {

            $category = Category::find($request->category_id);
            $category->update($request->input());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Data Saved Successfully',
                ]);

        } else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }



    public function storeBusinessType(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Name is Required',
            ]
        );

        if ($validator->passes()) {

            BusinessTypes::create($request->input());

            return response()->json([
                'status' => 'success',
                'message' => 'Business Type Created Successfully.'
            ]);

        } else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }



    public function updateBusinessType(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Name is Required',
            ]
        );

        if ($validator->passes()) {

            $category = BusinessTypes::find($request->business_types_id);
            $category->update($request->input());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Data Saved Successfully',
                ]);

        } else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }

    public function manageLocation(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Name is Required',
            ]
        );

        if ($validator->passes()) {

            if(!empty($request->location_id)){
                $data = Locations::find($request->location_id);
                $data->update($request->input());
            }else{
                Locations::create($request->input());
            }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Data Saved Successfully',
                ]);

        } else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }


    public function get_locations(Request $request)
    {

        if($request->type =='division_id'){
            $data = Locations::where($request->type, $request->id)->where('district_id', 0)->pluck('name', 'location_id');
        }
        if($request->type =='district_id'){
            $data = Locations::where($request->type, $request->id)->pluck('name', 'location_id');
        }

        $html = '<option value="">Select</option>';
        foreach ($data as $key => $item){
            $html .='<option value="'.$key.'">'.$item.'</option>';
        }
        echo  $html;

    }




}
