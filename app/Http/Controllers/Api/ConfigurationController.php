<?php

namespace App\Http\Controllers\Api;

use Configuration\Configuration;
use Configuration\Network;
use Configuration\Personal;

use Configuration\UpLine;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class ConfigurationController extends Controller
{


    public function personal(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'position' => 'required',
                'purchase' => 'required',
                'commission' => 'required',
            ],
            [
                'position.required'    => 'Position is Required',
                'purchase.required'    => 'Purchase is Required',
                'commission.required'  => 'Commission is Required',
            ]
        );

        if ($validator->passes()) {

            if($request->personal_status_id > 0 ){
                $data = Personal::find( $request->personal_status_id );
                $data->update( $request->input() );

            }else{
                Personal::create($request->input());
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Personal Configuration Save Successfully.'
            ]);

        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }



    public function network(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'position' => 'required',
                'network_status_child' => 'required',
                'network_status_count' => 'required',
                'commission' => 'required',
            ],
            [
                'position.required'    => 'Position is Required',
                'network_status_child.required'    => 'Child Status is Required',
                'network_status_count.required'    => 'Count is Required',
                'commission.required'  => 'Commission is Required',
            ]
        );

        if ($validator->passes()) {

            if($request->network_status_id > 0 ){
                $data = Network::find( $request->network_status_id );
                $data->update( $request->input() );

            }else{
                Network::create($request->input());
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Network Configuration Save Successfully.'
            ]);

        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }


    public function configuration_edit(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'value' => 'required|numeric|between:0,99999.99',

            ],
            [
                'value.required'    => 'value is Required',
            ]
        );

        if ($validator->passes()) {

            if($request->configuration_id > 0 ){
                $data = Configuration::find( $request->configuration_id );
                $data->update( $request->input() );

            }

            return response()->json([
                'status' => 'success',
                'message' => 'Configuration Save Successfully.'
            ]);

        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }

    public function up_line_bill_manage(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'value' => 'required|numeric|between:0,99999.99',
            ],
            [
                'value.required'    => 'value is Required',
            ]
        );

        if ($validator->passes()) {

            if($request->up_line_bill_id > 0 ){
                $data = UpLine::find( $request->up_line_bill_id );
                $data->update( $request->input() );

            }else{
                UpLine::create($request->input());
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Configuration Save Successfully.'
            ]);

        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }





}
