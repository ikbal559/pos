<?php

namespace App\Http\Controllers\Api;

use Users\UserBusinessTypes;
use Shops\Shops;
use Transaction\AccountTransaction;
use Transaction\ShopPayment;
use Users\Account;
use Users\ProfileDetail;
use Users\User;
use Users\UserBusinessCategories;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class NetworkController extends Controller
{

    private $transaction_account =[];
    public function registration_user(Request $request)
    {
        $validator = Validator::make( $request->input(),
            [
                'users.name' => 'required',
                'users.username' => 'required|unique:users|max:11',
                'users.password' => 'required',

            ],
            [
                'users.name.required'        => 'Name is Required',
                'users.username.required'    => 'username is Required',
                'users.password.required'    => 'Password is Required',
            ]
        );

        if ($validator->passes()) {
            $shop = Shops::create([
                'phone'             => $request->input('users.username'),
                'name'              => $request->input('shop.name'),
                'address'           => $request->input('shop.address'),
                'division_id'       => $request->input('shop.division'),
                'district_id'       => $request->input('shop.district'),
                'location_id'       => $request->input('shop.thana'),
                'reference_name'    => $request->input('shop.reference_name'),
                'reference_phone'   => $request->input('shop.reference_phone'),
            ]);
            $user = User::create([
                'shop_id'           => $shop->shop_id,
                'password'   => bcrypt($request->input('users.password')),
                'name'       => $request->input('users.name'),
                'user_email'       => $request->input('users.email'),
                'user_type'    => 'normal',
                'username'   => $request->input('users.username'),
                'phone'      => $request->input('users.phone')
            ]);
            $sms = 'Thank you for register to www.paikarimarketbd.com. Please wait for your information verify. HotLine 01848304555';
            $this->sendSMSgetway($request->input('users.username'), $sms);

            if(!empty($request->input('business_type'))){
                $data = [];
                foreach ($request->input('business_type') as $item){
                    array_push($data, [
                        'business_types_id'   => $item,
                        'shop_id'       => $shop->shop_id,
                        'user_id'       => $user->id,
                    ]);
                }
                UserBusinessTypes::insert($data);
            }

            if(!empty($request->input('business_category'))){
                $data = [];
                foreach ($request->input('business_category') as $item){
                    array_push($data, [
                        'category_id'   => $item,
                        'shop_id'       => $shop->shop_id,
                        'user_id'       => $user->id,
                    ]);
                }
                UserBusinessCategories::insert($data);
            }
            return response()->json([
                'status' => 'success',
                'message' => 'Congratulations account created successfully. Please wait Until we verify your information. Help: 01848304555'
            ]);

        }else{

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }

    public function update_profile(Request $request)
    {
        if($request->input('type') == 'info'){
            $validator = Validator::make( $request->input(),
                [
                    'name' => 'required'
                ],
                [
                    'name.required'    => 'Name is Required'
                ]
            );

            if ($validator->passes()) {

                $user = User::find($request->user_id);
                $user->name = $request->name;
                $user->hide_phone = (isset($request->hide_phone) && $request->hide_phone > 0 ) ? $request->hide_phone : 0;
                $user->update();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Profile update successfully.'
                ]);

            }else{

                $errors = array_values($validator->errors()->getMessages());
                return response()->json([
                    'status' => 'validation',
                    'message' => $errors
                ]);

            }
        }

        if($request->input('type') == 'username'){

            $validator = Validator::make( $request->input(),
                [
                    'username' => 'required|unique:users|max:20'
                ],
                [
                    'username.required'    => 'Username is Required'
                ]
            );

            if ($validator->passes()) {

                $user = User::find($request->user_id);
                $user->username = $request->username;
                $user->update();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Username update successfully.'
                ]);

            }else{


                return response()->json([
                    'status' => 'false',
                    'message' => "Invalid username or already used!"
                ]);

            }
        }


        if($request->input('type') == 'password'){

            $validator = Validator::make( $request->input(),
                [
                    'password' => 'required'
                ],
                [
                    'password.required'    => 'Password is Required'
                ]
            );

            if ($validator->passes()) {

                $user = User::find($request->user_id);
                $user->password = bcrypt($request->input('password'));
                $user->update();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Password update successfully.'
                ]);

            }else{

                return response()->json([
                    'status' => 'false',
                    'message' => "Invalid Password!"
                ]);

            }
        }


        if($request->input('type') == 'photo'){

            $validator = Validator::make( $request->input(),
                [
                    'user_images' => 'required'
                ],
                [
                    'user_images.required'    => 'Images is Required'
                ]
            );

            if ($validator->passes()) {

                $user = User::find($request->user_id);
                $user->user_images = $request->input('user_images');
                $user->update();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Photo update successfully.'
                ]);

            }else{

                return response()->json([
                    'status' => 'false',
                    'message' => "Invalid Password!"
                ]);

            }
        }
    }
    public function update_profile_detail(Request $request)
    {

        $validator = Validator::make( $request->input(),
            [
                'user_id' => 'required',

            ],
            [
                'user_id.required'    => 'User is Required',
            ]
        );

        if ($validator->passes()) {
            $user_profile = ProfileDetail::where('user_id', $request->user_id)->first();
            if(empty($user_profile)){
                ProfileDetail::create($request->except(['_token']));
            }else{
                $user_profile->update($request->except(['_token']));
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Data Save successfully.'
            ]);

        }else{
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }

  public function balance_transfer(Request $request)
    {
        $validator = Validator::make( $request->input(),
            [
                'to_id' => 'required',
                'amount' => 'required'
            ],
            [
                'to_id.required'    => 'User is Required',
                'amount.required'   => 'Amount is Required'
            ]
        );

        if ($validator->passes()) {

            $receiver = User::find($request->input('to_id'));
            $user = User::find($request->input('user_id'));

            if(!empty($receiver) && $receiver->user_type == 'network' && !empty($receiver->account) && !empty($user) && $user->id != $receiver->id &&  $request->input('amount') > 0){

                $account = $user->account;
                if($account->balance > $request->input('amount')){
                    // Sender
                    $account->balance -= $request->input('amount');
                    $account->update();
                    array_push($this->transaction_account, [
                        'user_id' => $user->id,
                        'description' => "Send Balance to {$receiver->username}",
                        'type' => 'debit',
                        'amount' => $request->input('amount'),
                        'balance' => $account->balance,
                    ]);

                    // Receiver
                    $account = $receiver->account;
                    $account->balance += $request->input('amount');
                    $account->update();

                    array_push($this->transaction_account, [
                        'user_id' => $receiver->id,
                        'description' => "Receive Balance from {$user->username}",
                        'type' => 'credit',
                        'amount' => $request->input('amount'),
                        'balance' => $account->balance,
                    ]);
                    if(!empty($this->transaction_account)){
                        AccountTransaction::insert($this->transaction_account);
                    }
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Balance Transfer successfully.'
                    ]);

                }else{
                    return response()->json([
                        'status' => 'false',
                        'message' => "You don't have sufficient balance to make this transaction!"
                    ]);
                }

            }else{
                return response()->json([
                    'status' => 'false',
                    'message' => "Invalid User ID "
                ]);
            }
        }else{

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }

  public function balance_transfer_shop(Request $request)
    {
        $validator = Validator::make( $request->input(),
            [
                'to_id' => 'required',
                'amount' => 'required'
            ],
            [
                'to_id.required'    => 'Shop is Required',
                'amount.required'   => 'Amount is Required'
            ]
        );

        if ($validator->passes()) {

            $receiver = Shops::find($request->input('to_id'));
            $user = User::find($request->input('user_id'));

            if(!empty($receiver)  && !empty($user)   &&  $request->input('amount') > 0){

                $account = $user->account;
                if($account->balance > $request->input('amount')){
                    // Sender
                    $account->balance -= $request->input('amount');
                    $account->update();
                    array_push($this->transaction_account, [
                        'user_id' => $user->id,
                        'description' => "Send Balance to {$receiver->user->username}",
                        'type' => 'debit',
                        'amount' => $request->input('amount'),
                        'balance' => $account->balance,
                    ]);


                    $receiver->points += $request->input('amount');
                    $receiver->update();

                    ShopPayment::create([
                        'shop_id'   => $receiver->shop_id,
                        'amount' => $request->input('amount'),
                        'description' => "BT FROM {$user->username}",
                        'balance' => $receiver->points
                    ]);

                    if(!empty($this->transaction_account)){
                        AccountTransaction::insert($this->transaction_account);
                    }
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Balance Transfer successfully.'
                    ]);

                }else{
                    return response()->json([
                        'status' => 'false',
                        'message' => "You don't have sufficient balance to make this transaction!"
                    ]);
                }

            }else{
                return response()->json([
                    'status' => 'false',
                    'message' => "Invalid Shop ID "
                ]);
            }
        }else{

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }


  public function get_detail(Request $request)
    {
        $validator = Validator::make( $request->input(),
            [
                'id' => 'required'
            ],
            [
                'id.required'    => 'User is Required'
            ]
        );

        if ($validator->passes()) {


            $user = User::find($request->input('id'));

            $level01 = Account::with('user')->where('sponsor_id', $user->account->account_id )->get();
            return response()->json([
                'status' => 'validation',
                'data' => [
                    'user' => $user,
                    'net' => $level01
                ]
            ]);


        }else{

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }



}
