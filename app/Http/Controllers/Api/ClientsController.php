<?php

namespace App\Http\Controllers\Api;


use Orders\Orders;
use Users\Client;
use Users\Payment;
use Users\Transaction;
use Validator;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class ClientsController extends Controller
{


    public function store(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
                'phone' => 'required'
            ],
            [
                'name.required'  => 'Name is Required',
                'phone.required'  => 'Phone is Required'
            ]
        );

        if ($validator->passes()) {

            $client = Client::where('shop_id', $request->shop_id)->where('phone', $request->phone)->first();
            if(!empty($client)){
                return response()->json([
                    'status' => 'false',
                    'message' => 'Phone Number already Used!'
                ]);
            }

            Client::create($request->input());



            return response()->json([
                'status' => 'success',
                'message' => 'Client Created Successfully.'
            ]);


        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }



    public function edit(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
                'phone' => 'required'
            ],
            [
                'name.required'  => 'Name is Required',
                'phone.required'  => 'Phone is Required'
            ]
        );
        if ($validator->passes()) {
            $user = Client::find($request->client_id);

                $user->update(
                    [
                        'name' => $request->input('name'),
                        'phone' => $request->input('phone'),
                        'address' => $request->input('address'),
                    ]
                );
                return response()->json([
                    'status' => 'success',
                    'message' => 'Client Update Successfully',
                ]);

        } else {
            $errors = array_values($validator->errors()->getMessages());

            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }

    public function add_payment(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'client_id' => 'required',
                'shop_id' => 'required',
                'amount' => 'required',
            ],
            [
                'client_id.required'  => 'Client is Required',
                'shop_id.required'  => 'Shop is Required',
                'amount.required'  => 'Amount is Required',
            ]
        );
        if ($validator->passes()) {

            $payment = Payment::create($request->except('name', 'phone'));

            $cashIn  = Orders::where('shop_id', $request->shop_id)->sum('paid');
            $cashOut = Payment::where('shop_id', $request->shop_id)->sum('amount');

            Transaction::create([
                'shop_id' => $request->shop_id,
                'ref_id' => $payment->payment_id,
                'amount' => $request->amount,
                'type' => 'Payment',
                'description' => 'Pay To '. $request->name,
                'balance' => $cashIn - $cashOut,
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Payment added Successfully.'
            ]);

        }else {
            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }
    }





}
