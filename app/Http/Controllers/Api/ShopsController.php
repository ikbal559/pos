<?php

namespace App\Http\Controllers\Api;

use Products\BusinessTypes;
use Transaction\ShopPayment;
use Users\User;
use Shops\Shops;

use Users\UserBusinessCategories;
use Users\UserBusinessTypes;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class ShopsController extends Controller
{


    public function store(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'username' => 'required|unique:users|max:20',
                'password' => 'required',
            ],
            [
                'name.required'  => 'Name is Required',
                'phone.required'  => 'Phone is Required',
                'address.required'  => 'Address is Required',
                'username.required'  => 'Username is Required',
                'password.required'  => 'Password is Required',
            ]
        );

        if ($validator->passes()) {


            $shop = Shops::create($request->except(['username', 'password']));


            User::create([
                'password'   => bcrypt($request->input('password')),
                'name' => $request->input('owner'),
                'shop_id' => $shop->shop_id,
                'user_type' => 'shop',
                'username' => $request->input('username')
            ]);

//            $sms = 'Your store is successfully created. HotLine 01848304555';
//            $this->sendSMSgetway($request->input('username') , $sms);

            return response()->json([
                'status' => 'success',
                'message' => 'Shop Created Successfully.'
            ]);

        }else {

            $errors = array_values($validator->errors()->getMessages());
            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);

        }
    }



    public function edit(Request $request)
    {
        $validator = Validator::make($request->input(),
            [
                'name' => 'required',
                'address' => 'required',
            ],
            [
                'name.required'  => 'Name is Required',
                'address.required'  => 'Address is Required',
            ]
        );

        if ($validator->passes()) {

            $data = Shops::find($request->shop_id);


            $data->update(
                $request->except( 'user_username', 'user_name',  'user_pass')
            );


            $user = User::find($data->user->id);



            if(empty($request->input('user_pass'))){
                $user->update(
                    [
                        'name' => $request->input('user_name'),
                        'username' => $request->input('user_username')

                    ]
                );
            }else{
                $user->update(
                    [
                        'name' => $request->input('user_name'),
                        'username' => $request->input('user_username'),
                        'password'   => bcrypt($request->input('user_pass')),
                    ]
                );
            }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Shop Update Successfully',
                ]);

        } else {
            $errors = array_values($validator->errors()->getMessages());

            return response()->json([
                'status' => 'validation',
                'message' => $errors
            ]);
        }

    }




}
