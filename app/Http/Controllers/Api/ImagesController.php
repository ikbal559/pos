<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Intervention\Image\Facades\Image;


class ImagesController extends Controller
{

    /**
     * Profile Image Upload Feature
     */
    public function profileImage(Request $request){

        $image = $request->image;
        $height = $request->height;
        $width = $request->width;
        $image_x = $request->image_x;
        $image_y = $request->image_y;


        // Return error if missing required information
        if (!$image) {
            return response()->json([
                'status' => 'validation',
                'message' => 'Images Not found!'
            ]);
        }

        // Set up Image Name
        $image_name =md5(time()).'.jpg';
        
        $folder = $request->folder;

        $image_path = base_path('public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$image_name);

        try{
            Image::make($image)->crop($width,$height,$image_x,$image_y)->resize(220, 220)->save($image_path);
        }catch (Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => 'Image Not Saved'
            ]);
        }

        return response()->json([
            'status'=> 'success',
            'message' => 'Profile image updated !',
            'image' =>  asset('images'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$image_name),
            'image_name' => $image_name
        ]);
    }
}
