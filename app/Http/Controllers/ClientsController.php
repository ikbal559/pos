<?php

namespace App\Http\Controllers;

use Orders\Orders;
use Products\StoreProducts;
use Users\Client;
use Users\Payment;
use Users\Transaction;
use Users\User;
use Auth;

class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('Clients.index');
    }

    public function store()
    {
        return view('Clients.add');
    }

    public function update($id)
    {

        return view('Clients.edit', [
            'user' => Client::SearchByShop()->where('client_id' , $id)->first()
        ]);
    }

    public function show($id)
    {

        return view('Clients.view', [
            'client' => Client::SearchByShop()->where('client_id' , $id)->first(),
            'storeInProduct' => StoreProducts::where('client_id', $id)->orderBy('created_at', 'desc')->take(50)->get(),
            'clientPayments' => Payment::where('client_id', $id)->orderBy('created_at', 'desc')->take(50)->get()
        ]);
    }


    public function payment($id)
    {
        return view('Clients.payment',[
            'client' => Client::SearchByShop()->where('client_id' , $id)->first(),
        ]);
    }

    public function payments_report()
    {
        $cashIn = Orders::SearchByShop()->sum('paid');
        $cashOut = Payment::SearchByShop()->sum('amount');

        return view('Clients.payments_report',[
            'payments_report' => Transaction::SearchByShop()->orderBy('updated_at', 'desc')->take(100)->get(),
            'cashIn' =>$cashIn,
            'cashOut' => $cashOut
        ]);
    }

}
