<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Orders\Orders;
use Users\Member;

class MembersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if( Auth::user()->user_type == 'admin'){
            return view('Members.index');
        }
        return false;
    }

//    public function member_report(Request $request)
//    {
//        $member = [];
//        $memberOrders = [];
//        $member = [];
//        if($request->member_id){
//            $member = Member::where('username', $request->member_id)->first();
//            if(!empty($member)){
//                $memberOrders = Orders::where('member_id', $member->member_id)->get();
//            }
//        }
//
//        return view('Members.view', ['member' => $member , 'memberOrders' => $memberOrders]);
//    }

//    public function store()
//    {
//        return view('Members.add');
//    }
//
//    public function update($id)
//    {
//        return view('Members.edit', [
//            'user' => User::find($id)
//        ]);
//    }

}
