<?php

namespace App\Http\Controllers;
use Configuration\PacTracking;
use DB;
use Auth;

use Configuration\Personal;
use Configuration\Configuration;
use Orders\Orders;
use Orders\OrdersProducts;
use Users\Account;
use Users\Payment;
use Users\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Products\BusinessTypes;
use Products\Category;
use Users\Locations;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public $users_list = [];

    public function account_payment()
    {
        return view('Admin.account_payment' ,[ 'access_admin' => $this->hasAdminPermission('account-list') ]);
    }

    public function account_add_payment($id)
    {
        return view('Admin.account_add_payment' ,[ 'access_admin' => $this->hasAdminPermission('account-list'), 'user' => User::find($id) ]);
    }

    public function account_list()
    {
        return view('Admin.admin_account_list' ,[ 'access_admin' => $this->hasAdminPermission('account-list') ]);
    }

    public function user_update()
    {
        return view('Admin.user_update', ['users' => User::getAllListSelect(), 'access_admin' => $this->hasAdminPermission('admin-user-update') ]);
    }

    public function marketing(Request $request){


        $users = User::leftjoin( 'shops', 'users.shop_id', '=', 'shops.shop_id' )
        ->select('users.*', 'shops.division_id', 'shops.district_id', 'shops.location_id')
        ->searchBy($request)
        ->paginate(50);



        return view('Admin.marketing',[
            'business_types' => BusinessTypes::pluck('name',  'business_types_id'),
            'category' => Category::where('parent_id', 0)->pluck('category_name',  'category_id'),

            'division' => Locations::where('division_id', 0)->pluck('name', 'location_id'),
            'district' => ($request->division) ?  Locations::where('division_id', $request->division)->pluck('name', 'location_id') : [],
            'locations' => ($request->district) ?  Locations::where('district_id', $request->district)->pluck('name', 'location_id') : [],

            'users' => $users,
 
        ]);
    }

    public function orders(){
        $orders = Orders::all();

        return view('Admin.order_list',[
            'pending' => $orders->where('order_status', 1)->count(),
            'request_payment' => $orders->where('order_status', 2)->count(),
            'delivery' => $orders->where('order_status', 3)->count(),
            'complete' => $orders->where('order_status', 4)->count(),
        ]);
    }

    public function cash_report(){
        $cashIn = Orders::SearchByShop()->sum('paid');
        $cashOut = Payment::SearchByShop()->sum('amount');

        return view('Admin.cash_report',[
            'cashIn' =>$cashIn,
            'cashOut' => $cashOut
        ]);
    }
    
    public function admin_history_sms(){
        return view('Admin.admin_history_sms');
    }

    public function admin_history_email(){
        return view('Admin.admin_history_email');
    }

    public function order_edit($id){
        $order = Orders:: findOrFail($id);
        $products = OrdersProducts::where('order_id', $id)->get();
        return view('Admin.order_edit', ['order' => $order, 'products' => $products]);
    }
    public function order_view($id){
        $order = Orders:: findOrFail($id);
        $products = OrdersProducts::where('order_id', $id)->get();
        return view('Admin.order_view', ['order' => $order, 'products' => $products]);
    }
 

}
