<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Products\Products;
use Orders\Orders;
use Orders\OrdersProducts;

use Auth;
use Shops\Shops;
use Users\Member;
use Users\Payment;
use Users\Transaction;
use Validator;


class CartDealerController extends Controller
{
    public $items = [];
    public $total = 0;
    public $qty = 0;
    public $point = 0;
    public $discount = 0;
    public $order_id = 0;

    private function setCart ($request){

        if($request->session()->has('dealer_cart')){
            $oldCart = $request->session()->get('dealer_cart');

            $this->items = $oldCart['items'];
            $this->total = $oldCart['total'];
            $this->qty = $oldCart['qty'];
            $this->point = $oldCart['point'];
            $this->order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
        }
    }
    public function add(Request $request)
    {

       $product = Products::find($request->id);
       if(!empty($product) && $product->total_store_in >= $request->qty ){

            if($request->session()->has('dealer_cart')){
                $oldCart = $request->session()->get('dealer_cart');
                $this->items = $oldCart['items'];
                $this->total = $oldCart['total'];
                $this->qty = $oldCart['qty'];
                $this->point = $oldCart['point'];
                $this->order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
            }

            if($request->qty > 0){

                //$product = Products::find($request->id);
                $total = $request->qty * $product->price;

                $profit = $product->price - $product->avg_store_in;
                $point = $profit / .30;
                $product_point = $request->qty * $point;

                $this->items[$request->id] = [
                      'qty' =>   $request->qty
                    , 'point' =>  number_format((float)$product_point, 2, '.', '')
                    , 'price' => $product->price
                    , 'total' =>   number_format((float)$total, 2, '.', '')
                    , 'product_name' =>  $product->name
                    , 'store_in' =>  $product->total_store_in
                ];

            }else{
                if (array_key_exists($request->id, $this->items )){
                    unset( $this->items[$request->id] );
                }
            }

            $this->updateTotal();
            $cart = [
                'items' => $this->items,
                'point' => $this->point,
                'total' => $this->total,
                'qty' => $this->qty,
                'order_id' => $this->order_id
            ];
            $request->session()->put('dealer_cart', $cart);
            return response()->json([
                'status' => 'success',
                'cart'   => $cart
            ]);

       }else{
            return response()->json([
                'status' => 'OutOffStock',
                'message' => " {$product->product_id}  {$product->name} is Out Off Stock!",
            ]);
       }
    }
    public function addByCode(Request $request)
        {

           $product = Products::where('code', $request->code )->first();
           if(!empty($product)  ){

                if($request->session()->has('dealer_cart')){
                    $oldCart = $request->session()->get('dealer_cart');
                    $this->items = $oldCart['items'];
                    $this->total = $oldCart['total'];
                    $this->qty = $oldCart['qty'];
                    $this->point = $oldCart['point'];
                    $this->order_id = (isset($oldCart['order_id']) && $oldCart['order_id'] > 0)? $oldCart['order_id'] : 0;
                }

               $request_qty = 1;
               foreach ($this->items as $key => $item){
                   if($product->product_id == $key){
                       $request_qty = $item['qty'] + 1;
                   }
               }

               if($product->total_store_in >= $request_qty){


                   $total = $request_qty* $product->price;

                   $profit = $product->price - $product->avg_store_in;
                   $point = $profit / .30;
                   $product_point = $request->qty * $point;

                    $this->items[$product->product_id] = [
                          'qty' =>   $request_qty
                        , 'point' =>  number_format((float)$product_point, 2, '.', '')
                        , 'price' => $product->price
                        , 'total' =>   number_format((float)$total, 2, '.', '')
                        , 'product_name' =>  $product->name
                        , 'store_in' =>  $product->total_store_in
                    ];

                    $this->updateTotal();
                    $cart = [
                        'items' => $this->items,
                        'point' => $this->point,
                        'total' => $this->total,
                        'qty' => $this->qty,
                        'order_id' => $this->order_id
                    ];
                    $request->session()->put('dealer_cart', $cart);
                    return response()->json([
                        'status' => 'success',
                        'cart'   => $cart
                    ]);
               }else{
                   return response()->json([
                       'status' => 'OutOffStock',
                       'message' => " {$product->product_id}  {$product->name} is Out Off Stock!"
                   ]);
               }


           }else{
                return response()->json([
                    'status' => 'OutOffStock',
                    'message' => "Code: {$request->code}  Not Found!"
                ]);
           }
        }


    private function updateTotal(){
        $this->total = 0;
        $this->qty = 0;
        $this->point = 0;
        foreach ($this->items as $item){
            $this->point += $item['point'];
            $this->total += $item['total'];
            $this->qty += $item['qty'];
        }
    }


    public function read(Request $request){
        $this->setCart($request);

        if($this->order_id > 0){
            $order = Orders::SearchByShop()->find($this->order_id);
            return response()->json([
                'status' => 'success',
                'cart'   => [
                    'items' => $this->items,
                    'total' => $this->total,
                    'point' => $this->point,
                    'qty' => $this->qty,
                    'order_id' => $this->order_id,

                    'order' =>$order,
                    'customer' =>$order->member
                ]
            ]);
        }else{
            return response()->json([
                'status' => 'success',
                'cart'   => [
                    'items' => $this->items,
                    'total' => $this->total,
                    'point' => $this->point,
                    'qty' => $this->qty,
                    'order_id' => 0
                ]
            ]);
        }
    }


    public function complete(Request $request){
            $this->setCart($request);
            if($this->total > 0){
                
                $login_user = Auth::user();

                if ($this->order_id> 0 ){

                    $order = Orders::find($this->order_id);
                    $order_due = $this->total - $request->paid;
                    $member_balance = $order->previous_due + $order_due;

                    $order->update([
                        'order_total' => $this->total,
                        'order_point' => $this->point,
                        'order_qty' => $this->qty,
                        'paid' => $request->paid,
                        'due' => $order_due,
                        'member_balance' => $member_balance
                    ]);
                    
                    $transaction = Transaction::SearchByShop()->where( 'ref_id', $order->order_id )->first();
                    if(!empty($transaction)){
                        $cashIn = Orders::SearchByShop()->sum('paid');
                        $cashOut = Payment::SearchByShop()->sum('amount');

                        $transaction->update([
                            'amount' => $request->paid,
                            'description' => 'Sales total '. $this->total,
                            'balance' => $cashIn - $cashOut,
                        ]);

                    }

                    foreach ($order->products as $sale){
                        DB::table('products')
                            ->where('product_id', $sale->product_id)
                            ->increment('total_store_in', $sale->qty);
                    }
                    OrdersProducts::where('order_id', $this->order_id)->delete();

                }else{
                    $member_id = 0;
                    $previous_due = 0;
                    if(!empty($request->customer)){
                        $member = Member::where('username', $request->customer)->first();

                        if(!empty($member)){
                            $member_id = $member->member_id;
                            $previous_due = Orders::SearchByShop()->where('member_id', $member_id)->sum('due');
                        }else{
                            $member = Member::create([
                                'shop_id' => $login_user->id,
                                'username' => $request->customer,
                                'name' => $request->customer_name,
                                'address' => $request->customer_address,
                            ]);
                            $member_id = $member->member_id;
                        }
                    }
                    $order_due = $this->total - $request->paid;
                    $member_balance = $previous_due + $order_due;

                    $order = Orders::create([
                            'shop_id' => $login_user->shop_id,
                            'user_id'  => $login_user->id,
                            'order_total' => $this->total,
                            'order_point' => $this->point,
                            'order_qty' => $this->qty,
                            'order_date' => date('Y-m-d'),
                            'order_discount' => 0,
                            'order_profit' => 0,

                            'member_id' => $member_id,
                            'paid' => $request->paid,
                            'due' => $order_due,
                            'member_balance' => $member_balance,
                            'previous_due' => $previous_due,
                        ]);


                    $cashIn = Orders::SearchByShop()->sum('paid');
                    $cashOut = Payment::SearchByShop()->sum('amount');

                    Transaction::create([
                        'shop_id' => $login_user->shop_id,
                        'ref_id' => $order->order_id,
                        'amount' => $request->paid,
                        'type' => 'Order',
                        'description' => 'Sales total '. $this->total,
                        'balance' => $cashIn - $cashOut,
                    ]);

                }
                if(!empty($order)){

                    $orderProducts = [];

                    $discount = 0;
                    $profit = 0;

                    foreach ($this->items as $product_id => $item){

                        $orderProductInfo = Products::find($product_id);
                        array_push($orderProducts, [
                            'shop_id' => $login_user->shop_id,
                            'order_id' => $order->order_id,
                            'product_id' => $product_id,
                            'qty' => $item['qty'],
                            'point' => $item['point'],
                            'price' => $item['price'],
                            'total' => $item['total'],

                            'avg_store_in' => $orderProductInfo->avg_store_in,
                            'mrp' => $orderProductInfo->mrp,
                        ]);

                        $mrp_price =  $orderProductInfo->mrp * $item['qty'];
                        $discount +=  $mrp_price - $item['total'];

                        $profit_price =  $orderProductInfo->avg_store_in * $item['qty'];
                        $profit +=  $item['total'] - $profit_price;

                        DB::table('products')
                            ->where('product_id', $product_id)
                            ->decrement('total_store_in', $item['qty']);
                    }
                    if(!empty($orderProducts)){
                        OrdersProducts::insert($orderProducts);

                        $order->update([
                            'order_discount' => $discount,
                            'order_profit' => $profit,
                        ]);
                    }

                    $request->session()->forget('dealer_cart');
                    return response()->json([
                        'status' => 'success',
                        'order_url'   => route('order-view', $order->order_id )
                    ]);

                }
            }else {
            return response()->json([
                'status' => 'validation',
                'message' => 'Invalid'
            ]);
        }
    }


    public function order_edit($id, Request $request){
        $request->session()->forget('dealer_cart');
        $order = Orders::SearchByShop()->find($id);
        if($order){
            $products = OrdersProducts::with('product')->where('order_id', $id)->get();

            foreach ($products as $product){

                $this->items[$product->product_id] = [
                     'qty' =>   $product->qty
                    , 'point' => $product->point
                    , 'price' => $product->price
                    , 'total' =>   $product->total
                    , 'product_name' =>  $product->name
                ];
            }


                $this->total = $order->order_total;
                $this->qty = $order->order_qty;
                $this->point = $order->order_point;
            $this->updateTotal();
            $cart = [
                'items' => $this->items,
                'point' => $this->point,
                'total' => $this->total,
                'qty' => $this->qty,
                'order_id' => $id
            ];
            $request->session()->put('dealer_cart', $cart);
        }
        return redirect()->route('sales');
    }

    public function empty_cart(Request $request){
        if($request->session()->has('dealer_cart')){
            $request->session()->forget('dealer_cart');
        }
        return redirect()->route('sales');
    }

    public function get_customer(Request $request){
        $member = Member::where('username', $request->id)->first();
        if(!empty($member)){
            $this->setCart($request);
            $due = Orders::SearchByShop()->where('member_id', $member->member_id)->sum('due');

            return response()->json([
                'status' => 'success',
                'name' => $member->name,
                'due' => $due,
                'payable' => $this->total + $due,
                'message' =>1,
            ]);

        }else{
            return response()->json([
                'status' => 'success',
                'message' => 2,
            ]);
        }
    }

    public function get_order_data(Request $request){
        $oldCart = $request->session()->get('dealer_cart');
        return response()->json([
            'status' => 'success',
            'order'   => Orders::find($oldCart['order_id'])
        ]);

    }

}
