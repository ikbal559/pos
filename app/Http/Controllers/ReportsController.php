<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Orders\Orders;
use Products\Damage;
use Carbon\Carbon;
use DB;
use Products\ManageProducts;

class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function sales()
    {
        return view('Reports.sales');
    }

    public function store()
    {
        return view('Reports.store');
    }
}
