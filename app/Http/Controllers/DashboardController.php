<?php

namespace App\Http\Controllers;

use Products\ManageProducts;
use Users\User;
use Carbon\Carbon;
use Orders\Orders;
use Auth;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    private function admin(){

        $now = Carbon::now();
        $startDay = $now->startOfMonth()->format('Y-m-d h:m:i');
        $endDay = $now->endOfMonth()->format('Y-m-d h:m:i');
        $ToDay = Carbon::now()->format('Y-m-d');
//
//        $manageProduct = ManageProducts::whereBetween('created_at', [$startDay, $endDay])
//            ->select(DB::raw('sum(manage_products_quantity) as total, manage_products_type'))
//            ->groupBy('manage_products_type')->get();

        $import_month = 0;
        $export_month = 0;
        $damage_month = 0;

//        foreach ($manageProduct as $item){
//            if($item->manage_products_type == 'import'){
//                $import_month = $item->total;
//            }
//            if($item->manage_products_type == 'export'){
//                $export_month = $item->total;
//            }
//            if($item->manage_products_type == 'damage'){
//                $damage_month = $item->total;
//            }
//        }

        $users = User::where('user_type', 'shop')->count();
        $sales_month = Orders::whereBetween('created_at', [$startDay, $endDay])->sum('order_total');
        $sales_today = Orders::whereDate('order_date', $ToDay)->sum('order_total');


        $weekStartDay = Carbon::now()->subDays(6);
        $sales_week = Orders::whereBetween('order_date', [$weekStartDay->format('Y-m-d'), $ToDay])
            ->select(DB::raw('sum(order_total) as total, order_date'))
            ->groupBy('order_date')->get();

        $day = $weekStartDay;
        $WeekSaleReport = [];
        for ($i = 0; $i < 7; $i++) {

            array_push($WeekSaleReport, [
                'day' => $day->format('Y-m-d'),
                'display' => $day->format('jS M'),
                'total' => 0,
            ]);
            $day = $day->addDays(1);

        }

        foreach ($sales_week as $sale) {
            foreach ($WeekSaleReport as $key => $week) {
                if ($week['day'] == $sale->order_date) {
                    $WeekSaleReport[$key]['total'] = $sale->total;
                }
            }
        }

        $WeekSaleReportDay = '';
        $WeekSaleReportTotal = "";

        foreach ($WeekSaleReport as $item) {

            $WeekSaleReportTotal .= $item['total'] . ',';
            $WeekSaleReportDay .= "'{$item['display']}', ";

        }
        return [
            'shops' => $users,
            'sales_month' => $sales_month,
            'sales_today' => $sales_today,
            'WeekSaleReportDay' => $WeekSaleReportDay,
            'WeekSaleReportTotal' => $WeekSaleReportTotal,
            'access_admin' => $this->hasAdminPermission()
        ];

    }

    private function shop(){
        
        $ToDay = Carbon::now()->format('Y-m-d');





        $weekStartDay = Carbon::now()->subDays(6);
        $sales_week = Orders::SearchByShop()->whereBetween('order_date', [$weekStartDay->format('Y-m-d'), $ToDay])
            ->select(DB::raw('sum(order_total) as total, order_date'))
            ->groupBy('order_date')->get();

        $day = $weekStartDay;
        $WeekSaleReport = [];
        for ($i = 0; $i < 7; $i++) {

            array_push($WeekSaleReport, [
                'day' => $day->format('Y-m-d'),
                'display' => $day->format('jS M'),
                'total' => 0,
            ]);
            $day = $day->addDays(1);

        }

        foreach ($sales_week as $sale) {
            foreach ($WeekSaleReport as $key => $week) {
                if ($week['day'] == $sale->order_date) {
                    $WeekSaleReport[$key]['total'] = $sale->total;
                }
            }
        }

        $WeekSaleReportDay = '';
        $WeekSaleReportTotal = "";

        foreach ($WeekSaleReport as $item) {

            $WeekSaleReportTotal .= $item['total'] . ',';
            $WeekSaleReportDay .= "'{$item['display']}', ";

        }
        return [
 

            'WeekSaleReportDay' => $WeekSaleReportDay,
            'WeekSaleReportTotal' => $WeekSaleReportTotal,
        ];

    }
    private function network(){
        return [
          'user' => User::find(Auth::user()->id)
        ];

    }

    public function index()
    {
 
        if(Auth::user()->user_type == 'admin'){
            return view('Dashboard.admin', $this->admin());
        }
        if(Auth::user()->user_type == 'shop'){
            return view('Dashboard.shop', $this->shop());
        }

        if(Auth::user()->user_type == 'customer'){
            return redirect()->route('home');
        }
        if(Auth::user()->user_type == 'normal'){
            return redirect()->route('home');
        }

    }
}
