<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Users\Member;


class HourlyMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'HourlyMemberReceive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'HourlyMemberReceive ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $url = "http://ombazar.com/api/index?key=Tsi4y9lRvfuaAfMfujZFMAsbAFYOEcHw0O5UWcpDYB8NrxPfx5HRN1IHAEFn";


//  Initiate curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result, true);
        if(!empty($result)){
            $users = [];
            foreach ($result as $user){
                if(!empty($user) && !empty($user['User']['wsusername']) && !empty($user['User']['id'])   && strlen($user['User']['wsusername']) > 10  ){

                    $mem = Member::where('username', $user['User']['wsusername'])->first();
                    if(empty($mem)){
                        array_push($users, [
                            'username' => $user['User']['wsusername'],
                            'name' => $user['User']['name'],
                            'address' => $user['User']['address'],
                            'om_user_id' => $user['User']['id'],
                        ]);
                    }else {
                        $mem->update([
                            'name' => $user['User']['name'],
                            'address' => $user['User']['address']
                        ]);

                    }
                }
            }
            if(!empty($users)){
                Member::insert($users);
            }
        }

        $this->info('All HourlyMemberReceive!');

    }
}
