<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->increments('order_id');
            $table->integer('user_id');
            $table->decimal('order_total');
            $table->integer('order_qty');
            $table->decimal('order_service_fee')->default(0.00);
            $table->decimal('order_discount')->default(0.00);
            $table->date('order_date');

            $table->string('order_note')->nullable();
            $table->string('delivery_name')->nullable();
            $table->string('delivery_invoice')->nullable();


            $table->integer('order_status')->default(1);
            
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
