<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('product_id');

            $table->integer('shop_id');
            $table->integer('product_category_id');
            $table->integer('product_category_main_id');
            $table->string('product_name', 200);
            $table->string('product_images', 80)->nullable();

            $table->decimal('product_mrp');
            $table->decimal('product_sales');
            $table->decimal('product_price');

            $table->integer('minimum_order');
            $table->integer('stock_balance');
            $table->integer('total_sales')->default(0);

            $table->text('description');
            $table->text('terms_and_conditions');
            $table->string('product_variation', 80)->nullable();
            $table->string('product_weight', 30)->nullable();
            $table->integer('product_status')->default(0);
            $table->integer('home_status')->default(0);

            $table->date('ordering')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
