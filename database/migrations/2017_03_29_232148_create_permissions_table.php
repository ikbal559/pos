<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
     public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('permission_id');
            $table->integer('permission_group_id');
            $table->integer('user_id');
            $table->integer('status');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('permissions');
    }
    
}