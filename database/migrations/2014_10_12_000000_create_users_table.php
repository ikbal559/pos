<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->string('username', 20);
            $table->string('user_email')->nullable();
            $table->string('name');
            $table->string('phone', 20)->nullable();
            $table->string('password');
            $table->enum('user_type', [ 'admin', 'manager', 'shop', 'customer', 'normal'] )->default('normal');
            $table->string('user_images', 60)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
