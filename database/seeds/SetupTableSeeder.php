<?php

    /**
     * Prometheus
     *
     * Seeds the initial user table
     *
     * LICENSE:
     *
     * @package     Prometheus
     * @subpackage  DB Seeds
     * @author      Nick Woodhead <naw103@gmail.com>
     * @copyright   Copyright (c) 2016
     * @license
     * @version     1.0
     * @link        https://www.Prometheus.com
     */
    use Illuminate\Database\Seeder;



    class SetupTableSeeder extends Seeder
    {


        public function run()
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('business_types')->truncate();
            DB::table('category')->truncate();
            DB::table('locations')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');


            \Products\BusinessTypes::insert(
            [
                [
                    'name' => 'Shop'
                ], [
                    'name' => 'Dealer'
                ], [
                    'name' => 'Wholesaler'
                ], [
                    'name' => 'Flying'
                ],[
                    'name' => 'Suppliers'
                ],[
                    'name' => 'Pharmacy'
                ],[
                    'name' => 'Others'
                ]
             ]
            );
            \Products\Category::insert(
            [
                [
                    'category_name' => 'Stationary'
                ], [
                    'category_name' => 'Electric & Electronics'
                ], [
                    'category_name' => 'Clothing'
                ], [
                    'category_name' => 'Hardware'
                ],[
                    'category_name' => 'Sports'
                ],[
                    'category_name' => 'Herbal'
                ],[
                    'category_name' => 'Cosmatics'
                ],[
                    'category_name' => 'Mobile Accessories'
                ],[
                    'category_name' => 'Garment Accessories'
                ],[
                    'category_name' => 'Food & Beverage'
                ],[
                    'category_name' => 'Medicin'
                ],[
                    'category_name' => 'Others'
                ]
            ]
            );

            \Users\Locations::insert(
                [
                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Barisal',
                ],                 [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Dhaka',
                ],                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Chittagong',
                ],                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Khulna',
                ],                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Mymensingh',
                ],                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Rajshahi',
                ],                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Rangpur',
                ],
                [
                    'division_id' => 0,
                    'district_id' => 0,
                    'name' => 'Sylhet',
                ]

                ,[
                    'division_id' => 1,
                    'district_id' => 0,
                    'name' => 'Barguna',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 0,
                    'name' => 'Bhola',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 0,
                    'name' => 'Jhalokati',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 0,
                    'name' => 'Patuakhali',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 0,
                    'name' => 'Pirojpur',
                ],
                [
                    'division_id' => 2,
                    'district_id' => 0,
                    'name' => 'Dhaka',
                ],
                [
                    'division_id' => 2,
                    'district_id' => 0,
                    'name' => 'Gazipur',
                ],
                [
                    'division_id' => 2,
                    'district_id' => 0,
                    'name' => 'Tangail',
                ]


                ,[
                    'division_id' => 1,
                    'district_id' => 9,
                    'name' => 'Muladi',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 9,
                    'name' => 'Babuganj',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 9,
                    'name' => 'Agailjhara',
                ],
                [
                    'division_id' => 1,
                    'district_id' => 9,
                    'name' => 'Barisal Sadar',
                ]

                ]
            );


        }
    }