<?php

    /**
     * Prometheus
     *
     * Seeds the initial user table
     *
     * LICENSE:
     *
     * @package     Prometheus
     * @subpackage  DB Seeds
     * @author      Nick Woodhead <naw103@gmail.com>
     * @copyright   Copyright (c) 2016
     * @license
     * @version     1.0
     * @link        https://www.Prometheus.com
     */
    use Illuminate\Database\Seeder;
    use Users\User;


    class UsersTableSeeder extends Seeder
    {


        public function run()
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('users')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');


			User::create([

				'password'   => bcrypt('admin'),

				'shop_id' => 0,
				'name' => 'Admin',
				'username' => 'admin',
				'user_type' => 'admin'

			]);


        }
    }    
