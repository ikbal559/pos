<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {

    Route::group(['prefix' => 'category'], function () {
        Route::post('storeCategory',  'ProductController@storeCategory')->name('api.product.storeCategory');
        Route::post('updateCategory', 'ProductController@updateCategory')->name('api.product.updateCategory');
    });
    Route::group(['prefix' => 'location'], function () {
        Route::post('manage',  'ProductController@manageLocation')->name('api.manage.location');
        Route::post('get',  'ProductController@get_locations')->name('api.get.location');
    });

    Route::group(['prefix' => 'business'], function () {
        Route::post('storeBusinessType', 'ProductController@storeBusinessType')->name('api.product.storeBusinessType');
        Route::post('updateBusinessType', 'ProductController@updateBusinessType')->name('api.product.updateBusinessType');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::post('store_in_save', 'ProductController@store_in_save')->name('api.store.in.save');
        Route::post('store_in_update', 'ProductController@store_in_update')->name('api.store.in.update');
        
        Route::post('store', 'ProductController@store')->name('api.product.store');
        Route::post('update', 'ProductController@edit')->name('api.product.update');
        Route::post('entry', 'ProductController@entry')->name('api.product.entry');
        Route::post('entry_update', 'ProductController@entry_update')->name('api.product.entry_update');
        Route::post('damage', 'ProductController@damage')->name('api.product.damage');
        Route::post('damage_update', 'ProductController@damage_update')->name('api.product.damage_update');
    });

    Route::group(['prefix' => 'shops'], function () {
        Route::post('add', 'ShopsController@store')->name('api.shop.add');
        Route::post('update', 'ShopsController@edit')->name('api.shop.update');
        Route::post('add-payment', 'ShopsController@add_payment')->name('api.shop.add-payment');
    });

    Route::group(['prefix' => 'clients'], function () {
        Route::post('add', 'ClientsController@store')->name('api.client.add');
        Route::post('update', 'ClientsController@edit')->name('api.client.update');

        Route::post('add-payment', 'ClientsController@add_payment')->name('api.client.add_payment');
    });

    Route::group(['prefix' => 'photo'], function () {
        Route::post('add', 'ImagesController@profileImage')->name('api.add.image');
        Route::post('remove', 'ImagesController@profileImageRemove')->name('api.remove.image');
    });


    Route::group(['prefix' => 'user'], function () {
        Route::post('detail', 'NetworkController@get_detail')->name('api.user.detail');
        Route::post('user-registration', 'NetworkController@registration_user')->name('api.network.registration-frontend');
    });

    Route::group(['prefix' => 'configuration'], function () {
        Route::post('api-up_line_bill-manage', 'ConfigurationController@up_line_bill_manage')->name('api.up_line_bill.manage');
        Route::post('configuration-edit', 'ConfigurationController@configuration_edit')->name('api.configuration.edit');
        Route::post('personal', 'ConfigurationController@personal')->name('api.configuration.add.personal');
        Route::post('network', 'ConfigurationController@network')->name('api.configuration.add.network');
    });

    Route::group(['prefix' => 'network'], function () {
        Route::post('api-balance-transfer', 'NetworkController@balance_transfer')->name('api.balance.transfer');
        Route::post('api-balance-transfer-shop', 'NetworkController@balance_transfer_shop')->name('api.balance.transfer.shop');
        Route::post('registration', 'NetworkController@registration')->name('api.network.registration');

        Route::post('update-profile', 'NetworkController@update_profile')->name('api.update.profile');
        Route::post('update-profile-detail', 'NetworkController@update_profile_detail')->name('api.update.profile.detail');
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::post('order-bill-process', 'AdminController@order_bill_process')->name('api.order-bill-process');
        Route::post('bill-process-pac',   'AdminController@bill_process_pac')->name('api.bill-process-pac');
        Route::post('add-manager',   'AdminController@add_manager')->name('api.admin.add.manager');
        Route::post('manage-manager',   'AdminController@manage_manager')->name('api.admin.manage.manager');
        Route::post('api-admin-account-add-payment',   'AdminController@api_admin_account_add_payment')->name('api-admin-account-add-payment');

        Route::post('order-updater', 'AdminController@order_update')->name('order-update-admin');


        Route::post('admin-send-sms', 'AdminController@admin_send_sms')->name('admin-send-sms');
        Route::post('admin-send-email', 'AdminController@admin_send_email')->name('admin-send-email');

        Route::post('admin-send-order-smsEmail', 'AdminController@admin_send_order_smsEmail')->name('admin-send-order-smsEmail');
    });


});