<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index' )->name('home');
Route::get('/home', 'HomeController@index' );
Route::get('/search', 'HomeController@search' );

Route::get('/cash-report', 'AdminController@cash_report' )->name('cash-report');

Route::get('/bank-info', 'HomeController@bank_info')->name('bank-info');
Route::get('/contact-us', 'HomeController@contact_us')->name('contact-us');

Route::group(['prefix' => 'users'], function () {
    Route::get('/registration', 'UsersController@registration')->name('users-registration');
    Route::get('/clients', 'UsersController@clients')->name('clients');
});

Route::group(['prefix' => 'dashboard'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard-index');
});


Route::group(['prefix' => 'products'], function () {

    Route::get('/', 'ProductsController@index')->name('product-list');
    Route::get('/add', 'ProductsController@store')->name('product-add');

    Route::get('/sales', 'SalesController@sales')->name('product-sales');

    Route::get('/collective-sales', 'ProductsController@collective_sales')->name('collective-sales');

    Route::get('/store-in', 'ProductsController@store_in')->name('store-in');
    Route::get('/{id}/store-in-edit', 'ProductsController@store_in_edit')->name('store-in-edit');
    Route::get('/store-in-list', 'ProductsController@store_in_list')->name('store-in-list');


    Route::get('/store', 'ProductsController@manage')->name('manage-products');
    Route::get('/manage-store', 'ProductsController@manage_products')->name('manage-products-list');
    Route::get('/{id}/edit', 'ProductsController@update')->name('product-edit');
    Route::get('/{id}/view', 'ProductsController@view')->name('product-view');

    Route::get('/{id}/store-view', 'ProductsController@manage_view')->name('manage-products-view');
    Route::get('/{id}/edit-collective-sales', 'ProductsController@edit_collective_sales')->name('order-edit-collective-sales');

    Route::post('get-product-info', 'ProductsController@get_product_info')->name('get-product-info');
});

Route::group(['prefix' => 'shops'], function () {

    Route::get('/', 'ShopsController@index')->name('shops');

//    Route::get('/payment-admin', 'ShopsController@shop_payments_admin')->name('shop-payments-admin');
//    Route::get('/payments', 'ShopsController@shop_payments')->name('shops-payment-list');
//    Route::get('/{id}/payment-add', 'ShopsController@shop_add_payment')->name('shops-payment-add');

    Route::get('/add', 'ShopsController@store')->name('shops-add');
    Route::get('/{id}/edit', 'ShopsController@update')->name('shops-edit');

});

Route::group(['prefix' => 'clients'], function () {

    Route::get('/', 'ClientsController@index')->name('clients');
    Route::get('/add', 'ClientsController@store')->name('clients-add');
    Route::get('/{id}/edit', 'ClientsController@update')->name('clients-edit');
    Route::get('/{id}/view', 'ClientsController@show')->name('clients-view');

    Route::get('/{id}/payment', 'ClientsController@payment')->name('clients-payment');
    Route::get('/{id}/payment-edit/', 'ClientsController@payment_edit')->name('clients-payment-edit');
    Route::get('/payments', 'ClientsController@payments_list')->name('clients-payments');
    Route::get('/payments-report', 'ClientsController@payments_report')->name('payments-report');
});



Route::group(['prefix' => 'cart'], function () {
    Route::get('empty-cart', 'CartController@empty_cart')->name('api.empty.cart');
    Route::post('get-customer', 'CartController@get_customer')->name('get-customer');
    Route::post('add', 'CartController@add')->name('api.add.to.cart');
    Route::post('add-by-code', 'CartController@addByCode')->name('api.add.to.cartCode');
    Route::post('read', 'CartController@read')->name('api.read.cart');
    Route::post('complete', 'CartController@complete')->name('api.complete.cart');
    Route::post('complete-collective-sales', 'CartController@complete_collective_sales')->name('api.complete.collective.sales');
    Route::get('/{id}/edit', 'CartController@order_edit')->name('order-edit');
    Route::post('/get-order-data', 'CartController@get_order_data')->name('get-order-data');
    Route::post('/cart-read', 'CartController@read')->name('api.read.cart');
});

Route::group(['prefix' => 'dealer'], function () {
    Route::get('empty-cart', 'CartDealerController@empty_cart')->name('api.empty.cart.dealer');
    Route::post('get-customer', 'CartDealerController@get_customer')->name('get-customer.dealer');
    Route::post('add', 'CartDealerController@add')->name('api.add.to.cart.dealer');
    Route::post('add-by-code', 'CartDealerController@addByCode')->name('api.add.to.cartCode.dealer');
    Route::post('read', 'CartDealerController@read')->name('api.read.cart.dealer');
    Route::post('complete', 'CartDealerController@complete')->name('api.complete.cart.dealer');
    Route::post('complete-collective-sales', 'CartDealerController@complete_collective_sales')->name('api.complete.collective.sales.dealer');
    Route::get('/{id}/edit', 'CartDealerController@order_edit')->name('order-edit.dealer');
    Route::post('/get-order-data', 'CartDealerController@get_order_data')->name('get-order-data.dealer');
    Route::post('/cart-read', 'CartDealerController@read')->name('api.read.cart.dealer');

});

Route::group(['prefix' => 'manage'], function () {
    Route::post('add',      'ManageController@add')->name('api.add.to.manage');
    Route::post('read',     'ManageController@read')->name('api.read.manage');
    Route::post('complete', 'ManageController@complete')->name('api.complete.manage');
    Route::get('empty',     'ManageController@empty_cart')->name('api.empty.manage');
    Route::get('/{id}/edit','ManageController@manage_edit')->name('manage-edit');
});

Route::group(['prefix' => 'orders'], function () {
    Route::post('/new-cart', 'HomeController@new_order')->name('order-new');
    Route::post('/update-cart', 'HomeController@update_order')->name('order-update');


    Route::get('/view_orders', 'SalesController@view_orders')->name('order-list');
    Route::get('/{id}/view', 'SalesController@view_order')->name('order-view');
    
    Route::get('/save', 'HomeController@save_order')->name('submit-order');
    Route::get('/cart', 'HomeController@cart')->name('order-cart');

    Route::get('/send-points', 'SalesController@send_points')->name('send-points');
    Route::get('/send-points-process', 'SalesController@send_points_process')->name('send-points-process');
    Route::get('/{id}/send-points-retry', 'SalesController@send_points_retry')->name('send-points-retry');
});


Route::group(['prefix' => 'reports'], function () {
    Route::get('/store', 'ReportsController@store')->name('reports-store');
    Route::get('/sales', 'ReportsController@sales')->name('reports-sales');
});



Route::group(['prefix' => 'admin'], function () {
    Route::get('/marketing', 'AdminController@marketing')->name('admin-marketing');
    Route::get('/history-sms', 'AdminController@admin_history_sms')->name('admin-history-sms');
    Route::get('/history-email', 'AdminController@admin_history_email')->name('admin-history-email');

    Route::get('/account', 'AdminController@account_list')->name('admin-account-list');
    Route::get('/user-update', 'AdminController@user_update')->name('admin-user-update');
    Route::get('/admin-user-view-single', 'AdminController@user_view_single')->name('admin-user-view-single');
    Route::get('/sales', 'AdminController@orders')->name('orders-list');

    Route::get('/{id}/order-edit', 'AdminController@order_edit')->name('order-edit-admin');
    Route::get('/{id}/order-view', 'AdminController@order_view')->name('order-view-admin');

});

Route::group(['prefix' => 'sales'], function () {
    Route::get('/', 'SalesController@sales')->name('sales');
    Route::get('/dealer', 'SalesController@dealer_sales')->name('dealer-sales');
});

Route::group(['prefix' => 'members'], function () {
    Route::get('/', 'MembersController@index')->name('members');
});

Route::group(['prefix' => 'managers'], function () {
    Route::get('/', 'ManagersController@index')->name('manager-index');
    Route::get('/add', 'ManagersController@add')->name('admin.add.manager');
    Route::get('/{id}/manage', 'ManagersController@manage')->name('admin.manage.manager');
});

Route::get('/load/{namespace}/{model}', 'DataTableController@load')->name('db-load');


