$(document).ready(function(e) {
    $('.form-vertical-registration-frontend')
        .formValidation(

            {
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    'users[username]': {
                        validators: {
                            notEmpty: {
                                message: 'The username is required'
                            },
                            stringLength: {
                                min: 11,
                                max: 11,
                                message: 'username must be a 11 Digit phone number (example: 01800XXXXXX )',
                                number: true
                            },
                            regexp: {
                                regexp: /^[0-9_]+$/,
                                message: 'The username can only consist of alphabetical, number and underscore'
                            }
                        }
                    },
                    'users[password]': {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 6,
                                max: 20,
                                message: 'The Password must be more than 6 and less than 20 characters long'
                            }
                        }
                    },
                    password_confirmation : {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 6,
                                max: 20,
                                message: 'The username must be more than 6 and less than 20 characters long'
                            },
                            identical: {
                                field: 'users[password]',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }

                }
            }
        )
        .on('success.form.fv', function(e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({

                type: "POST",
                cache: false,
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function(){

                    form.find('#please-wait').removeClass('hidden');
                    form.find('#success').addClass('hidden');
                    form.find('#error').addClass('hidden');

                },
                success: function(json){
                    form.find('#please-wait').addClass('hidden');
                    if(json.status == 'success'){
                        var success = form.find('#success');
                        success.find('.message').text(json.message);
                        success.removeClass('hidden');
                    } else if(json.status == 'false') {
                        var error = form.find('#error');
                        error.find('.message').text(json.message);
                        error.removeClass('hidden');

                    } else if(json.status == 'validation'){
                        var error = form.find('#error');
                        error.removeClass('hidden');
                        var fv    = form.data('formValidation');
                        for (var field in json.message) {

                            if(json.message[field][0] = 'The users.username has already been taken.'){
                                error.find('.message').text('The Username has already been taken.');
                                fv.updateMessage('users[username]', 'blank', 'The Username has already been taken.').updateStatus('users[username]', 'INVALID', 'blank');
                            }

                        }
                    }
                },

                error : function(){
                    var error = form.find('#error');
                    error.find('.message').text('Internal Server Error');
                    error.removeClass('hidden');
                },
                dataType: "json"

            });
        });

    $('.form-vertical-update')
        .formValidation(
            {
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {

                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 6,
                                max: 20,
                                message: 'The Password must be more than 6 and less than 20 characters long'
                            }
                        }
                    },
                    password_confirmation : {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 6,
                                max: 20,
                                message: 'The username must be more than 6 and less than 20 characters long'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }

                }
            }
        )
        .on('success.form.fv', function(e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({

                type: "POST",
                cache: false,
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function(){

                    form.find('#please-wait').removeClass('hidden');
                    form.find('#success').addClass('hidden');
                    form.find('#error').addClass('hidden');

                },
                success: function(json){
                    form.find('#please-wait').addClass('hidden');
                    if(json.status == 'success'){

                        var success = form.find('#success');
                        success.find('.message').text(json.message);
                        success.removeClass('hidden');
                        setTimeout(function(){location.reload();}, 3000);

                    } else if(json.status == 'false') {

                        var error = form.find('#error');
                        error.find('.message').text(json.message);
                        error.removeClass('hidden');

                    } else if(json.status == 'validation'){

                        var fv = form.data('formValidation');
                        var error = form.find('#error');
                        for (var field in json.message) {
                            error.find('.message').text('Please Correct the Marked Errors');
                            error.removeClass('hidden');
                        }
                    }
                },

                error : function(){
                    var error = form.find('#error');
                    error.find('.message').text('Internal Server Error');
                    error.removeClass('hidden');
                },
                dataType: "json"

            });
        });


 });

