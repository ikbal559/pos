
$('.form-vertical')
    .formValidation()
    .on('success.form.fv', function(e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({

            type: "POST",
            cache: false,
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function(){

                form.find('#please-wait').removeClass('hidden');
                form.find('#success').addClass('hidden');
                form.find('#error').addClass('hidden');

            },

            success: function(json){
                form.find('#please-wait').addClass('hidden');
                if(json.status == 'success'){
                    var success = form.find('#success');
                    success.find('.message').text(json.message);
                    success.removeClass('hidden');
                    setTimeout(function(){location.reload();}, 3000);
                } else if(json.status == 'false') {
                    var error = form.find('#error');
                    error.find('.message').text(json.message);
                    error.removeClass('hidden');

                } else if(json.status == 'validation'){

                    //Getting Form Validation Instance
                    var fv = form.data('formValidation');
                    var error = form.find('#error');

                    for (var field in json.message) {
                        error.find('.message').text('Please Correct the Marked Errors');
                        error.removeClass('hidden');
                    
                    }
                }
            },

            error : function(){
                var error = form.find('#error');
                error.find('.message').text('Internal Server Error');
                error.removeClass('hidden');
            },
            dataType: "json"

        });
    });


$('.form-vertical_cart')
    .formValidation()
    .on('success.form.fv', function(e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({

            type: "POST",
            cache: false,
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function(){

                form.find('#please-wait').removeClass('hidden');
                form.find('#success').addClass('hidden');
                form.find('#error').addClass('hidden');

            },

            success: function(json){
                form.find('#please-wait').addClass('hidden');
                if(json.status == 'success'){
                    var success = form.find('#success');
                    success.find('.message').text(json.message);
                    success.removeClass('hidden');
                    setTimeout(function(){  window.location.href = '/orders/cart' }, 3000);
                } else if(json.status == 'false') {
                    var error = form.find('#error');
                    error.find('.message').text(json.message);
                    error.removeClass('hidden');

                } else if(json.status == 'validation'){

                    //Getting Form Validation Instance
                    var fv = form.data('formValidation');
                    var error = form.find('#error');

                    for (var field in json.message) {
                        error.find('.message').text('Please Correct the Marked Errors');
                        error.removeClass('hidden');

                    }
                }
            },

            error : function(){
                var error = form.find('#error');
                error.find('.message').text('Internal Server Error');
                error.removeClass('hidden');
            },
            dataType: "json"

        });
    });
