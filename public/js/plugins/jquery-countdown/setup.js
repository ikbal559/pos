$(document).ready(function(){
    $('.js-countdown').countdown( countdownTime , function(event) {
        $(this).html(event.strftime('<div class="row items-push text-center">'
            + '<div class="col-xs-3"><div class="font-s12">%-D</div><div class="font-s12 ">DAYS</div></div>'
            + '<div class="col-xs-3"><div class="font-s12">%H</div><div class="font-s12 ">HOURS</div></div>'
            + '<div class="col-xs-3"><div class="font-s12">%M</div><div class="font-s12 ">MINUTES</div></div>'
            + '<div class="col-xs-3"><div class="font-s12">%S</div><div class="font-s12 ">SECONDS</div></div>'
            + '</div>'
        ));
    });
});