/*
 *  Document   : base_comp_charts.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Charts Page
 */

var BaseCompCharts = function() {
    // Chart.js Charts, for more examples you can check out http://www.chartjs.org/docs
    var initChartsChartJS = function () {
        // Get Chart Containers

         var $chartBarsCon   = jQuery('.js-chartjs-bars')[0].getContext('2d');

        // Set global chart options
        var $globalOptions = {
            scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            scaleFontColor: '#999',
            scaleFontStyle: '600',
            tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            tooltipCornerRadius: 3,
            maintainAspectRatio: false,
            responsive: true
        };

        // Lines/Bar/Radar Chart Data
        var $chartLinesBarsRadarData = {
            labels:  window.salesLastWeekDay,
            datasets: [
                {
                    label: 'Last Week',
                    fillColor: '#e6f7d8',
                    strokeColor: '#e6f7d8',
                    pointColor: '#e6f7d8',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: '#e6f7d8',
                    data:  window.salesLastWeekTotal
                }
            ]
        };


        // Init Charts
         $chartBars  = new Chart($chartBarsCon).Bar($chartLinesBarsRadarData, $globalOptions);
    };

    return {
        init: function () {
            // Init all charts
            initChartsChartJS();

        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseCompCharts.init(); });