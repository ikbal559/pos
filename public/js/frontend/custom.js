$(document).ready(function(){
//    var swtichToDenger = function (target){
//        $(target).closest('.form-material').addClass()
//    }
//    
   $('.checkLoginWS').click(function (){
       var user = $('#frontend-login-username').val();
       var pass = $('#frontend-login-password').val();
       
       $('.ShowLoginErrow').html('<div id="name-error" class="help-block text-left animated zoomIn">Login Processing...</div>');

       if(user != ''){
           var login = $.ajax({
            url: baseURL+"users/logincheck",
            method: "POST",
            data: { User:{wsusername : user, wspassword:pass }},
            dataType: "json"
          });

          login.done(function( data ) {
              if(data.code ==200){
                 $('.ShowLoginErrow').html('<div id="name-error" class="help-block text-left animated fadeInDown loginSuccess">'+data.msg+'</div>');
                window.location = baseURL+'dashboards';
              }else{
                 $('.ShowLoginErrow').html('<div id="name-error" class="help-block text-left animated zoomIn loginError">'+data.msg+'</div>');
              }
          });

          login.fail(function( jqXHR, textStatus ) {
           console.log('404');
          });
       }
   }); 
   
if($('.noticeModelHandel').length > 0){
    $('.getModelNotice').click(function (){
       var notice = $(this).closest('.notice'); 
       var content = $('.FullContent', notice).html(); 
       $('#NoticeModel .block-content').html(content);
       $('#NoticeModel .block-title').html($(this).html());
       $('#NoticeModel').modal('show');
    });
}

if($('.regSubBtn').length > 0){
    $('.regSubBtn').click(function (){
        var name = $('#RegisterName').val();
        var phone = $('#RegisterPhone').val();
        var email = $('#RegisterEmail').val();
        var pas = $('#RegisterPassword').val();
        var sponsor = $('#RegisterSponsorPhone').val();
        var redirect_reg = $('.redirect_reg').val();

        var address = $('.addressR').val();

        if(phone != '' && phone.length == 11){
        $('.ShowLoginErrowReg').html('Loading...').show('slow');
            var login = $.ajax({
                url: baseURL+"register_now",
                method: "POST",
                data: {
                    name:name,
                    phone: phone,
                    email: email,
                    pass: pas,
                    sponsor: sponsor,
                    redirect_reg: redirect_reg,
                    address: address
                },
                dataType: "json"
            });

            login.done(function( data ) {
                if(data.code ==200){
                    $('.ShowLoginErrowReg').html('<div id="name-error" class="help-block text-left animated fadeInDown loginSuccess">'+data.msg+'</div>');
                    window.location = baseURL+data.to;
                }else{
                    $('.ShowLoginErrowReg').html('<div id="name-error" class="help-block text-left animated zoomIn loginError">'+data.msg+'</div>');
                }
            });

        }else{
                $('.ShowLoginErrowReg').html('<div id="name-error" class="help-block text-left animated fadeInDown loginSuccess">Invalid</div>');
        }

    });
}
if($('.loginBtnC').length > 0){

    $('.onEnterRunLogin').keypress(function (e) {
        if (e.which == 13) {
            logIN();
        }

    });

    $('.loginBtnC').click(function (){
        logIN();
    });
}

    var logIN = function () {
        var name = $('#LoginName').val();
        var pas = $('#LoginPassword').val();
        if (name != '') {
            $('.ShowLoginErrowLogin').html('Loading...').show('slow');
            var login = $.ajax({
                url: baseURL + "login_now",
                method: "POST",
                data: {
                    User: {
                        wsusername: name,
                        wspassword: pas
                    }
                },
                dataType: "json"
            });

            login.done(function (data) {
                if (data.code == 200) {
                    $('.ShowLoginErrowLogin').html('<div id="name-error" class="help-block text-left animated fadeInDown loginSuccess">' + data.msg + '</div>');
                    window.location = baseURL + data.to;
                } else {
                    $('.ShowLoginErrowLogin').html('<div id="name-error" class="help-block text-left animated zoomIn loginError">' + data.msg + '</div>');
                }
            });
        } else {
            $('.ShowLoginErrowLogin').html('<div id="name-error" class="help-block text-left animated fadeInDown loginSuccess">Invalid</div>');
        }
    };


$('.OnChangeGetSubLocation').change(function(){
    var id = $('option:selected', this).val();
    $('.conSubLocationLoading').fadeIn();
    var getSection = $.ajax({
        url: baseURL+"locations/get_sub_location",
        method: "POST",
        data: { id:id},
        dataType: "json"
    });
    getSection.done(function( data ) {
        $('.UpdateOnChangeLocation').find('option').remove();
        $('.UpdateOnChangeLocation').append('<option value="" selected="selected">Select</option>');
        $.each(data, function(i, obj) {
            $('.UpdateOnChangeLocation').append($('<option></option>').text(obj).attr('value', i));
        })
        $('.conSubLocationLoading').fadeOut('slow');
        $('.conSubLocation').fadeIn();
    });
    getSection.fail(function( jqXHR, textStatus ) {
        $('.conSubLocationLoading').fadeOut('slow');
        console.log('404');
    });

});

$('.OnChangeLocationID').change(function(){
    var id = $('option:selected', this).val();
    $('.conShopLoading').fadeIn();
    var getSection = $.ajax({
        url: baseURL+"shops/get_shop",
        method: "POST",
        data: { id:id},
        dataType: "html"
    });
    getSection.done(function( data ) {
        $('.conShopLoading').fadeOut('slow');
        $('.UpdateOnChangeLocationID').html(data);
    });
});



if ($('.singleZoomBox').length > 0) {
    $('.jqzoom').jqzoom({
        zoomType: 'standard',
        lens: true,
        preloadImages: true,
        alwaysOn: false
    });
}


if ($('.forgotPassLogin').length > 0) {
    $('.forgotPassLogin').click( function () {
        $('.forgotFormShow').slideDown();
        $('.loginFormShow').slideUp();
        $('.ShowLoginErrowLogin').html(' ');
    });
    $('.backToLogin').click( function () {
        $('.loginFormShow').slideDown();
        $('.forgotFormShow').slideUp();
        $('.ShowLoginErrowLogin').html(' ');
    });
}


var widthDisplay  = $(document).width() - cart_holder_width;

$('.customWidth').css('max-width', widthDisplay);
$('.customWidthInner').css('width', widthDisplay-277);


if($('.recoverPassBtn').length > 0){

    $('.onEnterRunForgot').keypress(function (e) {
        if (e.which == 13) {
            fotgotPass();
        }

    });

    $('.recoverPassBtn').click(function (){
        fotgotPass();
    });
}

var fotgotPass = function () {
    var name = $('#ForgotName').val();
    var sponsor = $('#ForgotSponsorPhone').val();
    if (name != '') {
        $('.ShowLoginErrowLogin').html('Loading...').show('slow');
        var login = $.ajax({
            url: baseURL + "forgot_password",
            method: "POST",
            data: { wsusername: name, sponsor: sponsor },
            dataType: "json"
        });

        login.done(function (data) {
            if (data.code == 200) {
                $('.ShowLoginErrowLogin').html('<div id="name-error" class="help-block text-left animated fadeInDown loginSuccess">' + data.msg + '</div>');
            } else {
                $('.ShowLoginErrowLogin').html('<div id="name-error" class="help-block text-left animated zoomIn loginError">' + data.msg + '</div>');
            }
        });
    } else {
        $('.ShowLoginErrowLogin').html('<div id="name-error" class="help-block text-left animated zoomIn loginError">Invalid Information</div>');
    }
};

    if($('#RegisterAddForm').length > 0){

        $('#RegisterSponsorPhone').keyup(function () {
            if ($('#RegisterSponsorPhone').val().length == 11 ) {
                checkSponsor();
            }
        });

        var checkSponsor = function () {
            var name = $('#RegisterSponsorPhone').val();

                $('.sponsorNameShow').addClass('fa-spinner');
                var login = $.ajax({
                    url: baseURL + "check_sponsor",
                    method: "POST",
                    data: {

                        wsusername: name

                    },
                    dataType: "json"
                });

                login.done(function (data) {
                    if (data.code == 200) {
                        $('.sponsorNameShow').removeClass('fa-spinner').html(data.msg);
                    }
                });

        };
    }


});