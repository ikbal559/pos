/**
 * Prometheus
 *
 * Configeration wrapper for datatables.js
 *
 * LICENSE:
 *
 * @package     Prometheus
 * @subpackage  Datatables
 * @author      Nick Woodhead <naw103@gmail.com>
 * @copyright   Copyright (c) 2016
 * @license
 * @version     1.0
 * @link        https://www.Prometheus.com
 */

(function ($) {
    $.fn.systemMultiTables = function (options) {

        if (!$(this.selector).length) {
            return false;
        }

        var start_date,end_date;
        var table;

        var settings = $.extend({
            // These are the defaults.
            columnDefs: [],
            listName: '',
            params: []
        }, options);
        params = $.extend(params,settings.params);
        table = this.DataTable({
            stateSave: true,
            responsive: true,
            processing: true,
            //columnDefs: settings.columnDefs,
            serverSide: true,
            searchDelay: '1200',
            "language": {
                "lengthMenu": "Show _MENU_",
                "sSearch": '<i class="fa fa-search"></i>',
                'searchPlaceholder': "Search",
                "buttons": {
                    'colvis':'<i class="fa fa-th" aria-hidden="true"></i>'
                }
            },
            ajax: {
                url: base_url + '/load/' + settings.listName,
                method: 'POST',
                beforeSend: function (request) {
                    request.setRequestHeader("X-CSRF-TOKEN", Laravel.csrfToken);
                },
                'data': function (d) {
                    if($('#systemTables_min2').length>0){
                        //getting start date, end date
                        var startDate = $('#systemTables_min2').val() || '';
                        var endDate = $('#systemTables_max2').val() || '';
                    }else{
                        var startDate = params.start_date || '';
                        start_date = params.start_date || '';

                        var endDate = params.end_date || '';
                        end_date = params.end_date || '';
                    }

                    //getting start date, end date in an array
                    var startDate = startDate.split('/');
                    var endDate = endDate.split('/');

                    //creating date string of start date
                    if (startDate.length > 1) {
                        startDate = startDate[2] + '-' + startDate[0] + '-' + startDate[1];
                    } else {
                        startDate = '';
                    }

                    //creating date string of end date
                    if (endDate.length > 1) {
                        endDate = endDate[2] + '-' + endDate[0] + '-' + endDate[1];
                    } else {
                        endDate = '';
                    }

                    //pushing start and end date to params array
                    params.start_date = startDate;
                    params.end_date = endDate;
                    d.customData = params;
                }
            },
            columns: settings.columnDefs,
            dom: "<'dataTableHead'<'col-sm-7 dataTableEntriesLeft'l><'col-sm-5 dataTableEntriesRight'Bf>>" +
            "<'contentTable'<''tr>>" +
            "<'dataTableFood'<'col-sm-5'i><'col-sm-7'p>>",
            colReorder: true,
            buttons: [
                'colvis'
            ],
            //Call back triggered whenever there is a change in datatable
            stateSaveCallback: function (table, data) {
                $.ajax({
                    method: "POST",
                    url: base_url + "/savestate/" + settings.listName,
                    async: false,
                    dataType: "json",
                    data: {data: JSON.stringify(data)}
                });
            },
            //Callback trigerred when the datatable is initiated
            stateLoadCallback: function (table) {

                var json = '';
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    async: false,
                    url: base_url + "/getstate/" + settings.listName,
                    success: function (msg) {
                        json = msg;
                    }
                });
                return  json;

            }
        });

        var dateFilterHtml = '<div class="dataTableDate"><span class="entriesSpan hidden-xs">Entries</span><div class="input-group"><input id="systemTables_min2" data-date-start-date="0d" class="datepicker form-control" name="min" placeholder="From"><span class="input-group-addon"><i class="fa fa-calendar"></i></i></span></div><div class="input-group"><input id="systemTables_max2"  class="datepicker form-control" name="max" placeholder="To"><span class="input-group-addon""><i class="fa fa-calendar"></i></span></div></div>';

        $(this.selector + '_length').prepend(dateFilterHtml);

        if (settings.params.hasOwnProperty('start_date')) {
            $('#systemTables_min2').val(settings.params.start_date);
        }

        if (settings.params.hasOwnProperty('end_date')) {
            $('#systemTables_max2').val(settings.params.end_date);
        }

        //redrawing when filters are changed
        $('#systemTables_min2, #systemTables_max2').change(function () {

            if($(this).attr('id') == 'systemTables_min2'){
                $("#systemTables_max2").datepicker('destroy');
                $("#systemTables_max2").datepicker({startDate:$("#systemTables_min2").datepicker("getDate")});
            }

            table.ajax.reload();
        });

        //initializing date picker
        $('.datepicker').datepicker();

        return this;
    };
}(jQuery));