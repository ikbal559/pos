/**
 /**
 * Prometheus
 *
 * Used to create all datatable instances for the system
 *
 * LICENSE:
 *
 * @package     Prometheus
 * @subpackage  Datatables
 * @author      Nick Woodhead <naw103@gmail.com>
 * @copyright   Copyright (c) 2016
 * @license
 * @version     1.0
 * @link        https://www.Prometheus.com
 */

$(document).ready(function () {

    var base_url = window.base_url + '/load/';
    $('#category-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Products/Category' ,
        columns: [
            { data: 'category_id', name: 'category_id' },
            { data: 'category_name', name: 'category_name' },
            { data: 'action', name: 'action' },

        ]
    });
    $('#business-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Products/BusinessTypes' ,
        columns: [
            { data: 'business_types_id', name: 'business_types_id' },
            { data: 'name', name: 'name' },
            { data: 'action', name: 'action' },

        ]
    });



    $('#entry-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Products/Entry' ,
        columns: [
            { data: 'product_id', name: 'product_id' },
            { data: 'product_name', name: 'product_name' },
            { data: 'quantity', name: 'quantity' },
            { data: 'product_price', name: 'product_price' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' },

        ]
    });

    $('#damage-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Products/Damage' ,
        columns: [
            { data: 'product_id', name: 'product_id' },
            { data: 'product_name', name: 'product_name' },
            { data: 'quantity', name: 'quantity' },
            { data: 'note', name: 'note' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' },

        ]
    });



    $('#shop-payments-data-table').DataTable({
        processing: true,
        serverSide: true,
        order: [ [0, 'desc'] ],
        ajax: base_url + 'Transaction/ShopPayment' ,
        columns: [
            { data: 'transaction_shop_payment_id', name: 'transaction_shop_payment_id' },
            { data: 'shop_id', name: 'shop_id' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name: 'created_at' },
            { data: 'amount', name: 'amount' },
            { data: 'balance', name: 'balance' },

        ]
    });

    $('#shop-payment-admin-data-table').DataTable({
        processing: true,
        serverSide: true,
        order: [ [0, 'desc'] ],
        ajax: base_url + 'Transaction/ShopPayment?admin=1' ,
        columns: [
            { data: 'transaction_shop_payment_id', name: 'transaction_shop_payment_id' },
            { data: 'shop_id', name: 'shop_id' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name: 'created_at' },
            { data: 'amount', name: 'amount' },
            { data: 'balance', name: 'balance' },

        ]
    });

    $('#user-account-transaction-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Transaction/AccountTransaction' ,
        order: [ [0, 'desc'] ],
        columns: [
            { data: 'transaction_account_id', name: 'transaction_account_id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'description', name: 'description' },
            { data: 'type', name: 'type' },
            { data: 'amount', name: 'amount' },
            { data: 'balance', name: 'balance' },

        ]
    });


    $('#manage-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Products/ManageProducts' ,
        columns: [
            { data: 'manage_products_id', name: 'manage_products_id' },
            { data: 'manage_products_type', name: 'manage_products_type'},
            { data: 'manage_products_invoice', name: 'manage_products_invoice' },
            { data: 'manage_products_quantity', name: 'manage_products_quantity' },
            { data: 'manage_products_total', name: 'manage_products_total' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' },
        ]
    });

    $('#marketing-sms-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Marketing/Marketing?type=sms' ,
        columns: [
            { data: 'to', name: 'to' },
            { data: 'total', name: 'total' },
            { data: 'content', name: 'content'},
            { data: 'created_at', name: 'created_at' },
        ]
    });

    $('#marketing-email-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Marketing/Marketing?type=email' ,
        columns: [
            { data: 'to', name: 'to' },
            { data: 'total', name: 'total' },
            { data: 'content', name: 'content'},
            { data: 'created_at', name: 'created_at' },
        ]
    });

    $('#account-payment-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Transaction/AccountPayments' ,
        columns: [
            { data: 'transaction_account_payment_id', name: 'transaction_account_payment_id' },
            { data: 'created_at', name: 'created_at'},
            { data: 'user_id', name: 'user_id'},
            { data: 'description', name: 'description' },
            { data: 'amount', name: 'amount' },
            { data: 'balance', name: 'balance' },
        ]
    });

// Reporting

    $('#report-store-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Products/Products?store_report=1' ,
        columns: [
            { data: 'product_id', name: 'product_id' },
            { data: 'product_name', name: 'product_name' },
            { data: 'import', name: 'import', 'searchable': false, orderable: false },
            { data: 'export', name: 'export', 'searchable': false, orderable: false },
            { data: 'damage', name: 'damage', 'searchable': false, orderable: false },
            { data: 'sales', name: 'sales', 'searchable': false, orderable: false },
            { data: 'balance', name: 'balance', 'searchable': false, orderable: false },

        ]
    });

    $('#report-sales-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Products/Products?sales_report=1' ,
        columns: [
            { data: 'product_id', name: 'product_id' },
            { data: 'product_name', name: 'product_name' },
            { data: 'import', name: 'import', 'searchable': false, orderable: false },
            { data: 'export', name: 'export', 'searchable': false, orderable: false },
            { data: 'damage', name: 'damage', 'searchable': false, orderable: false },
            { data: 'sales', name: 'sales', 'searchable': false, orderable: false },
        ]
    });

    $('#admin-accounts-list-data-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searchDelay: '1200',
        ajax: base_url + 'Users/User?admin_network=1' ,
        columns: [
            { data: 'id', name: 'id' },
            { data: 'username', name: 'username' },
            { data: 'name', name: 'name',  },
            { data: 'name', name: 'name',  },

        ]
    });
    //
    // $('#admin-accounts-list-manager-data-table').DataTable({
    //     processing: true,
    //     responsive: true,
    //     serverSide: true,
    //     searchDelay: '1200',
    //     ajax: base_url + 'Users/User?admin_network_manager=1' ,
    //     columns: [
    //         { data: 'id', name: 'id' },
    //         { data: 'username', name: 'username' },
    //         { data: 'name', name: 'name',  },
    //         { data: 'sponsor', name: 'sponsor', orderable: false },
    //         { data: 'point',   name: 'point', 'searchable': false },
    //         { data: 'balance', name: 'balance', 'searchable': false },
    //     ]
    // });
    //
    // $('#managers-data-table').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: base_url + 'Users/User?admin_manager=1' ,
    //     columns: [
    //         { data: 'id', name: 'id' },
    //         { data: 'username', name: 'username' },
    //         { data: 'name', name: 'name',  },
    //         { data: 'action', name: 'action' },
    //
    //     ]
    // });

    $('#location-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Users/Locations' ,
        columns: [
            { data: 'location_id', name: 'location_id' },
            { data: 'division_id', name: 'division_id' },
            { data: 'district_id', name: 'district_id' },
            { data: 'name', name: 'name' },
            { data: 'action', name: 'action' },

        ]
    });

    $('#UserClients-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Users/Client' ,
        columns: [
            { data: 'action', name: 'action' },
            { data: 'client_id', name: 'client_id' },
            { data: 'name', name: 'name' },
            { data: 'balance', name: 'balance' },
            { data: 'phone', name: 'phone' },
            { data: 'address', name: 'address' },
        ]
    });

    $('#Members-data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base_url + 'Users/Member' ,
        columns: [
            { data: 'username', name: 'username' },
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
        ]
    });


});