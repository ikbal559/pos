/*
 *  Document   : base_forms_wizard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Wizard Page
 */

var BaseFormWizard = function() {

    // Init wizards with validation, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
    var initWizardValidation = function(){
        // Get forms
        var $form2 = jQuery('.StudentInformationForm');
        var $validationHolder = jQuery('.validationGroup');
 

        // Init form validation on the other wizard form
        var $validator2 = $form2.validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.validationGroup >  div').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest($validationHolder).removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest($validationHolder).removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'data[Student][name]': {
                    required: true,
                    minlength: 4
                },
                'data[Student][roll]': {                  
                    minlength: 4,
                    number: true
                },
                'data[Student][class]': {
                    required: true,
                },
                'data[Student][section]': {
                    required: true
                },
                'data[Student][birthday]': {
                    required: true
                },
                'data[Guardian][name]': {
                    required: true,
                    minlength: 4
                },
                'data[Guardian][phone]': {
                    required: true,
                    number: true,
                    minlength: 6
                },
            },
            messages: {
                'data[Student][name]': {
                    required: 'Please enter student name',
                    minlength: 'Name must consist of at least 4 characters'
                },
                'data[Student][roll]': {                    
                    minlength: 'Pleas enter a valid roll number'
                },
                'data[Student][class]': 'Please select a class.',
                'data[Student][section]': 'Please select a section.',
                'data[Student][birthday]': 'Please select a Date.',
                'data[Guardian][name]': {
                    required: 'Please enter parents name',
                    minlength: 'Name must consist of at least 4 characters'
                },
                'data[Guardian][phone]': {
                    required: 'Please enter phone number',
                    minlength: 'Pleas enter a valid phone number'
                },
            }
        });

 
        // Init wizard with validation
        jQuery('.js-wizard-validation').bootstrapWizard({
            'tabClass': '',
            'previousSelector': '.wizard-prev',
            'nextSelector': '.wizard-next',
            'onTabShow': function($tab, $nav, $index) {
		var $total      = $nav.find('li').length;
		var $current    = $index + 1;

                // Get vital wizard elements
                var $wizard     = $nav.parents('.block');
                var $btnNext    = $wizard.find('.wizard-next');
                var $btnFinish  = $wizard.find('.wizard-finish');

		// If it's the last tab then hide the last button and show the finish instead
		if($current >= $total) {
                    $btnNext.hide();
                    $btnFinish.show();
		} else {
                    $btnNext.show();
                    $btnFinish.hide();
		}
            },
            'onNext': function($tab, $navigation, $index) {
                var $valid = $form2.valid();
                if(!$valid) {
                    $validator2.focusInvalid();

                    return false;
                }
            },
            onTabClick: function($tab, $navigation, $index) {
		return false;
            }
        });
    };

    return {
        init: function () {
            // Init wizards with validation
            initWizardValidation();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseFormWizard.init(); });