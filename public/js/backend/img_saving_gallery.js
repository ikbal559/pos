$(document).ready(function (){ 

// Variable to store your files
var files;

// Add events
$('.SocityFileUploadGallery').on('change', prepareUpload);
// Grab the files and set them to our variable
function prepareUpload(event)
{
    $('.LoadingAjex').fadeIn('slow');                               
    event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening
    
    var folder = $(this).attr('data-folder');
    files = event.target.files;
    var target = $(this).closest('.uploadContainer');
    // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });
        data.append('folder', folder);

    $.ajax({
        url: '/uploadimages/upload_new',
        type: 'POST',
        data:data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if (data.code == 200)
            {
                var countImages = $('.GalleryNewImages').length +1;
                var htmlData = "<li class='list-group-item indent10 col-md-3 row GalleryNewImages' ><input type='hidden' value='"+data.file+"'  name='data[ProductImage]["+countImages+"][name]'><img  width='60' src='/upload/gallery/thumbs/"+data.file+"'><i class='fa fa-trash-o fa-1x removeImages'></i></li>";
                $('.NewImages').append(htmlData);
                removeImages();
                $('.LoadingAjex').fadeOut('slow');
            }
            else
            {
                $('.LoadingAjex').fadeOut('slow');                               
                // Handle errors here
                console.log('ERRORS: ' + data.code);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
                $('.LoadingAjex').fadeOut('slow');                               
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
}

    var removeImages = function (){
        $('.removeImages').click(function (){
            $(this).closest('.list-group-item').remove();
        });
        $('.removeImagesAjex').click(function (){
            $('.LoadingAjex').fadeIn();
            $(this).closest('.list-group-item').remove();
            var images_id = $(this).attr('data-id');
            var getSection = $.ajax({
                url: baseURL+"pages/remove_images_ajex",
                method: "POST",
                data: { id: images_id },
                dataType: "json"
            });

            getSection.done(function( data ) {
                $('.LoadingAjex').fadeOut('slow');
            });

            getSection.fail(function( jqXHR, textStatus ) {
                $('.LoadingAjex').fadeOut('slow');
                console.log('404');
            });

        });
    };
    removeImages();

});