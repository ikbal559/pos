$(document).ready(function (){ 





// Variable to store your files
var files;

// Add events
$('.SocityFileUpload').on('change', prepareUpload);
// Grab the files and set them to our variable
function prepareUpload(event)
{
    $('.LoadingAjex').fadeIn('slow');                               
    event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening
    
    var folder = $(this).attr('data-folder');
    files = event.target.files;
    var target = $(this).closest('.uploadContainer');
    // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });
        data.append('folder', folder);

    $.ajax({
        url: '/uploadimages/upload_new',
        type: 'POST',
        data:data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if (data.code == 200)
            {
                // Success so call function to process the form
                $('.NewImages', target).attr('src', '/upload/'+folder+'/thumbs/'+data.file);
                $('.UpdateNewPhotoName', target ).attr('value', data.file);
                $('.LoadingAjex').fadeOut('slow');                               
                
            }
            else
            {
                $('.LoadingAjex').fadeOut('slow');                               
                // Handle errors here
                console.log('ERRORS: ' + data.code);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
                $('.LoadingAjex').fadeOut('slow');                               
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });





}
});