initUpload();
function initUpload() {
        var settings = "settings_";
        settings = {
            url: "/uploadimages/teachers",
            method: "POST",
            allowedTypes: "jpg,png,gif",
            fileName: "Filedata",
            folderName: "student",
            multiple : false,
            onSuccess: function(files, data, response)
            {
 
                $('.NewImages').attr('src', '/upload/teachers/thumbs/'+data);
                $('.UpdateNewPhotoName').attr('value', data);
                $('.LoadingAjex').fadeOut('slow');
            },
            afterUploadAll: function()
            {
             
            },
            onError: function(files, status, errMsg)
            {
                 $('.LoadingAjex').fadeOut('slow');
            }
        }
        
        $("#file_upload1").uploadFile(settings);
}

 