
$(window).load(function (e) {
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
        cache: false,
        url: $('.posProductItem').attr('data-read'),
        data: {},
        beforeSend: function(){
            $('.spinner').removeClass('hidden');
        },

        success: function(json){
            $('.spinner').addClass('hidden');
            if(json.status == 'success'){
                if(json.cart.qty > 0){
                    
                    $('.posProductItem').html('');
                    $.each(json.cart.items, function( index, product ) {
                        var itemHtml = '<tr class="manageItems">' +
                            '<td class="text-center"><button class="btn btn-xs btn-default RemoveProductPOS" data-product-id="'+ index +'"   type="button" data-toggle="tooltip" title="" data-original-title="Remove Product"><i class="fa fa-times"></i></button></td>' +
                            '<td class="itemNamePOS"># ' + index + ' '+ product.product_name +'</td>' +
                            '<td  style="width: 80px;" ><input min="1" data-last="'+ product.qty +'" class="itemQtyPOS form-control" data-id="'+ index +'" type="number" value="'+ product.qty +'"></td>' +
                            '<td  style="width: 140px;" ><input min="1" data-last="'+ product.price +'" class="itemPricePOS form-control" data-id="'+ index +'" type="number" value="'+ product.price +'"></td>' +
                            '<td class="text-right itemTotalPOS"> '+ product.total +'</td>' +
                            '</tr>';
                        $('.posProductItem').append(itemHtml);
                    });

                }else{
                    $('.posProductItem').html('<tr><td colspan="5" class="text-center">No Item</td></tr>');
                }

                    $('.CartItemTotal').html(json.cart.qty);
                    $('.CartTotal').html(json.cart.total +'<span style="font-size: 12px;">tk</span>');
                }
        },
        error : function(){
        },
        dataType: "json"
    });
});


var addProduct = function (id, qty, price) {
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
        cache: false,
        url: $('.add-product-url').attr('data-add-product-url'),
        data: { 'id' : id, 'qty' : qty  , 'price' : price  },
        beforeSend: function(){
            $('.spinner').removeClass('hidden');
        },
        success: function(json){
            $('.spinner').addClass('hidden');
            if(json.status == 'success'){
                if(json.cart.qty > 0){
                $('.posProductItem').html('');
                    $.each(json.cart.items, function( index, product ) {
                        var itemHtml = '<tr class="manageItems">' +
                            '<td  class="text-center"><button class="btn btn-xs btn-default RemoveProductPOS" data-product-id="'+ index +'"   type="button" data-toggle="tooltip" title="" data-original-title="Remove Product"><i class="fa fa-times"></i></button></td>' +
                            '<td  class="itemNamePOS"># ' + index + ' '+ product.product_name +'</td>' +
                            '<td  style="width: 80px;" ><input min="1" data-last="'+ product.qty +'" class="itemQtyPOS form-control" data-id="'+ index +'" type="number" value="'+ product.qty +'"></td>' +
                            '<td  style="width: 100px;" ><input min="1" data-last="'+ product.price +'" class="itemPricePOS form-control" data-id="'+ index +'" type="number" value="'+ product.price +'"></td>' +
                            '<td  class="text-right itemTotalPOS"> '+ product.total +'</td>' +
                            '</tr>';
                        $('.posProductItem').append(itemHtml);
                    });
                }else{
                    $('.posProductItem').html('<tr><td colspan="5" class="text-center">No Item</td></tr>');
                }

                    $('.CartItemTotal').html(json.cart.qty);
                    $('.CartTotal').html(json.cart.total +'<span style="font-size: 12px;">tk</span>');

            }
        },
        error : function(){
        },
        dataType: "json"
    });
};

var delaySearch = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


$(document).ready(function(e) {
    
    $(".ProductSelectBox").select2().on("change", function (e) {
        var id = $(this).val();
        if(id > 0){
            addProduct($(this).val(), 1, 0)
        }
    });

    $(".posProductItem").on("click", '.RemoveProductPOS', function (e) {
        addProduct($(this).attr('data-product-id'), 0);
    });

    $('.posProductItem').on("keyup change", '.itemQtyPOS', function (e) {
        var id = $(this).attr('data-id');
        var qty = $(this).val();
        var price = $(this).closest('tr.manageItems').find('.itemPricePOS').val();

        if (qty != $(this).attr('data-last')) {
            delaySearch(function () {
                addProduct(id, qty, price);
            }, 400);
        }
        $(this).attr('data-last',qty);
    });

    $('.posProductItem').on("keyup change", '.itemPricePOS', function (e) {
        var id = $(this).attr('data-id');
        var price = $(this).val();
        var qty = $(this).closest('tr.manageItems').find('.itemQtyPOS').val();

        if (price != $(this).attr('data-last')) {
            delaySearch(function () {
                addProduct(id, qty, price);
            }, 400);
        }
        $(this).attr('data-last',qty);
    });

    $('.manageType').on("click", '.manage_type', function (e) {
        var manage_type = $("input[name='manage_type']:checked").val();
        $('.modelBtnComplite').removeClass('hidden');
        $('.InputManageType').val(manage_type);

    });

    $('.form-vertical-manage')
        .formValidation()
        .on('success.form.fv', function(e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({

                type: "POST",
                cache: false,
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function(){
                    $('.spinner').removeClass('hidden');
                    form.find('#please-wait').removeClass('hidden');
                    form.find('#success').addClass('hidden');
                    form.find('#error').addClass('hidden');

                },
                success: function(json){
                if(json.status == 'success'){
                        window.location.href = json.manage_url;
                    }else {
                        var error = form.find('#error');
                        error.find('.message').text('Please Typ Later!');
                        error.removeClass('hidden');
                    }

                },

                error : function(){
                    var error = form.find('#error');
                    error.find('.message').text('Internal Server Error');
                    error.removeClass('hidden');
                },
                dataType: "json"

            });
        });
 });

