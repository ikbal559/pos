$(document).ready( function () {
    $('.onChangeShowSub').change(function () {
        var category = $(this).val();
        var url = $(this).attr('data-url');

        $.ajax({
            type: "POST",
            cache: false,
            headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
            url: url,
            data: { id:category},
            success: function(json){
                var html ='<option selected="selected" disabled="disabled" hidden="hidden" value="">Select Sub Category</option>'
                $.each(json, function (key, val) {
                    html +='<option value="'+val.category_id+'" >'+val.category_name+'</option>';
                });
                $('.UpdateOnMainChange').html(html);
            },
            error : function(){
                console.log('error');
            },
            dataType: "json"
        });

    });
});