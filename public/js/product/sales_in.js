$(document).ready(function(e) {

    $('.form-vertical-storeIn')
        .formValidation()
        .on('success.form.fv', function(e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({

                type: "POST",
                cache: false,
                url: form.attr('action'),
                data: form.serialize(),
                success: function(html){
 

                    $('.productINfoSuccess').html(html);
                },
                error : function(){
                    var error = form.find('#error');
                    error.find('.message').text('Internal Server Error');
                    error.removeClass('hidden');
                },
                dataType: "html"

            });
        });



    $('.StoreInOnChange').change(function () {
        var val = $(this).val();
        var html  = '';

        if(val > 0){
            $.ajax({
                headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
                type: "POST",
                cache: false,
                url: $('.onChangeUrl').attr('data-onChangeUrl'),
                data: {id: val},
                success: function(html){
                    $('.productINfo').html(html);
                },


                dataType: "html"

            });
        }

    });

 });


