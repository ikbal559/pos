var cropper;
$(document).ready(function(e){

    //
    // Upload Photo
    $("#uploadPhoto").click(function(e){
        $('#profile_photo').trigger('click');
    });

    $('#profile_photo').change(function(e){
        destroyImage();
        readURL(this);
        $('#profile_photo').val("")

    });


    $('#confirmImage').click(function(e){
        destroyImage();

        // Get Image URI
        var imageURI = $('#my_result').find('img').attr('src');
        $('#imageCropper').attr('src',imageURI);

        // Init Cropper
        initCropper();
    });


    $('#imageCropperModal').on('hidden.bs.modal', function () {

        $('#imageCropper').removeAttr('src');
        $('.cropper-container').remove();

        // do something…
        if(cropper)
            destroyImage();

    });



// Image Upload
    $('#formProfilePicutre').formValidation()
        .on('success.form.fv', function(e) {
            // Prevent default form submission
            e.preventDefault();

            var form = $(this);
            $.ajax({
                type: "POST",
                cache: false,
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function(){
                    $('#modal-please-wait').removeClass('hidden');
                    $('#modal-success').addClass('hidden');
                    $('#modal-error').addClass('hidden');
                },
                success: function(json){
                    $('#modal-please-wait').addClass('hidden');

                    if(json.status == 'success'){
                        $('#modal-success').removeClass('hidden');
                        $('#modal-success').find('.message').html(json.message);
                        $('#product-image').attr('src',json.image);
                        $('#product_images').val(json.image_name);

                        setTimeout(function(e){
                            $('#modal-success').addClass('hidden');
                            $('#imageCropperModal').modal('hide');
                            $('#profile_photo').val('');

                        },1000);

                    } else if(json.status == 'validation'){

                        //Showing an Error Message
                        var error = form.find('#error');
                        error.find('.message').text('Please Correct the Marked Errors');
                        error.removeClass('hidden');

                        //Getting Form Validation Instance
                        var fv    = form.data('formValidation');

                        for (var field in json.message) {
                            fv
                            // Show the custom message
                                .updateMessage(field, 'blank', json.message[field])
                                // Set the field as invalid
                                .updateStatus(field, 'INVALID', 'blank');
                        }
                    }else if (json.status == 'error'){
                        $('#modal-error').removeClass('hidden');
                        $('#modal-error').find('.message').html(json.message);

                        setTimeout(function(e){
                            $('#modal-error').addClass('hidden');
                        },2000);
                    }
                },
                error : function(json){
                    var error = form.find('#modal-error');
                    error.find('.message').text('Internal Server Error');
                    error.removeClass('hidden');
                },
                complete:function(){
                    form.find('#upload').removeAttr('disabled').removeClass('disabled');
                },
                dataType: "json"
            });
        });


});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageCropper').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

        // Initialize Cropper
        initCropper();


    }
}

function convertFileToDataURLviaFileReader(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}


function initCropper(){


    var image = document.getElementById('imageCropper');
    var preview = document.getElementById('imgPreview')
    setTimeout(function(){
        cropper = new Cropper(image, {
            aspectRatio: 1 / 1,
            preview:'#imgPreview',
            crop: function(e) {

                $('#hdn_profile_image').val($(image).attr('src'));
                $('#hdn_image_height').val(parseInt(e.detail.width));
                $('#hdn_image_width').val(parseInt(e.detail.height));
                $('#hdn_image_x').val(parseInt(e.detail.x));
                $('#hdn_image_y').val(parseInt(e.detail.y));

            }
        });
    },1000);

    $('.web-cam-space').addClass('hide');
    $('.crop-space,.modal-footer').removeClass('hide');

    $('#imageCropperModal').modal({backdrop: 'static', keyboard: false,show:true});
}

function destroyImage(){
    if(cropper){
        console.log("destroyed");
        cropper.destroy();
    }

}

