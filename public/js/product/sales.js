
$(window).load(function (e) {
    $(".ProductSelectBoxCode").focus();

    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
        cache: false,
        url: $('.posProductItem').attr('data-read'),
        data: {},
        beforeSend: function(){
            $('.spinner').removeClass('hidden');
        },

        success: function(json){
            $('.spinner').addClass('hidden');
            if(json.status == 'success'){
                if(json.cart.qty > 0){
                    
                    $('.posProductItem').html('');
                    $.each(json.cart.items, function( index, product ) {
                        var itemHtml = '<tr>' +
                            '<td class="text-center"><button class="btn btn-xs btn-default RemoveProductPOS" data-product-id="'+ product.product_id +'"   type="button" data-toggle="tooltip" title="" data-original-title="Remove Product"><i class="fa fa-times"></i></button></td>' +
                            '<td class="itemNamePOS"># ' +  product.product_id + ' '+ product.product_name +'</td>' +
                            '<td style="width: 40px;"  class="itemPricePOS text-left"> '+ product.store_in +'</td>' +
                            '<td style="width: 80px;"  class="itemPricePOS text-left"> '+ product.price +'</td>' +
                            '<td  style="width: 80px;" ><input  min="0" data-last="'+ product.qty +'" class="itemQtyPOS form-control" data-id="'+  product.product_id +'" type="number" value="'+ product.qty +'"></td>' +
                            '<td style="width: 80px;"  class="itemPricePOS text-left"> '+ product.point +'</td>' +
                            '<td class="text-right itemTotalPOS"> '+ product.total +'</td>' +
                            '</tr>';
                        $('.posProductItem').append(itemHtml);
                    });
                }else{
                    $('.posProductItem').html('<tr><td colspan="5" class="text-center">No Item</td></tr>');
                }


                    $('.CartTotalPoint').html(json.cart.point);
                    $('.CartItemTotal').html(json.cart.qty);
                    $('.CartTotal').html(json.cart.total +'<span style="font-size: 12px;">tk</span>');
                if(json.cart.order_id > 0){
                    $('.paidAmount').val(json.cart.order.paid);

                    var orderHtml ='<td colspan="2">' +

                        '<div class="form-group" style="margin-bottom: 10px;">' +
                            '<label for="" class="col-sm-3 control-label">Name:</label>' +
                            '<div class="col-sm-9 text-left cusName" style="padding-top: 7px;">'+ json.cart.order.member.name +'</div>' +
                        '</div>' +

                        '<div class="form-group" style="margin-bottom: 10px;">' +
                            '<label for="" class="col-sm-3 control-label">Phone:</label>' +
                            '<div class="col-sm-9 text-left cusName" style="padding-top: 7px;">'+ json.cart.order.member.username +'</div>' +
                        '</div>' +

                        '<div class="form-group" style="margin-bottom: 10px;">' +
                            '<label for="" class="col-sm-3 control-label">Address:</label>' +
                            '<div class="col-sm-9 text-left cusName" style="padding-top: 7px;">'+ json.cart.order.member.address +'</div>' +
                        '</div>' +

                        '</td>';

                    $(".customerArea").html(orderHtml);


                    
                }


            }
        },
        error : function(){
        },
        dataType: "json"
    });


});

var addProduct = function (id, qty) {
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
        cache: false,
        url: $('.add-product-url').attr('data-add-product-url'),
        data: { 'id' : id, 'qty' : qty  },
        beforeSend: function(){
            $('.spinner').removeClass('hidden');
        },
        success: function(json){
            $('.spinner').addClass('hidden');
            if(json.status == 'success'){
                if(json.cart.qty > 0){
                $('.posProductItem').html('');
                    $.each(json.cart.items, function( index, product ) {
                        var itemHtml = '<tr>' +
                            '<td class="text-center"><button class="btn btn-xs btn-default RemoveProductPOS" data-product-id="'+  product.product_id +'"   type="button" data-toggle="tooltip" title="" data-original-title="Remove Product"><i class="fa fa-times"></i></button></td>' +
                            '<td class="itemNamePOS"># ' +  product.product_id + ' '+ product.product_name +'</td>' +
                            '<td style="width: 40px;"  class="itemPricePOS text-left"> '+ product.store_in +'</td>' +
                            '<td style="width: 80px;"  class="itemPricePOS text-left"> '+ product.price +'</td>' +
                            '<td   style="width: 80px;" ><input min="0" step="0.5" data-last="'+ product.qty +'" class="itemQtyPOS form-control" data-id="'+  product.product_id +'" type="number" value="'+ product.qty +'"></td>' +
                            '<td style="width: 80px;"  class="itemPricePOS text-left"> '+ product.point +'</td>' +
                            '<td class="text-right itemTotalPOS"> '+ product.total +'</td>' +
                            '</tr>';
                        $('.posProductItem').append(itemHtml);
                    });
                }else{
                    $('.posProductItem').html('<tr><td colspan="5" class="text-center">No Item</td></tr>');
                }
                $('.CartTotalPoint').html(json.cart.point);
                $('.CartItemTotal').html(json.cart.qty);
                $('.CartTotal').html(json.cart.total +'<span style="font-size: 12px;">tk</span>');
            }
            if(json.status == 'OutOffStock') {
                $('.outOffStockMessage').text(json.message);
                $('#OutOffStock').modal('show');
            }
        },
        error : function(){
        },
        dataType: "json"
    });
};

var addProductByCode = function (code, qty) {
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
        cache: false,
        url: $('.add-product-url-code').attr('data-add-product-url-code'),
        data: { 'code' : code, 'qty' : qty  },
        beforeSend: function(){
            $('.spinner').removeClass('hidden');
        },
        success: function(json){
            $('.spinner').addClass('hidden');
            if(json.status == 'success'){
                if(json.cart.qty > 0){
                $('.posProductItem').html('');
                    $.each(json.cart.items, function( index, product ) {
                        var itemHtml = '<tr>' +
                            '<td class="text-center"><button class="btn btn-xs btn-default RemoveProductPOS" data-product-id="'+  product.product_id +'"   type="button" data-toggle="tooltip" title="" data-original-title="Remove Product"><i class="fa fa-times"></i></button></td>' +
                            '<td class="itemNamePOS"># ' +  product.product_id + ' '+ product.product_name +'</td>' +
                            '<td style="width: 40px;"  class="itemPricePOS text-left"> '+ product.store_in +'</td>' +
                            '<td style="width: 80px;"  class="itemPricePOS text-left"> '+ product.price +'</td>' +
                            '<td   style="width: 80px;" ><input min="0" step="0.5" data-last="'+ product.qty +'" class="itemQtyPOS form-control" data-id="'+  product.product_id +'" type="number" value="'+ product.qty +'"></td>' +
                            '<td style="width: 80px;"  class="itemPricePOS text-left"> '+ product.point +'</td>' +
                            '<td class="text-right itemTotalPOS"> '+ product.total +'</td>' +
                            '</tr>';
                        $('.posProductItem').append(itemHtml);
                    });
                }else{
                    $('.posProductItem').html('<tr><td colspan="5" class="text-center">No Item</td></tr>');
                }
                $('.CartTotalPoint').html(json.cart.point);
                $('.CartItemTotal').html(json.cart.qty);
                $('.CartTotal').html(json.cart.total +'<span style="font-size: 12px;">tk</span>');
            }
            if(json.status == 'OutOffStock') {
                $('.outOffStockMessage').text(json.message);
                $('#OutOffStock').modal('show');
            }
        },
        error : function(){
        },
        dataType: "json"
    });
};

var delaySearch = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


$(document).ready(function(e) {

    $(".ProductSelectBox").select2().on("change", function (e) {
        var id = $(this).val();
        if(id > 0){
            var Addqty = 1;
            $('.itemQtyPOS ').each(function (i, obj) {
                if(id == $(obj).attr('data-id')){
                   Addqty = parseInt($(obj).val()) + 1;
                }
            });
            addProduct($(this).val(), Addqty)
            $(".ProductSelectBox").val('');
            showCustomerInfo();
        }
    });

    $(".ProductSelectBoxCode").on("keyup", function () {
        var code = $(this).val();
        if(code != ''){
            delaySearch(function () {
                addProductByCode(code);
                $(".ProductSelectBoxCode").val('');
                $(".ProductSelectBoxCode").focus();
                showCustomerInfo();
            }, 400);
        }
    });

    $(".onChangeCustomerSales").on("keyup", function () {
        showCustomerInfo();
    });

    var showCustomerInfo = function () {
        var id = $('.onChangeCustomerSales').val();
        if(id != '' && id.length > 10){
            delaySearch(function () {
                $('.customerInfoSales').hide();
                $('.noCustomerFound').hide();
                $('.pleaseWait').show();

                $.ajax({
                    headers: {'X-CSRF-TOKEN': Laravel.csrfToken},
                    type: "POST",
                    cache: false,
                    url: $('.onChangeCustomerSales').attr('data-cus_url'),
                    data: { id : id},
                    success: function(json){
                        $('.pleaseWait').hide();
                        if(json.message == 1){
                            $('.customerInfoSales').show();
                            $('.cusName').html(json.name);
                            $('.cusDue').html(json.due);
                            $('.cusPayable').html(json.payable);
                        }else{
                            $('.noCustomerFound').show();
                        }
                    },
                    error : function(){

                    },
                    dataType: "json"

                });

            }, 400);
        }
    };


    $(".posProductItem").on("click", '.RemoveProductPOS', function (e) {
        addProduct($(this).attr('data-product-id'), 0);
        showCustomerInfo();
    });

    $('.posProductItem').on("keyup change", '.itemQtyPOS', function (e) {
        var id = $(this).attr('data-id');
        var qty = $(this).val();
        if (qty != $(this).attr('data-last') && qty > 0) {
            delaySearch(function () {
                addProduct(id, qty);
                showCustomerInfo();
            }, 400);
        }
        $(this).attr('data-last',qty);
    });


    $('.form-vertical-sales')
        .formValidation()
        .on('success.form.fv', function(e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({

                type: "POST",
                cache: false,
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function(){
                    $('.spinner').removeClass('hidden');
                    form.find('#please-wait').removeClass('hidden');
                    form.find('#success').addClass('hidden');
                    form.find('#error').addClass('hidden');

                },
                success: function(json){
                    if(json.status == 'success'){
                          window.location.href = json.order_url;
                    }
                    else if(json.status == 'false') {
                        var error = form.find('#error');
                        error.find('.message').text(json.message);
                        error.removeClass('hidden');

                    }
                    else {
                        var error = form.find('#error');
                            error.find('.message').text('Please Typ Later!');
                            error.removeClass('hidden');
                    }
                },

                error : function(){
                    var error = form.find('#error');
                    error.find('.message').text('Internal Server Error');
                    error.removeClass('hidden');
                },
                dataType: "json"

            });
        });

 });
