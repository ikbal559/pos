var ProcessBill = function (id, qty) {
    $.ajax({
        type: "POST",
        cache: false,
        url: $('.StartBillProcessBtn').attr('data-url'),
        data: {   },
        beforeSend: function(){
            $('.spinner').removeClass('hidden');
        },
        success: function(jsonOutput){
            var json = jsonOutput.output;
            $('.spinner').addClass('hidden');
            $('.totalPendingOrder').html(json.total);
            $('.totalPendingPoint').html(json.points);
            if(json.total > 0){
                $('.order_id').html(json.order.order_id);
                $('.points').html(json.order.order_point);
                $('.customer_id').html(json.order.customer_id);

                setTimeout(function(){
                    ProcessBill();
                }, 5000);
                
            }else {
               setTimeout(function(){location.reload();}, 1000);
            }

        },
        error : function(){
        },
        dataType: "json"
    });
};

$(document).ready(function(e) {

    $(".StartBillProcessBtn").on("click", function (e) {
        $('.startProcess').addClass('hidden');
        $('.bill_processing').removeClass('hidden');
        $('#confirm-delete-role').modal('hide');
        ProcessBill();
    });

    $("#processes_pac_bill").on("click", function (e) {
        $.ajax({
            type: "POST",
            cache: false,
            url: $('#processes_pac_bill').attr('data-action'),
            beforeSend: function(){

                $('#please-wait').removeClass('hidden');
                $('#success').addClass('hidden');
                $('#error').addClass('hidden');

            },
            success: function(json){

                $('#please-wait').addClass('hidden');

                if(json.status == 'success'){

                    var success = $('#success');
                    success.find('.message').text(json.message);
                    success.removeClass('hidden');
                    setTimeout(function(){location.reload();}, 3000);

                }else{

                    var error = $('#error');
                    error.find('.message').text('Bill Already Paid');
                    error.removeClass('hidden');

                }
            },

        });
    });

 });
