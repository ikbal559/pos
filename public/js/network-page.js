$(document).ready(function(e){

    $('.NetworkDetailBtn').click(function () {
        $('#NetworkDetailModel').modal('show');
        var id = $(this).attr('data-user-id');
        $('.netdetailload').show();
        $('.netdetail').hide();
        $.ajax({
            type: "POST",
            cache: false,
            url:   $('#NetworkDetailModel').attr('data-action'),
            data: { 'id' : id},
            success: function(json){
                var networkHtml ='';
                $.each(json.data.net, function (key, account) {

                    if(account.user.user_images == null){
                        var img = '<img class="img-avatar img-avatar48" src="'+ window.base_url +'/images/avatars/avatar4.jpg" alt="">';
                    }else{
                        var img = '<img class="img-avatar img-avatar48" src="'+ window.base_url +'/images/user_images/'+ account.user.user_images +'" alt="">';
                    }

                    if(account.user.hide_phone == 0){
                        var username = account.user.username;
                    }else{
                        var username = 'Hidden';
                    }

                     networkHtml +='<tr>' +
                         '<td class="text-center">'+ img +'</td>' +
                         '<td>'+ username +'</td>' +
                         '<td>'+ account.user.name +'</td>' +
                         '<td>'+ account.points +'</td>' +
                         '</tr>';

                });
                if(networkHtml == ''){
                    $('.NetDetailRow').html('<tr><td colspan="4" class="text-center">No User Found!</td></tr>');
                }else{
                    $('.NetDetailRow').html(networkHtml);
                }
                $('.netdetailload').hide();
                $('.netdetail').show();
            },
            error : function(){
                console.log('error');
            },
            dataType: "json"

        });

    });

});
